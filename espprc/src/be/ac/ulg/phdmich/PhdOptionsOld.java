package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.StdOut;

/**
 * Class describing the user options for the pricing algorithm.
 * 
 * @author Michelini
 */
public class PhdOptionsOld {
  private final int numCol;
  // private final int concatStopMultiplier; //TODO add it
  private final AccStr acc;
  // DSSR options
  //private final boolean dssr;
  private final DssrInitStr initStr;
  private final int dssrInitAmount;
  private final DssrInsStrPath insPathStr;
  private final int insPathAmount;
  private final DssrInsStrNode insNodeStr;
  private final double insNodePercent;
  // NG-route options
  //private final boolean ng;
  private final NgSetType ngType;
  private final int ngSize;
  private final double mixAlpha;
  //private final boolean ngDssr;
  
  public enum AccStr { 
    NONE, DSSR_G, DSSR_L,
    NG, HYBRID1, HYBRID2,
    HYBRID1_PLUS, HYBRID2_PLUS 
  }

  public enum DssrInitStr { HCA, TCA, WHCA, WTCA, MIXED, NONE }

  public enum DssrInsStrPath { ONE_PATH, ALL_PATHS, IN_BETWEEN }
  
  public enum DssrInsStrNode { ONE_NODE, ALL_NODES, IN_BETWEEN }
  
  public enum NgSetType { TRAVEL_TIME, MAX_DURATION, MIXED }
  
  /**
   * Constructs the set of user-defined options from an array of strings. The accepted amount of 
   * strings is eleven, with each element being the following:
   * 
   *  <p>1: (int) the max amount of NRC columns to be inserted at each iteration;
   *  2: (str) the acceleration strategy for the pricing problem;
   *  3: (str) the type of critical set initialization (if DSSR is activated);
   *  4: (int) the amount of nodes with which to initialize the critical set;
   *  5: (str) the type of critical set update strategy w. respect to the amount of paths to check;
   *  6: (int) the actual n. of paths to check for elementarity agfter each iteration of DSSR;
   *  7: (str) the type of critical set update strategy w.r. to the percentage of nodes to include;
   *  8: (dou) the percentage of repeated nodes to include at the end of each iteration of DSSR; 
   *  9: (str) the type of measure with which to build the ng-sets;
   *  10: (int) the size of the ng-sets;
   *  11: (dou) the affine coefficient for the mixed ng-set metric strategy.
   *  
   *  <p>Ex.: { 5, "DSSR", "WTCA", 10, "IN_BETWEEN", 4, "ALL_NODES", 0.2, "TRAVELTIME", 15, 0.5 }
   * 
   * @param input the array of strings defining the user options 
   */
  public PhdOptionsOld(String[] input) throws IllegalArgumentException {
    if (input.length != 11) {
      throw new IllegalArgumentException("Invalid amount of arguments (not 11).");
    }
    numCol = Integer.parseInt(input[0]);
    switch (input[1].toUpperCase()) {
      case "DSSR": case "DSSR_G":
        acc = AccStr.DSSR_G;
        break;
      case "DSSR_L":
        acc = AccStr.DSSR_L;
        break;
      case "NG":
        acc = AccStr.NG;
        break;
      case "HYBRID1":
        acc = AccStr.HYBRID1;
        break;
      case "HYBRID2":
        acc = AccStr.HYBRID2;
        break;
      case "HYBRID1_PLUS":
        acc = AccStr.HYBRID1_PLUS;
        break;
      case "HYBRID2_PLUS":
        acc = AccStr.HYBRID2_PLUS;
        break;
      default:
        acc = AccStr.NONE;
        break;
    }
    switch (input[2].toUpperCase()) {
      case "HCA":
        initStr = DssrInitStr.HCA;
        break;
      case "TCA":
        initStr = DssrInitStr.TCA;
        break;
      case "WHCA":
        initStr = DssrInitStr.WHCA;
        break;
      case "WTCA":
        initStr = DssrInitStr.WTCA;
        break;
      case "MIXED":
        initStr = DssrInitStr.MIXED;
        break;
      default:
        initStr = DssrInitStr.NONE;
    }
    dssrInitAmount = Integer.parseInt(input[3]);
    switch (input[4].toUpperCase()) {
      case "ONE":
        insPathStr = DssrInsStrPath.ONE_PATH;
        insPathAmount = 1;
        break;
      case "IN_BETWEEN":
        insPathStr = DssrInsStrPath.IN_BETWEEN;
        insPathAmount = Integer.parseInt(input[5]);
        if (insPathAmount < 1 || insPathAmount > numCol) {
          throw new IllegalArgumentException("Invalid number of paths to check");
        }
        break;
      default:
        insPathStr = DssrInsStrPath.ALL_PATHS;
        insPathAmount = numCol;
    }
    
    switch (input[6].toUpperCase()) {
      case "ONE":
        insNodeStr = DssrInsStrNode.ONE_NODE;
        break;
      case "IN_BETWEEN":
        insNodeStr = DssrInsStrNode.IN_BETWEEN;
        break;
      default:
        insNodeStr = DssrInsStrNode.ALL_NODES;
    }
    insNodePercent = Double.parseDouble(input[7]);
    if (insNodePercent < 0 || insNodePercent > 1) {
      throw new IllegalArgumentException("Invalid value for node insertion percentage");
    }
    switch (input[8].toUpperCase()) {
      case "TRAVEL_TIME":
        ngType = NgSetType.TRAVEL_TIME;
        break;
      case "MIXED":
        ngType = NgSetType.MIXED;
        break;
      default:
        ngType = NgSetType.MAX_DURATION;
    }
    ngSize = Integer.parseInt(input[9]);
    mixAlpha = Double.parseDouble(input[10]);
    if (mixAlpha < 0 || mixAlpha > 1) {
      throw new IllegalArgumentException("Not a valid coefficient for affine combination");
    }
  }
  
  public boolean usingGlobalTheta() {
    return (acc == AccStr.DSSR_G || acc == AccStr.HYBRID1 || acc == AccStr.HYBRID1_PLUS);
  }
  
  public boolean usingNgSets() {
    return !(acc == AccStr.NONE || acc == AccStr.DSSR_G || acc == AccStr.DSSR_L);
  }
  
  public boolean usingTempNgSets() {
    return (acc == AccStr.HYBRID2 || acc == AccStr.HYBRID2_PLUS);
  }
  
  public boolean usingLocalTheta() {
    return (acc == AccStr.DSSR_L);
  }
  
  public boolean usingPlus() {
    return (acc == AccStr.HYBRID1_PLUS || acc == AccStr.HYBRID2_PLUS);
  }
  
  public int getNumCol() {
    return numCol;
  }

  public AccStr getAccStr() {
    return acc;
  }

  public DssrInitStr getInitStr() {
    return initStr;
  }

  public DssrInsStrPath getInsPathStr() {
    return insPathStr;
  }

  public int getInsPathAmount() {
    return insPathAmount;
  }

  public DssrInsStrNode getInsNodeStr() {
    return insNodeStr;
  }

  public double getInsNodePercent() {
    return insNodePercent;
  }

  public int getDssrInitAmount() {
    return dssrInitAmount;
  }

  public NgSetType getNgType() {
    return ngType;
  }

  public int getNgSize() {
    return ngSize;
  }

  public double getMixAlpha() {
    return mixAlpha;
  }
  
  @Override
  public String toString() {
    String newline = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    str.append("Number of columns:............................." + numCol);
    str.append(newline);
    str.append("Acceleration Strategy:.........................");
    switch (acc) {
      case NONE:
        str.append("NONE");
        break;
      case DSSR_G:
        str.append("GLOBAL DSSR");
        break;
      case DSSR_L:
        str.append("LOCAL DSSR");
        break;
      case NG:
        str.append("NG");
        break;
      case HYBRID1:
        str.append("HYBRID1");
        break;
      case HYBRID2:
        str.append("HYBRID2");
        break;
      case HYBRID1_PLUS:
        str.append("HYBRID1_PLUS");
        break;
      case HYBRID2_PLUS:
        str.append("HYBRID2_PLUS");
        break;
      default:
        break;
    }
    str.append(newline);
    str.append("Initialization Strategy........................");
    if ((acc != AccStr.DSSR_G && acc != AccStr.DSSR_L 
        && acc != AccStr.HYBRID1 && acc != AccStr.HYBRID1_PLUS)) {
      str.append("n/a");
    } else {
      switch (initStr) {
        case HCA:
          str.append("HCA");
          str.append(newline);
          str.append("Size for Theta initialization:.................." + dssrInitAmount);
          break;
        case TCA:
          str.append("TCA");
          str.append(newline);
          str.append("Size for Theta initialization:.................." + dssrInitAmount);
          break;
        case WHCA:
          str.append("WHCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + dssrInitAmount);
          break;
        case WTCA:
          str.append("WTCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + dssrInitAmount);
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          str.append("Size for Theta initialization:.................." + dssrInitAmount);
          break;
        case NONE:
          str.append("NONE");
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("Path insertion strategy:.......................");
    if ((acc == AccStr.NONE || acc == AccStr.NG)) {
      str.append("n/a");
    } else {
      switch (insPathStr) {
        case ONE_PATH:
          str.append("ONE_PATH");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + 1);
          break;
        case ALL_PATHS:
          str.append("ALL_PATHS");
          str.append(newline);
          str.append("Amount of paths to verify and insert:........." + numCol);
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + insPathAmount);
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("Node insertion strategy:.......................");
    if ((acc != AccStr.DSSR_G && acc != AccStr.HYBRID1 && acc != AccStr.HYBRID1_PLUS)) {
      str.append("n/a");
    } else {
      switch (insNodeStr) {
        case ONE_NODE:
          str.append("ONE_NODE");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:..." + 1);
          break;
        case ALL_NODES:
          str.append("ALL_NODES");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:...ALL");
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          double perc = 100 * insNodePercent;
          str.append("Amount of multiple nodes to insert per path:..." + perc + "%");
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("NG set metric:.................................");
    if ((acc == AccStr.DSSR_G || acc == AccStr.NONE || acc == AccStr.DSSR_L)) {
      str.append("n/a");
    } else {
      switch (ngType) {
        case TRAVEL_TIME:
          str.append("TRAVEL_TIME");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case MAX_DURATION:
          str.append("MAX_DURATION");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          double perc = 100 * mixAlpha;
          str.append("NG metric mixing percentage:..................." + perc + "%");
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("NG set size:...................................");
    if (acc == AccStr.DSSR_G || acc == AccStr.NONE || acc == AccStr.DSSR_L) {
      str.append("n/a");
    } else {
      str.append(ngSize);
    }
    return str.toString();
  }
  
  public static void main(String[] args) {
    PhdOptionsOld options = new PhdOptionsOld(args);
    StdOut.println(options);
  }
}
