package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.EdgeWeightedDigraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.awt.Font;

/**
 *  Class supporting. TODO complete doc
 *
 *  @author Stefano Michelini
 */
public class Instance {
  private String name = "";
  private Options options;
  private int fleetSize;
  private double capacity;
  private final short size;
  private ModdedEdgeWeightedDigraph graph;
  private double avgDist;
  private final int[] xcoord;
  private final int[] ycoord;
  private final double[] dem;      // demands
  private final double[] tws;      // time windows start value        
  private final double[] twe;      // time windows end value
  private final double[] svt;      // service time
  private final double[] twsBw;    // reverse time windows, used for bidirectional DP
  private final double[] tweBw;
  private final double twsDepot;
  private final double tweDepot;
  private final double[][] distances; // auxiliary distance matrix
  private boolean[][] forbidden;  // keeps track of the eliminated arcs
  private final double max;        // latest possible arrival time, used to revert time windows
  private final double min;        //earliest possible arrival time
  private final Parameters par;

  public Instance(String parFile, String instFile) {
    this(new In(parFile), new In(instFile));
  }
  
  public Instance(In parFile, In instFile) {
    this(new Parameters(parFile), instFile, new Options(new String[0]));
  }
  
  public Instance(String inFile, Options options) {
    this(new Parameters(options), new In(inFile), options);
  }
  
  // Solomon instances have 9 lines before the list of customers
  private static final int OFFSET = 9;
  
  // line number of fleet size and capacity
  private static final int OFFSET2 = 4;
  
  /**
   * Constructor. TODO complete doc
   * 
   * @param par file with the parameter values
   * @param instFile file describing the instance
   */
  public Instance(Parameters par, In instFile, Options options) {
    this.par = par;
    this.options = options;
    String[] lines = instFile.readAllLines();
    size = (short) Math.min(par.getSize(), lines.length - OFFSET - 1);
    graph = new ModdedEdgeWeightedDigraph(size + 2);
    dem     = new double[getSize() + 2];
    tws     = new double[getSize() + 2];
    twe     = new double[getSize() + 2];
    svt     = new double[getSize() + 2];
    //pzs     = new double[getN() + 2];
    twsBw   = new double[getSize() + 2];
    tweBw   = new double[getSize() + 2];
    forbidden = new boolean[size + 2][size + 2];
    distances  = new double [size + 2][size + 2];
    xcoord = new int[size + 2];     // we won't store the coordinates
    ycoord = new int[size + 2];     // only use them to get edge lengths
    double maxDistFromDepot = 0;
    double minDistFromDepot = Double.POSITIVE_INFINITY;
    double twsd = 0;
    double twse = 0;
    //iFile.readLine();
    name = lines[0];
    String[] str = lines[OFFSET2].split("\\s+");
    fleetSize = Integer.parseInt(str[1]);
    capacity = Double.parseDouble(str[2]);
    if (!options.hasCapacity()) {
      par.maxCapacity = capacity;
    }
    for (int i = 0; i <= size; i++) {
      str = lines[i + OFFSET].split("\\s+");
      xcoord[i]   = Integer.parseInt(str[2]);
      ycoord[i]   = Integer.parseInt(str[3]);
      dem[i] = Double.parseDouble(str[4]);
      double twsValue = Double.parseDouble(str[5]);
      tws[i] = (i == 0) ? Double.NEGATIVE_INFINITY : twsValue;
      double tweValue = Double.parseDouble(str[6]);
      twe[i] = (i == 0) ? Double.POSITIVE_INFINITY : tweValue;
      if (i == 0) {
        twsd = twsValue;
        twse = tweValue;
      }
      svt[i] = Double.parseDouble(str[7]);
      double distanceToDepot = 0;
      if (i != 0) {
        //getPzs()[i] = dual[i - 1];
        for (int j = 0; j < i; j++) {
          double distance = computeDistance(xcoord[i], ycoord[i], xcoord[j], ycoord[j]);
          distances[i][j] = distance;
          distances[j][i] = distance;
          if (j == 0) {
            distanceToDepot = distance;
            distances[size + 1][i] = distance;
            distances[i][size + 1] = distance;
          }
          if (tws[j] + svt[j] + distance <= twe[i]) { // we eliminate useless arcs
            graph.addEdge(new DirectedEdge(j, i, distance));
          } else {
            // maybe it's better to put expensive arcs instead?
            //getG().addEdge(new DirectedEdge(j, i, Double.POSITIVE_INFINITY));
            forbidden[j][i] = true;
          }
          int dest = j == 0 ? getSize() + 1 : j;
          if (tws[i] + svt[i] + distance <= twe[j]) { // we eliminate useless arcs
            graph.addEdge(new DirectedEdge(i, dest, distance));
          } else {
            //getG().addEdge(new DirectedEdge(i, dest, Double.POSITIVE_INFINITY));
            forbidden[i][dest] = true;
          }
        }
        maxDistFromDepot = Math.max(maxDistFromDepot, twe[i] + svt[i] + distanceToDepot);
        minDistFromDepot = Math.min(minDistFromDepot, tws[i] - distanceToDepot);
      } else {
        xcoord[size + 1] = xcoord[0];
        ycoord[size + 1] = ycoord[0];
      }
    }
    max = maxDistFromDepot;
    min = minDistFromDepot;
    twsDepot = twsd;
    tweDepot = twse;
    dem[size + 1] = getDem()[0];
    tws[size + 1] = tws[0];
    twe[size + 1] = twe[0];
    svt[size + 1] = svt[0];
    //getPzs()[getN() + 1] = getPzs()[0];
    for (int i = 0; i < getSize() + 2; i++) {
      twsBw[i] = max - twe[i];
      tweBw[i] = max - tws[i];
    }
    
    if (par.getMaxDuration() == 0) {
      par.maxShiftDuration = tweDepot / 2;
    } else if (par.getMaxDuration() == -1) {
      par.maxShiftDuration = tweDepot;
    }
    
    avgDist = 0;
    for (int v = 0; v < graph.getVertices(); v++) {
      for (DirectedEdge e : graph.adj(v)) {
        avgDist += e.weight();
      }
    }
    avgDist /= graph.getEdges();
  }

  @Override
  public String toString() {
    final String newLine = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();

    for (int k = 0; k < getG().getVertices(); k++) {
      str.append(k
          + ": dem: " + getDem()[k] 
          + ", tws: " + getTws()[k]  
          + ", twe: " + getTwe()[k]  
          + ", svt: " + getSvt()[k]  
          // + ", pzs: " + getPzs()[k] 
          + "\n");
    }
    str.append(getG().toString());
    str.append("Min departure: " + min);
    str.append(newLine);
    str.append("Depot deadline: " + twsDepot);
    str.append(newLine);
    str.append("Max arrival: " + getMax());
    str.append(newLine);
    str.append("Depot deadline: " + tweDepot);
    str.append(newLine);
    return str.toString();
  }

  private static double computeDistance(int x1, int y1, int x2, int y2) {
    double dx = (x1 - x2) * (x1 - x2);
    double dy = (y1 - y2) * (y1 - y2);
    double dist = Math.sqrt(dx + dy);
    return Math.floor(10 * dist) / 10;
  }
  
  public double getTwsBwRs(int node) {
    return tws[node] + svt[node];
  }
  
  public double getTweBwRs(int node) {
    return twe[node] + svt[node];
  }
  
  /**
   * Return the distance (travel time) between n1 and n2 if it is
   * feasible to travel from n1 to n2 or vice-versa.
   * Otherwise, return infinity.
   * 
   * @param n1 the first node
   * @param n2 the second node
   * @return the distance if feasible, otherwise infinity
   */
  public double getFeasibleDistance(int n1, int n2) {
    if (isTwoCycleImpossible(n1, n2)) {
      return Double.POSITIVE_INFINITY;
    } else {
      return distances[n1][n2];
    }
  }
  
  /**
   * Returns the maximum duration between two nodes, defined as the maximum between the travel time
   * summed with the maximum waiting time going from n1 to n2, and the same quantity from n2 to n1.
   * If it's infeasible to go from n1 to n2 or to n2 to n1 (due for example to the time windows),
   * then the distance is positive infinity.
   * 
   * @param n1 the first node
   * @param n2 the second node
   * @return the maximum duration
   */
  public double getMaxDuration(int n1, int n2) {
    return Math.max(getMaxDur(n1, n2), getMaxDur(n2, n1));
  }
  
  private double getMaxDur(int n1, int n2) {
    if (graph.edge(n1, n2) == null) {
      return Double.POSITIVE_INFINITY;
    }
    // max(s1 + t12, a2 - a1)? OR max(s1 + t12, a2 - b1)
    return Math.max(svt[n1] + distances[n1][n2], tws[n2] - tws[n1]);
    // return Math.max(svt[n1] + distances[n1][n2], tws[n2] - twe[n1]);
  }
  
  /**
   * Returns the cheap cycle risk betwen node n1 and n2.
   * 
   * @param n1 the first node 
   * @param n2 the second node
   * @return the cheap cycle risk between n1 and n2
   */
  public double getCheapCycleRisk(int n1, int n2) {
    if (isTwoCycleImpossible(n1, n2)) {
      return Double.POSITIVE_INFINITY;
    }
    final double tij = svt[n1] + getDistance(n1, n2);
    final double tji = svt[n2] + getDistance(n2, n1);
    double ov = overlap(n1,n2);
    if (ov == 0) {
      return Double.POSITIVE_INFINITY;
    }
    return (tij + tji) / ov;
  }
  
  // return time window overlap between two time windows
  private double overlap(int n1, int n2) {
    if (tws[n2] < tws[n1]) {
      if (twe[n2] < tws[n1]) {
        return 0;
      } else if (twe[n2] < twe[n1]) {
        return twe[n2] - tws[n1];
      } else {
        return twe[n1] - tws[n1];
      }
    } else if (tws[n2] < twe[n1]) {
      if (twe[n2] < twe[n1]) {
        return twe[n2] - tws[n2];
      } else {
        return twe[n1] - tws[n2];
      }
    } else {
      return 0;
    }
  }
  
  private boolean isTwoCycleImpossible(int n1, int n2) {
    double tij = svt[n1] + distances[n1][n2];
    double tji = svt[n2] + distances[n2][n1];
    return (graph.edge(n1, n2) == null
      || graph.edge(n2, n1) == null
      || (Math.max(tws[n1] + tij, tws[n2]) + tji > twe[n1] 
          && Math.max(tws[n2] + tji, tws[n1]) + tij > twe[n2]));
  }

  public static class Parameters {
    private final int    num;
    private final double distUnitCost;
    private final double timeUnitCost;
    private double maxShiftDuration;
    private double maxCapacity;

    /**
     * Constructor. TODO complete doc
     * 
     * @param paramIn input file with the parameter values
     */
    public Parameters(In paramIn) {
      num = paramIn.readInt();
      paramIn.readString();
      distUnitCost = paramIn.readDouble();
      paramIn.readString();
      timeUnitCost = paramIn.readDouble();
      paramIn.readString();
      maxShiftDuration = paramIn.readDouble();
      paramIn.readString();
      maxCapacity = paramIn.readDouble();
    }
    
    /**
     * Build the parameter class from the program options.
     * 
     * @param options the program options
     */
    public Parameters(Options options) {
      num = options.getSize();
      maxCapacity = options.getCapacity();
      maxShiftDuration = options.getShift();
      distUnitCost = options.getDistUnitCost();
      timeUnitCost = options.getTimeUnitCost();
    }

    public int getSize() {
      return num;
    }

    public double getMaxDuration() {
      return maxShiftDuration;
    }

    public double getDistUnitCost() {
      return distUnitCost;
    }

    public double getTimeUnitCost() {
      return timeUnitCost;
    }

    public double getMaxCapacity() {
      return maxCapacity;
    }
  }
  
  public Options getOptions() {
    return options;
  }
  
  public int getX(int index) {
    return xcoord[index];
  }
  
  public int getY(int index) {
    return ycoord[index];
  }

  public short getSize() {
    return size;
  }

  public Parameters getPar() {
    return par;
  }

  public ModdedEdgeWeightedDigraph getG() {
    return graph;
  }

  public double[] getSvt() {
    return svt;
  }
  
  public double getServiceTime(int node) {
    return svt[node];
  }

  public double[] getDem() {
    return dem;
  }
  
  public double getDemand(int node) {
    return dem[node];
  }

  public double[] getTws() {
    return tws;
  }
  
  public double getTwsOf(int node) {
    return tws[node];
  }

  public double[] getTwe() {
    return twe;
  }
  
  public double getTweOf(int node) {
    return twe[node];
  }

  public double[] getTwsBw() {
    return twsBw;
  }

  public double[] getTweBw() {
    return tweBw;
  }

  public double getMax() {
    return max;
  }

  public double getAvgDist() {
    return avgDist;
  }

  public void setAvgDist(double val) {
    this.avgDist = val;
  }

  public void setG(ModdedEdgeWeightedDigraph val) {
    graph = val;
  }
  
  public double getDistance(int n1, int n2) {
    return distances[n1][n2];
  }
  
  public String getName() {
    return name;
  }
  
  public int getFleetSize() {
    return fleetSize;
  }
  
  public boolean[][] getForbidden() {
    return forbidden;
  }
  
  public boolean isForbidden(int tail, int head) {
    return forbidden[tail][head];
  }

  public void setForbidden(boolean[][] val) {
    this.forbidden = val;
  }
  
  /**
   * Draw the instance.
   */
  public void draw() {
    StdDraw.setPenRadius(0.01);
    StdDraw.setPenColor();
    StdDraw.setScale(-1, 100);
    StdDraw.point(xcoord[0], ycoord[0]);
    StdDraw.setPenColor(StdDraw.MAGENTA);
    StdDraw.setFont(new Font("Arial", Font.PLAIN, 10));
    for (int i = 1; i < size + 1; i++) {
      StdDraw.point(xcoord[i], ycoord[i]);
      String name = "" + i;
      StdDraw.textLeft(xcoord[i], ycoord[i] + 1, name);
    }
  }

  /**
   * Test client for the class.
   * 
   * @param args the program arguments
   */
  public static void main(String[] args) {
    EdgeWeightedDigraph graph = new EdgeWeightedDigraph((new In("data/tinyEWG.txt")));
    StdOut.println(graph);
    In parFile = new In(args[0]);
    In instFile = new In(args[1]);
    Instance inst = new Instance(parFile, instFile);
    //        for (int k = 0; k < n + 2; k++) {
    //            StdOut.printf("dem: %f, tws: %f, twe: %f, svt: %f, pzs: %f\n",
    //                    i.dem[k], i.tws[k], i.twe[k], i.svt[k], i.pzs[k]);
    //        }
    // StdOut.println(i.G.toString());
    // StdOut.println(i.maxT);
    // inst.draw();
    StdOut.println(inst.toString());
    instFile.close();
    parFile.close();
  }
}
