package be.ac.ulg.phdmich;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class representing the state of the entiore algorithm, or the data on which it is operating.
 * 
 * @author Michelini
 */
public class State {
  private final Options options;
  private final int size;
  private final boolean onRootNode;
  //private final Set<Integer> allNodes;
  private final boolean[][] forbidden;
  //NG-used fields
  private final Map<Short, Set<Short>> ngSet;
  private Map<Short, Set<Short>> ngDssrSet;
  
  /**
   * Constructs the representation of the state of the algorithm. If 
   * ngSet is null, constructs the neighborhoods according to the 
   * provided options.
   *  
   * @param instance the problem instance
   * @param forbidden matrix detailing which arcs are forbidden
   * @param options the algorithm options
   * @param onRoot whether the state is for the root node of the bb tree
   * @param ngSet the neighborhoods for ng-route algorithms
   */
  public State(Instance instance, boolean[][] forbidden, Options options, boolean onRoot,
      Map<Short, Set<Short>> ngSet) {
    this.onRootNode = onRoot;
    this.options = options;
    this.forbidden = forbidden;
    this.size = instance.getSize();
    this.ngDssrSet = new HashMap<>();
    if (options.usingNgSets()) {
      if (ngSet == null) {
        this.ngSet = new HashMap<>();
        for (short i = 0; i <= size + 1; i++) {
          this.ngSet.put(i, Lib.buildNgSet(instance, options, i, onRoot));
        } 
      } else {
        this.ngSet = ngSet;
      }
    } else {
      this.ngSet = new HashMap<>();
    }
    if (options.usingTempNgSets()) {
      this.ngDssrSet.put((short)0, Lib.buildNgSet(instance, options, (short)0, onRoot));
      this.ngDssrSet.put((short)(size + 1),
          Lib.buildNgSet(instance, options, (short)(size + 1), onRoot));
      for (short i = 1; i <= size; i++) {
        this.ngDssrSet.put(i, new HashSet<>());
      }
    }
  }
  
  public State(Instance instance, Options options, boolean onRoot) {
    this(instance, instance.getForbidden(), options, onRoot, null);
  }
  
  public boolean isForbidden(int from, int to) {
    return forbidden[from][to];
  }
  
  public int size() {
    return size;
  }
  
  public Set<Short> getNgSetAt(short node) {
    return ngSet.get(node);
  }
  
  public Set<Short> getTempNgSetAt(short node) {
    return ngDssrSet.get(node);
  }
  
  public Options getOptions() {
    return options;
  }
  
  public boolean isRootNode() {
    return onRootNode;
  }
  
  public Map<Short, Set<Short>> getTempNgSet() {
    return ngDssrSet;
  }
  
  /**
   * Inserts the node in all the applied ng sets encountered backwards from the starting point,
   * until it encounters again the same node. In other words it performs this action
   * on the nodes forming the cycle encountered backwards from the starting point.
   * 
   * @param list the sequence representing the path
   * @param node the node to insert in the applied ng sets
   * @param start the starting position where node is located
   */
  public void updateNgSets(List<Short> list, short node, short start) {
    Lib.updateLocalSets(ngDssrSet, list, node, start);
  }
}
