package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.StdDraw;

import java.util.Map;
import java.util.Set;

public class Solution {
  private double cost;
  private Map<Path, Double> pathsValues;
  private boolean allElementary;
  private boolean integral;
  private Instance instance;
  
  /**
   * Constructor for the data type.
   * 
   * @param cost the total cost of the solution
   * @param pathsValues a map associating a non-zero value to each path considered in the solution
   * @param instance the problem instance
   * @param allElem whether all the paths in the solution are elementary or not
   */
  public Solution(double cost, Map<Path, Double> pathsValues, 
      Instance instance, boolean allElem) {
    this.cost = cost;
    this.allElementary = allElem;
    this.pathsValues = pathsValues;
    this.instance = instance;
    this.integral = checkIntegrality(pathsValues);
  }
  
  private boolean checkIntegrality(Map<Path, Double> pathsValues) {
    if (pathsValues != null) {
      for (Path p : pathsValues.keySet()) {
        double val = pathsValues.get(p);
        if (0 < val && val < 1) { // TODO check for tolerance 
          return false;
        }
      } 
    }
    return true;
  }
  
  public double getCost() {
    return cost;
  }
  
  /**
   * Returns the set of paths if it exists.
   * 
   * @return the set of paths.
   */
  public Set<Path> getPaths() {
    if (pathsValues == null) {
      return null;
    }
    return pathsValues.keySet();
  }
  
  public boolean isIntegral() {
    return integral;
  }
  
  public double getValue(Path key) {
    return pathsValues.get(key);
  }
  
  public boolean areAllElementary() {
    return allElementary;
  }
  
  /**
   * Draw the instance and the paths forming this solution.
   */
  public void draw() {
    instance.draw();
    StdDraw.setPenColor(StdDraw.BLACK);
    for (Path p : pathsValues.keySet()) {
      p.draw(instance);
    }
  }
  
  @Override
  public String toString() {
    String newline = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    str.append("Solution values: ");
    str.append(newline);
    int count = 1;
    if (pathsValues != null) {
      for (Path p : pathsValues.keySet()) {
        str.append("Path " + (count++) + ": " + p);
        str.append("Value: " + Lib.roundToString(pathsValues.get(p)));
        str.append(newline);
      } 
    }
    String elem = allElementary ? "yes"  : "no";
    str.append("Total cost: " + Lib.roundToString(cost));
    str.append("\tAll paths elementary? " + elem);
    str.append(newline);
    return str.toString();
  }
}
