package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.Out;
import edu.princeton.cs.algs4.StdOut;

public class HeuristicsClient {
  /**
   * Client for testing the heuristics (unused).
   * 
   * @param args the program arguments 
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    //StdOut.println(options);
    int count = 0;
    Out out = new Out(options.getOutFilename());
    for (String s : options.getArgs()) {
      count++;
      if (count == 6) {
        options.setSize(50);
        StdOut.println("Setting size to 50");
        out.println("Setting size to 50");
      }
      StdOut.println("Processing " + s);
      out.println("Processing " + s);
      Instance instance = new Instance(s, options);
      BranchPrice bp = new BranchPrice(instance, options);
      //ColumnGeneration cg = new ColumnGeneration(instance, options);
      StdOut.println(bp);
      out.println(bp);
    }
    StdOut.println("Terminating");
    out.close();
  }
}
