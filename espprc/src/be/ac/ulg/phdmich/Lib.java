package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Out;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.StopwatchCPU;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.ToDoubleBiFunction;
import java.util.stream.Stream;

/**
 * Library class providing static methods to be used as components of a ESPPRC-type algorithm.
 * 
 * @author Stefano Michelini
 */
final class Lib {
  
  public static final double TOLERANCE  = 1.0e-8;
  public static final double TOLERANCE2 = 1.0e-8;
  public static final int    PRECISION  = 6;
  
  /**
   * Initializes an iteration of the ESPPRC type algorithm, maintaining the sets needed for 
   * Decremental State Space Relaxation if it is activated.
   * 
   * @param inst the problem instance
   * @param state the state of the ESPPRC algorithm 
   */
  public static void initialize(Instance inst, PricingState state, boolean isHeuristic) {
    state.getPaths().clear();
    state.getGammaFw().clear();
    state.getGammaBw().clear();
    state.getTheta().addAll(state.getPsi());
    state.getPsi().clear();
    state.resetNodesToExamine(inst.getSize(), isHeuristic);

    short source = 0;
    short sink = (short) (inst.getSize() + 1);
    state.getGammaFw().put(sink, new ArrayList<>());
    state.getGammaBw().put(source, new ArrayList<>());
    if (isHeuristic) {
      state.getGammaFw().put(source, new ArrayList<>());
      state.getGammaBw().put(sink, new ArrayList<>());
      for (short node = 1; node <= inst.getSize(); node++) {
        List<Label> gammaFw = new ArrayList<>();
        if (!state.isForbidden(source, node)) {
          gammaFw.add(new HeuristicLabel(node, inst, state, true));
        }
        state.getGammaFw().put(node, gammaFw);
        List<Label> gammaBw = new ArrayList<>();
        if (!state.isForbidden(node, sink)) {
          gammaBw.add(new HeuristicLabel(node, inst, state, false));
        }
        state.getGammaBw().put(node, gammaBw);
      }
    } else {
      List<Label> gammaFwSource = new ArrayList<>();
      if (inst.getOptions().isClassic()) {
        gammaFwSource.add(ClassicLabel.sourceLabel(inst));
      } else {
        gammaFwSource.add(ExactLabel.sourceLabel(inst));
      }
      state.getGammaFw().put(source, gammaFwSource);
      
      List<Label> gammaBwSink = new ArrayList<>();
      if (inst.getOptions().isClassic()) {
        gammaBwSink.add(ClassicLabel.sinkLabel(inst));
      } else {
        gammaBwSink.add(ExactLabel.sinkLabel(inst));
      }
      state.getGammaBw().put(sink, gammaBwSink);
      
      for (short node = 1; node <= inst.getSize(); node++) {
        state.getGammaFw().put(node, new ArrayList<>());
        state.getGammaBw().put(node, new ArrayList<>());
      }
    }
  }

  /**
   * Attempts to insert a newly created label in the respective set by applying all possible 
   * domination tests.
   *  
   * @param inst the problem instance
   * @param state the state of the algorithm
   * @param newLabel the newly created label for which to attempt insertion
   * @return true iff the set Gamma has changed after trying to insert the label
   */
  public static boolean eff(Instance inst, PricingState state, 
      Label newLabel, Espprc.DomType dt) {
    if (newLabel == null) {
      return false;
    }
    List<Label> list = (newLabel.isFw()) 
        ? state.getGammaFw().get(newLabel.getNode())
        : state.getGammaBw().get(newLabel.getNode());
    for (Iterator<Label> iterator = list.iterator(); iterator.hasNext();) {
      Label oldLabel = iterator.next();
      if (oldLabel.dominates(inst, state, newLabel, dt)) {
        oldLabel.dominates(inst, state, newLabel, dt);
        return false;
      } else if (newLabel.dominates(inst, state, oldLabel, dt)) {
        iterator.remove();
      }
    }
    list.add(newLabel);
    return true;
  }
  
  /**
   * Performs the label extension subroutine needed for bidirectional bounded dynamic programming.
   * 
   * <p>More in detail, performs all possible (forward or backward) label extensions from a selected
   * node, and updates the relevant sets.
   *  
   * @param inst the problem instance
   * @param state the ESPPRC state
   * @param from the node from which to perform the extensions
   * @param isForward whether the extensions are performed forwards or backwards 
   */
  public static void labelExtension(Instance inst, PricingState state, Options options, 
      short from, boolean isForward, Espprc.DomType dt) {
    List<Label> gammaFrom = isForward ? state.getGammaFwAt(from) : state.getGammaBwAt(from);
    
    for (Label label : gammaFrom) {
      if (!label.isExt() && label.getDuration() < inst.getPar().getMaxDuration() / 2) {
        label.setExt(true);
        Iterable<DirectedEdge> edges = isForward ? inst.getG().adj(from) : inst.getG().adjTo(from);
        for (DirectedEdge edge : edges) {
          // this takes a lot of computation time, need to enforce differently
          // if (tl != null && tl.elapsedTime() > options.getTimeLimit()) {
          //   return;
          // }
          if (!state.isForbidden((short)edge.from(), (short)edge.to())) {
            short depot = (short) (isForward ? inst.getSize() + 1 : 0);
            short to = (short) (isForward ? edge.to() : edge.from());
            if (to != depot && (!state.doesThetaContain(to) || label.isReachable(to))) {
              Label newLabel = label.extend(inst, state, edge); // newLabel = null if not feasible
              if (eff(inst, state, newLabel, dt)) {
                state.addNodeToExamine(to);
              }
            } 
          }
        }
      }
    }
  }

  /**
   * Generates feasible solutions from two sets of forward and backward labels by performing every 
   * possible two-vertex concatenation.
   * 
   * <p>A two-vertex concatenation considers a forward label on vertex i and a backward label on
   * vertex j and concatenates them on arc i -> j.
   * 
   * @param inst the problem instance
   * @param state the state of the ESPPRC algorithm
   */
  public static boolean concatenateAllLabels(Instance inst, 
      PricingState state, boolean isHeuristic) {
    //int thr = state.getOpt().getConcatStop(state.isRootNode()) * inst.getSize();
    int thr = state.getConcatStop();
    // List<Integer> randStart = shuffledList(inst.getSize());
    // for (int from : randStart) {
    for (short from = 0; from < inst.getSize() + 1; from++) {
      for (DirectedEdge edge : inst.getG().adj(from)) {
        short to = (short) edge.to();
        if (!state.isForbidden(from, to)) {
          for (Label left : state.getGammaFw().get(from)) {
            for (Label right : state.getGammaBw().get(to)) {
              // this takes a lot of computation time
              // if (tl != null && tl.elapsedTime() > state.getOpt().getTimeLimit()) {
              //   return false;
              // }
              if (Thread.interrupted()) {
                return false;
              }
              // inexpensive check to prune out some infeasible concatenations
              if (right.isReachable(from) && left.isReachable(to)) {
                Path.Values values = Lib.pathValues(inst, state, left, right);
                if (values != null) {
                  if (Lib.feasible(inst, state, left, right, values)
                      && Lib.halfway(inst, left, right, values)
                      ) {
                    boolean compress = inst.getOptions().isClassic() ? false : isHeuristic;
                    Path path = Lib.createPath(left, right, values, inst, compress);
                    if (//path.getReducedCost() >= state.getDssrLowerBound()
                        //&&
                        path.getReducedCost() < -Lib.TOLERANCE2) {
                      state.getPaths().add(path);
                      if (state.getPaths().size() >= thr) {
                        return true;
                      }
                    }
                  }
                }
              }
            }
          } 
        }
      }
    }
    return true;
  }
  
  // Returns a shuffled list of integers from 0 to size, included.
  static List<Integer> shuffledList(int size) {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < size + 1; i++) {
      list.add(i);
    }
    Collections.shuffle(list);
    return list;
  }
  
  /**
   * Computes data associated to a path obtained by performing a two-vertex concatenation on a 
   * forward and backward label. If the reduced cost of the resulting path is >= 0, returns null.
   * 
   * <p>In particular it derives how much we can shift the two partial paths represented by the 
   * labels, in order to minimize waiting times.
   * 
   * <p>Note: this function does not perform a feasibility check on the resulting path values, or
   * the associated path. However, the function responsible for that needs the values returned by 
   * this function.
   * 
   * @param inst the problem instance
   * @param state the state of the ESPPRC algorithm
   * @param fw the forward label used for concatenation
   * @param bw the backward label used for concatenation
   * @return the data of the path resulting from concatenation
   */
  public static Path.Values pathValues(Instance inst, PricingState state, 
      Label left, Label right) {
    if (left.getClass() == ExactLabel.class) {
      return Lib.pathValues(inst, state, (ExactLabel)left, (ExactLabel)right);
    } else if (left.getClass() == HeuristicLabel.class) {
      return Lib.pathValues(inst, state, (HeuristicLabel) left, (HeuristicLabel) right);
    } else if (left.getClass() == ClassicLabel.class)  {
      return Lib.pathValues(inst, state, (ClassicLabel) left, (ClassicLabel) right);
    }  else {
      throw new IllegalArgumentException();
    }
  }
  
  private static Path.Values pathValues(Instance inst, PricingState state,
      ClassicLabel fw, ClassicLabel bw) {
    double tij = inst.getG().edge(fw.getNode(), bw.getNode()).weight();
    double lrc = fw.getRedCost();
    double rrc = bw.getRedCost();
    double totrc = lrc + rrc + tij - state.getPricesRs(fw.getNode(), bw.getNode());
    // we don't care about paths with nonnegative reduced cost
    if (totrc >= 0 - TOLERANCE) {
      return null;
    }
    double ldur = fw.getDuration();
    double rdur = bw.getDuration();
    double svti = inst.getServiceTime(fw.getNode());
    double svtj = inst.getServiceTime(bw.getNode());
    double totdur = ldur + svti + tij + svtj + rdur;
    // we can already discard paths that take too long
    if (totdur > inst.getMax() + TOLERANCE) {
      return null;
    }
    double ldist = fw.getTotalDistance();
    double rdist = bw.getTotalDistance();
    double totdist = ldist + rdist + tij;
   
    return new Path.Values(totrc, totdist, totdur, totdist, 0, 0);
  }

  private static Path.Values pathValues(Instance inst, PricingState state, 
      ExactLabel fw, ExactLabel bw) {
    double alpha = inst.getPar().getDistUnitCost();
    double beta  = inst.getPar().getTimeUnitCost();
    // forward time slack of the first partial path
    double slack1 = fw.getTotalTravel() - fw.getEst() + fw.getLst();
    double f1 = Math.max(0, slack1);
    // waiting time of the first partial path
    double w1 = Math.max(0, -slack1);
    // backward time slack of the second partial path
    double slack2 = bw.getTotalTravel() - bw.getEst() + bw.getLst();
    double b2 = Math.max(0, slack2);
    // waiting time of the second partial path
    double w2 = Math.max(0, -slack2);
    // Savelsbergh's lambda
    double tij = inst.getG().edge(fw.getNode(), bw.getNode()).weight();
    double lambda = fw.getEst() + tij + inst.getSvt()[fw.getNode()] - inst.getMax() + bw.getEst();
    // total forward slack
    double totF = (fw.getNode() == 0) ? b2 : Math.min(f1, w1 - lambda);
    // total waiting time
    double totWait = w2;
    if (bw.getNode() == inst.getSize() + 1) {
      totWait = w1;
    } else if (fw.getNode() != 0) {
      if (lambda >= 0) {
        if (w2 == 0) {
          totWait = w1;
        } else {
          totWait = w1 + Math.max(0, w2 - lambda);
        }
      } else {
        if (w2 == 0) {
          totWait = w1 + Math.max(0, -lambda - b2);
        } else {
          totWait = w1 + w2 - lambda;
        }
      }
    }
    // how much can we reduce the total waiting?
    double adjWait = Math.max(0, totWait - totF);
    // total distance travelled
    double totDistance = tij + fw.getTotalDistance() + bw.getTotalDistance();
    // total route duration
    double totTravelTime = fw.getTotalTravel() + bw.getTotalTravel() 
                         + tij + inst.getSvt()[fw.getNode()] + adjWait;
    // total path cost
    double totalStaticCost = fw.getSCost() + bw.getSCost() + alpha * tij;
    double totalReducedCost = totalStaticCost + beta * totTravelTime;
    // we don't care about paths with nonnegative reduced cost
    if (totalReducedCost >= 0 - TOLERANCE) {
      return null;
    }
    // window for optimal start from the depot
    double t0 = Math.min(fw.getEst() - fw.getTotalTravel(), fw.getLst());
    double t0Alt = 
        inst.getTwe()[bw.getNode()] + inst.getSvt()[bw.getNode()] - bw.getDuration() - totF;
    double start1 = 
        (fw.getNode() != 0 ? t0 : t0Alt) + Math.min(totF, totWait); //TODO check if correct
    double start2 = (fw.getNode() != 0 ? t0 : t0Alt) + totF;
    double actualCost = alpha * totDistance + beta * totTravelTime;
    return new Path.Values(totalReducedCost, actualCost, totTravelTime, 
        totDistance, start1, start2);
  }

  private static Path.Values pathValues(Instance inst, PricingState state, 
      HeuristicLabel left, HeuristicLabel right) {
    double start = left.getStartTime();
    double linkDist = inst.getDistance(left.getNode(), right.getNode());
    double linkTime = Math.max(linkDist + inst.getServiceTime(left.getNode()),
        inst.getTwsOf(right.getNode()) - left.getArrivalTime());
    double linkCost = inst.getPar().getDistUnitCost() * linkDist
        + inst.getPar().getTimeUnitCost() * linkTime;
    double dist = left.getTotalDistance() + right.getTotalDistance() + linkDist;
    double time = left.getDuration() + right.getDuration() + linkTime;
    double cost = left.getCost() + right.getCost() + linkCost;
    double redCost = left.getRedCost() + right.getRedCost() + linkCost;
    if (redCost >= 0 - TOLERANCE) {
      return null;
    }
    return new Path.Values(redCost, cost, time, dist, start, start);
  }
  
  /**
   * Checks wheter the concatenation of a forward and backward label (with associated values)
   * results in a feasible path.
   * 
   * @param inst the problem instance
   * @param state the state of the ESPPRC algorithm
   * @param left the forward label used for the concatenation
   * @param right the backward label used for the concatenation
   * @param values the values associated to the path resulting from the concatenation 
   * @return true if and only if the concatenation is feasible
   */
  public static boolean feasible(Instance inst, PricingState state, Label left, Label right,
      Path.Values values) {
    if (left.getClass() == ExactLabel.class) {
      return Lib.feasible(inst, state, (ExactLabel) left, (ExactLabel) right, values);
    } else if (left.getClass() == HeuristicLabel.class) {
      return Lib.feasible(inst, state, (HeuristicLabel) left, (HeuristicLabel) right, values);
    } else if (left.getClass() == ClassicLabel.class) {
      return Lib.feasible(inst, state, (ClassicLabel) left, (ClassicLabel) right, values); 
    } else {
      throw new IllegalArgumentException();
    }
  }
  
  private static boolean feasible(Instance inst, PricingState state, 
      ClassicLabel left, ClassicLabel right, Path.Values values) {
    // feasibility w/r to the capacity
    boolean delFlag = left.getDel() + right.getDel() <= inst.getPar().getMaxCapacity();
    // feasibility w/r to elementarity on critical nodes
    boolean elemFlag = isLegalConcat(left, right, state.getTheta(), state);
    return delFlag && elemFlag;
  }
  
  private static boolean feasible(Instance inst, PricingState state, 
      HeuristicLabel left, HeuristicLabel right, Path.Values values) {
    double linkTime = inst.getDistance(left.getNode(), right.getNode())
        + inst.getServiceTime(left.getNode());
    boolean timeFlag = left.getArrivalTime() + linkTime <= inst.getTweOf(right.getNode()); 
    // feasibility w/r to the capacity
    boolean delFlag = left.getDel() + right.getDel() <= inst.getPar().getMaxCapacity();

    // feasibility w/r to the total route duration
    boolean shiftFlag = values.getTravelTime() <= inst.getPar().getMaxDuration();

    // feasibility w/r to elementarity on critical nodes
    boolean elemFlag = isLegalConcat(left, right, state.getTheta(), state);

    return timeFlag && delFlag && shiftFlag && elemFlag;
  }
  
  private static boolean feasible(Instance inst, PricingState state, 
      ExactLabel left, ExactLabel right, Path.Values values) {
    // feasibility w/r to the time window in j
    double edge = inst.getG().edge(left.getNode(), right.getNode()).weight();
    double earliestServTime = Math.max(inst.getTws()[right.getNode()], 
        left.getEst() + inst.getSvt()[left.getNode()] + edge);
    double latestServTime = inst.getMax() - right.getEst();
    boolean timeFlag = earliestServTime <= latestServTime;

    // feasibility w/r to the capacity
    boolean delFlag = left.getDel() + right.getDel() <= inst.getPar().getMaxCapacity();

    // feasibility w/r to the total route duration
    boolean shiftFlag = values.getTravelTime() <= inst.getPar().getMaxDuration();

    // feasibility w/r to elementarity on critical nodes
    boolean elemFlag = isLegalConcat(left, right, state.getTheta(), state);

    return timeFlag && delFlag && shiftFlag && elemFlag;
  }

  private static boolean isLegalConcat(Label left, Label right, 
      Set<Short> theta, PricingState state) {
    boolean ngPlus = state.plusSecondPhase();
    boolean visited = false;
    //Iterable<Short> iterable = ngPlus ? left.getSequence() : left.getVisited();
    if (ngPlus) {
      for (short node : left.getSequence()) {
        // we use isReachable() but should we just use isVisited()?
        visited = ngPlus ? right.getSequence().contains(node) : right.isVisited(node);
        if (theta.contains(node) && visited) { // TODO there's a BUG HERE!
          return false;
        }
      } 
    } else {
      BitSet bitSet = left.getVisited();
      for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet.nextSetBit(i + 1)) {
        short node = (short) i;
        visited = ngPlus ? right.getSequence().contains(node) : right.isVisited(node);
        if (theta.contains(node) && visited) { // TODO there's a BUG HERE!
          return false;
        }
      }
    }
    //iterable = ngPlus ? right.getSequence() : right.getVisited();
    if (ngPlus) {
      for (short node : right.getSequence()) {
        // we already checked the left visited ones
        visited = ngPlus ? left.getSequence().contains(node) : left.isVisited(node);
        if (theta.contains(node) && visited) {
          return false;
        }
      } 
    } else {
      BitSet bitSet = right.getVisited();
      for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet.nextSetBit(i + 1)) {
        short node = (short) i;
        visited = ngPlus ? left.getSequence().contains(node) : left.isVisited(node);
        if (theta.contains(node) && visited) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * Determines if the path resulting from the two labels meets the halfway condition.
   * 
   * @param inst the problem instance
   * @param left the left side label
   * @param right the right side label
   * @param values the values of the resulting path
   * @return whether the concatenation meets the halfway condition or not
   */
  static boolean halfway(Instance inst, Label left, Label right, Path.Values values) {
    double consFw = left.getDuration();
    double consBw = right.getDuration();
    if (consFw == consBw) {
      return true;
    }
    double bnd = inst.getOptions().isClassic() ? inst.getMax() : inst.getPar().getMaxDuration();
    double phi = Math.abs(consFw - consBw);
    if (consFw < consBw) {
      double consBwNext = right.getMother() == null ? 0 : right.getMother().getDuration();
      // partial forward extension
      double consFwNext = (left.getNode() == right.getNode()) 
          ? getNewResourceConsumption(inst, left, right.getMother().getNode()) 
          : getNewResourceConsumption(inst, left, right.getNode());
          
      // there's not gonna be a duplicate if the label is not created because of bounding
      if (consFwNext > (bnd * .5)) {
        return true; 
      }
      double phiNext = Math.abs(consFwNext - consBwNext);
      return phi < phiNext;
    } else {
      double consFwPrev = left.getMother() == null ? 0 : left.getMother().getDuration();
      // partial backward extension
      double consBwPrev = (left.getNode() == right.getNode()) 
          ? getNewResourceConsumption(inst, right, left.getMother().getNode()) 
          : getNewResourceConsumption(inst, right, left.getNode());
      // same as above   
      if (consBwPrev > (bnd * .5)) {
        return true; 
      }
      double phiPrev = Math.abs(consFwPrev - consBwPrev);
      return phi <= phiPrev;
    }
  }

  /**
   *  Creates a path from two labels and the computed values.
   *  
   * @param left the left side label
   * @param right the right side label
   * @param values the values of the resulting path
   * @return a solution for the ESPPRC
   */
  public static Path createPath(Label left, Label right, 
      Path.Values values, Instance inst, boolean compress) {
    if (values == null) {
      return null;
    }
    // concatenate the subsequences
    List<Short> finalSequence = new ArrayList<>(left.getPartialPath());
    
    for (int k = right.getPartialPath().size() - 1; k >= 0; --k) {
      short node = right.getPartialPath().get(k);
      finalSequence.add(node);
    }
    return new Path(finalSequence, values, inst, compress, left, right);
  }
  
  /**
   * Returns false if there are no multiple visits / the DSSR loop can break.
   * 
   * @param path the path to check for violations
   * @param pstate the state of the algorithm
   * @return whether the check identified multiple visits or not
   */
  public static boolean multipleVisits(Path path, PricingState pstate, boolean firstPath) {
    if (path.isElementary()) {
      return false;
    }
    switch (pstate.getOpt().getAlgo()) {
      case HYBRID2: case HYBRID2_PLUS:
        return mvLocal(path, pstate, true, firstPath);
      case DSSR_L:
        return mvLocal(path, pstate, false, firstPath);
      case HYBRID1: case HYBRID1_PLUS:
        return mvGlobal(path, pstate, true, firstPath);
      case DSSR_G:
        return mvGlobal(path, pstate, false, firstPath);
      default:
        return false;
    }
  }
  
  // checks for violations and updates the applied ng-sets only.
  // returns true iff there are violations
  static boolean mvLocal(Path path, PricingState state, boolean useNg, boolean firstPath) {
    Set<Short> visited = new HashSet<>();
    List<Short> list = path.getSequence();
    boolean invalid = false;
    for (short i = 1; i < list.size() - 1; i++) {
      short node = list.get(i);
      if (state.getOpt().usingNgSets() && useNg && !state.plusSecondPhase()) {
        visited.retainAll(state.getNgSetAt(node));
      }
      if (visited.contains(node)) {
        invalid = true;
        Map<Short, Set<Short>> map = state.getOpt().usingNgSets() 
            ? state.getTempNgSets() : state.getLocalThetas();
        Lib.updateLocalSets(map, list, node, i);
      } else {
        visited.add(node);
      }
    }
    if (invalid) {
      return true;
    } else if (state.getOpt().usingPlus() && useNg && firstPath) {
      state.switchPlus();
      return mvLocal(path, state, false, firstPath);
    } else {
      return false;
    }
  }
  
  // checks for violations and updates the global critical set
  // returns true iff there are violations
  static boolean mvGlobal(Path path, PricingState state, boolean useNg, boolean firstPath) {
    Map<Short, Short> countMap = new HashMap<>();
    Set<Short> visited = new HashSet<>();
    int last = path.getSequence().get(path.getSequence().size() - 1);
    for (short node : path.getSequence()) {
      if (node != 0 && node != last && state.getOpt().usingNgSets() 
          && useNg && !state.plusSecondPhase()) {
        visited.retainAll(state.getNgSetAt(node));
      }
      keepCount(node, visited, countMap);
    }
    // at this point count contains all multiple nodes, with respective count
    if (countMap.isEmpty()) {
      if (state.getOpt().usingPlus() && useNg && firstPath) {
        state.switchPlus();
        return mvGlobal(path, state, false, firstPath);
      } else {
        return false;
      }
    }
    int numOfEntries = countMap.size();
    int numOfNodesToInclude = numOfEntries;
    switch (state.getDssrInsNodeStr()) {
      case ONE_NODE:
        numOfNodesToInclude = 1;
        countMap = sortByValue(countMap); // TODO need to check correct order of entries
        break;
      case IN_BETWEEN:
        double alpha = state.getDssrInsNodeAmount();
        numOfNodesToInclude = ratio(alpha, numOfEntries);
        countMap = sortByValue(countMap); // TODO need to check correct order of entries
        break;
      default:
        break;
    }
    int count = 0;
    for (Map.Entry<Short, Short> entry : countMap.entrySet()) {
      if (count == numOfNodesToInclude) {
        break;
      }
      state.getPsi().add(entry.getKey());
      count++;
    }
    return true;
  }
  
  private static void keepCount(short node, Set<Short> visited, Map<Short, Short> countMap) {
    if (!visited.contains(node)) {
      visited.add(node);
    } else {
      if (!countMap.containsKey(node)) {
        countMap.put(node, (short) 1);
      } else {
        short count = (short) (countMap.get(node) + 1);
        countMap.put(node, count);
      }
    }
  }
  
  private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
    Map<K,V> result = new LinkedHashMap<>();
    Stream<Entry<K,V>> st = map.entrySet().stream();
    st.sorted(Comparator.comparing(e -> e.getValue()))
        .forEachOrdered(e -> result.put(e.getKey(),e.getValue()));
    return result;
  }

  public static String roundToString(double num) {
    return String.format("%.2f", num);
  }
  
  public static double round(double value, int precision) {
    int scale = (int) Math.pow(10, precision);
    return Math.round(value * scale) / (double) scale;
  }
  
  public static double round(double value) {
    return round(value, Lib.PRECISION);
  }
  
  /** 
   * Checks whether a path is valid for the insertion in the LP.
   * 
   * @param path the path to check
   * @param state the state of the algorithm
   * @param options the options of the algorithm
   * @return whether the path is valid or not
   */
  public static boolean isValid(Path path, State state, Options options) {
    switch (options.getAlgo()) {
      case NG: case HYBRID1: case HYBRID2:
        return isNgValid(path, state);
      default:
        return path.isElementary();  
    }
  }
  
  /**
   * Checks if a path is elementary.
   * 
   * @param path the path to check
   * @return whether it is elementary or not.
   */
  public static boolean isElementary(Path path) {
    HashSet<Short> set = new HashSet<>();
    for (short node : path.getSequence()) {
      if (set.contains(node)) {
        return false;
      } else {
        set.add(node);
      }
    }
    return true;
  }
  
  /**
   * Checks if a path respects the ng-rules.
   * 
   * @param path the path to check
   * @param state the state of the algorithm
   * @return whether the path respects the rules or not
   */
  public static boolean isNgValid(Path path, State state) {
    HashSet<Short> set = new HashSet<>();
    for (short node : path.getSequence()) {
      if (node == 0 || node == state.size() + 1) {
        continue;
      } else {
        if (set.contains(node)) {
          return false;
        } else {
          Set<Short> ng = state.getNgSetAt(node);
          set.retainAll(ng);
          set.add(node);
        }
      }
    }
    return true;
  }
  
  /**
   * Returns the highest cycling attractiveness of a node. This measure can be adjusted with a
   * weight which corresponds to the width of the node's time window.
   * 
   * @param inst the problem instance
   * @param node input node 
   * @param weighted wheter the measure has to be weighted or not
   * @return the highest cycling attractiveness of the node
   */
  public static double getHca(Instance inst, double[] prices, int node, boolean weighted) {
    double max = Double.NEGATIVE_INFINITY;
    for (int i = 1; i <= inst.getSize(); i++) {
      if (i != node) {
        max = Math.max(max, getAttractiveness(inst, prices, node, i));
      }
    }
    return weighted ? max * (inst.getTwe()[node] - inst.getTws()[node]) : max;
  }
  
  /**
   * Returns the total cycling attractiveness of a node. This measure can be adjusted with a
   * weight which corresponds to the width of the node's time window.
   * 
   * @param inst the problem instance
   * @param node input node 
   * @param weighted wheter the measure has to be weighted or not
   * @return the total cycling attractiveness of the node
   */
  public static double getTca(Instance inst, double[] prices, int node, boolean weighted) {
    double tot = 0;
    for (int i = 1; i < inst.getSize(); i++) {
      if (i != node) {
        tot += getAttractiveness(inst, prices, node, i);
      }
    }
    return weighted ? tot * (inst.getTwe()[node] - inst.getTws()[node]) : tot;
  }
  
  private static double getAttractiveness(Instance inst, double[] prices, int node, int aux) {
    return prices[node] 
        / (inst.getSvt()[node] + inst.getSvt()[aux] + 2 * inst.getDistance(node, aux));
  }
  
  private static double getAdjWeight(Instance inst, int from, int to) {
    return inst.getG().edge(from, to).weight() + inst.getSvt()[from];
  }

  private static double getNewResourceConsumption(Instance inst, Label label, int node) {
    if (label.getClass() == ExactLabel.class) {
      return Lib.getNewResourceConsumption(inst, (ExactLabel) label, node);
    } else if (label.getClass() == HeuristicLabel.class) {
      return Lib.getNewResourceConsumption(inst, (HeuristicLabel) label, node);
    } else if (label.getClass() == ClassicLabel.class) {
      return Lib.getNewResourceConsumption(inst, (ClassicLabel) label, node);
    } else {
      throw new IllegalArgumentException();
    }
  }
  
  private static double getNewResourceConsumption(Instance inst, ClassicLabel label, int node) {
    DirectedEdge edge = label.isFw() 
        ? inst.getG().edge(label.getNode(), node) : inst.getG().edge(node, label.getNode());
    return label.newDuration(inst, edge);
  }
  
  // infers the resource consuption values for the halfway method
  private static double getNewResourceConsumption(Instance inst, ExactLabel label, int node) {
    double tws = label.isFw() ? inst.getTws()[node] : inst.getTwsBw()[node];
    double twe = label.isFw() ? inst.getTwe()[node] : inst.getTweBw()[node];
    double barT = label.isFw() 
        ? getAdjWeight(inst, label.getNode(), node) 
        : getAdjWeight(inst, node, label.getNode());
    double ttnew = label.getTotalTravel() + barT;
    double estnew = Math.max(tws, label.getEst() + barT);
    double lstnew = Math.min(label.getLst(), twe - ttnew);
    return Math.max(ttnew, estnew - lstnew);
  }
  
  private static double getNewResourceConsumption(Instance inst, HeuristicLabel label, int node) {
    double[] tws = label.isFw() ? inst.getTws() : inst.getTwsBw();
    double barT = label.isFw() 
        ? getAdjWeight(inst, label.getNode(), node) 
        : getAdjWeight(inst, node, label.getNode());
    return Math.max(label.getDuration() + barT, tws[node] - label.getStartTime());
  }
  
  /**
   * Uses deleted arcs to determine that the destination nodes of those arcs are literally 
   * unreachable for the newly created label.
   * 
   * @param inst the problem instance
   * @param state the algorithm state
   * @param newL the newly created label
   * @param newNode the node on which the label is created
   */
  public static void markDeletedAsInfeasible(Instance inst, PricingState state, 
      Label newL, int newNode) {
    for (short k = 1; k < inst.getSize(); k++) {
      if (k != newNode) {
        if (newL.isFw()) {
          if (inst.getG().edge(newNode, k) == null) {
            newL.addInfeasible(k);
          }
        } else {
          if (inst.getG().edge(k, newNode) == null) {
            newL.addInfeasible(k);
          }
        }
      }
    }
  }
  
  /**
   * Compares the unreachable vertices of two labels to determine dominance.
   * 
   * @param l1 the challenging label
   * @param l2 the opponent label
   * @param state the state of the algorithm
   * @return two boolean values: the first indicates if there is dominance,
   *     the second whether the dominance is strong
   */
  public static boolean[] compareUnreachable(Label l1, Label l2, PricingState state) {
    final Set<Short> set = getDominanceSubset(l1.getNode(), state);
    boolean one = true;
    boolean two = false;
    for (short i : set) {
      boolean s1 = l1.isVisited(i) || l1.isInfeasible(i);
      boolean s2 = l2.isVisited(i) || l2.isInfeasible(i);
      if (s1 && !s2) {
        // certainty that l1 does not dominate l2
        return new boolean[] { false, false };
      }
      if (!s1 && s2) {
        two |= true;
      }
    }
    return new boolean[] { one, two };
  }
  
  private static Set<Short> getDominanceSubset(short node, PricingState state) {
    Set<Short> set;
    switch (state.getOpt().getAlgo()) {
      case DSSR_L:
        set = state.getLocalTheta(node);
        break;
      case NG:
        set = state.getNgSetAt(node);
        break;
      case HYBRID1: case HYBRID1_PLUS:
        set = new HashSet<>(state.getNgSetAt(node));
        set.retainAll(state.getTheta());
        break;
      case HYBRID2: case HYBRID2_PLUS:
        set = state.getTempNgSetAt(node);
        break;
      default:
        set = state.getTheta();
        break;  
    }
    return set;
  }
  
  /**
   * Forget the nodes that are not in the current ng set.
   * 
   * @param pstate the algorithm state
   * @param newL the newly created label
   * @param newNode the node on which the new label is created
   */
  public static void updateVisited(PricingState pstate, 
      Label oldL, Label newL, final short newNode) {
    //newL.addVisited(newNode);
    if (pstate.getOpt().usingNgSets()) {
      boolean nd = pstate.getOpt().usingTempNgSets();
      // for (short node : oldL.getVisited()) {
      BitSet bitSet = oldL.getVisited();
      for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet.nextSetBit(i + 1)) {
        short node = (short)i;
        Set<Short> ngSet = nd ? pstate.getTempNgSetAt(newNode) : pstate.getNgSetAt(newNode);
        if (ngSet.contains(node)) {
          newL.addVisited(node);
        }
      }
      //for (short node : oldL.getInfeasible()) {
      bitSet = oldL.getInfeasible();
      for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet.nextSetBit(i + 1)) {
        short node = (short)i;
        Set<Short> ngSet = nd ? pstate.getTempNgSetAt(newNode) : pstate.getNgSetAt(newNode);
        if (ngSet.contains(node)) {
          newL.addInfeasible(node);
        }
      }
    } else { 
      newL.getVisited().or(oldL.getVisited());
      //newL.addAllVisited(oldL.getVisited());
      // newL.addAllInfeasible(oldL.getInfeasible()); TODO is this really the problem??????
    }
  }
  
  public static void updateLocalSets(Map<Short, Set<Short>> setMap, 
      List<Short> list, short node, short start) {
    setMap.get(node).add(node);
    for (int i = start - 1; i >= 1 && list.get(i) != node; i--) {
      short currentNode = list.get(i);
      Set<Short> set = setMap.get(currentNode);
      set.add(node);
    }
  }
  
  // this function works! (hopefully)
  public static Set<Short> buildNgSet(Instance inst, Options options, 
      short node, boolean onRootNode) {
    if (node == 0 || node == inst.getSize() + 1) {
      return new HashSet<>(allNodes(inst));
    }
    ToDoubleBiFunction<Short, Short> measure;
    switch (options.getNgSetType(onRootNode)) {
      case TRAVEL_TIME:
        measure = (n1, n2) -> inst.getFeasibleDistance(n1, n2);
        break;
      case CHEAP_CYCLE_RISK:
        measure = (n1, n2) -> inst.getCheapCycleRisk(n1, n2);
        break;
      case MIXED:
        measure = (n1, n2) -> {
          double alpha = options.getNgMixAlpha(onRootNode);
          return alpha * inst.getDistance(n1, n2) + (1 - alpha) * inst.getCheapCycleRisk(n1, n2);
        };
        break;
      default:
        measure = (n1, n2) -> inst.getDistance(n1, n2);
    }
    return buildNgSet(inst, options, measure, node, onRootNode);
  }

  public static Set<Short> buildNgSet(Instance inst, Options options, 
      ToDoubleBiFunction<Short, Short> measure, short node, boolean onRootNode) {
    Set<Short> set = new HashSet<>();
    set.add(node);
    Map<Short, Double> table = new HashMap<>();
    for (short i = 1; i <= inst.getSize(); i++) {
      if (i != node) {
        table.put(i, measure.applyAsDouble(i, node));
      }
    }
    PriorityQueue<Map.Entry<Short, Double>> pq = new PriorityQueue<>(11, new MinValue());
    pq.addAll(table.entrySet());
    for (short i = 0; i < getNgSize(inst, options, onRootNode) - 1; i++) {
      short polled = pq.poll().getKey();
      if (table.get(polled) != Double.POSITIVE_INFINITY) {
        set.add(polled);
      }
    }
    return set;
  }
  
  // TODO change when adopting new options class
  public static int getNgSize(Instance inst, Options options, boolean onRootNode) {
    // return options.getNgSize();
    double amount = options.getNgSetSize(onRootNode);
    return Lib.ratio(amount, inst.getSize());
  }

  public static class MinValue implements Comparator<Map.Entry<Short, Double>> {
    @Override
    public int compare(Map.Entry<Short, Double> e1, Map.Entry<Short, Double> e2) {
      return Double.compare(e1.getValue(), e2.getValue());
    }
  }
  
  public static Set<Short> allNodes(Instance inst) {
    Set<Short> set = new HashSet<>();
    for (short i = 1; i <= inst.getSize(); i++) {
      set.add(i);
    }
    return set;
  }
  
  public static double[] readDualPricesFromFile(In in, int num) {
    double[] arr = in.readAllDoubles();
    return arr;
  }
  
  public static double[] readDualPrices(double[] src, int num) {
    double[] pcs = new double[num + 2];
    System.arraycopy(src, 0, pcs, 1, num);
    return pcs;
  }
  
  public static void log(String str, StopwatchCPU st) {
    StdOut.println("[" + st.elapsedTime() + "] " + str);
  }
  
  public static void log(String str, Stopwatch st) {
    StdOut.println("[" + st.elapsedTime() + "] " + str);
  }
  
  public static Set<Path> getInitialSetOfPaths(Instance inst) {
    Set<Path> set = new HashSet<>();
    for (short node = 1; node <= inst.getSize(); node++) {
      List<Short> seq = new ArrayList<>();
      seq.add((short) 0);
      seq.add(node);
      seq.add((short) (inst.getSize() + 1));
      Path path = new Path(seq, inst);
      set.add(path);
    }
    return set;
  }
  
  /**
   * Returns the closest integer to ratio * num.
   * If the product is smaller than 1, it returns 1.
   * 
   * @param ratio the percentage
   * @param num the input number
   * @return the closest integer
   */
  public static int ratio(double ratio, int num) {
    double prod = ratio * num;
    int ans = (int) Math.floor(prod);
    if (prod - ans >= 0.5) {
      ans = (int) Math.ceil(prod);
    }
    if (ans < 1) {
      return 1;
    }
    return ans;
  }
  
  public static double arrSum(double[] arr) {
    double ans = 0;
    for (int i = 0; i < arr.length; i++) {
      ans += arr[i];
    }
    return ans;
  }
  
  // doesn't work yet - no need to make it work for now
  protected static String formatSeconds(double seconds) {
    return (new SimpleDateFormat("hh:mm:ss")).format(new Date((long)seconds));
  }
  
  public static void writeToFile(String filename, String content) {
    Out out = new Out(filename);
    out.println(content);
    out.close();
  }

  public static Set<Short> toSet(BitSet infeasible) {
    // TODO Auto-generated method stub
    return null;
  }
  
  // this class should not be instantiated
  private Lib() { }
}
