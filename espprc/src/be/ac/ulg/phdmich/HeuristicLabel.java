package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Set;

class HeuristicLabel implements Label {
  private short node;
  private double reducedCost;
  private double cost;
  private double totalDistance;
  private double totalTravel;      // theta
  private double startTime;        // departure time from the depot
  private double arrivalTime;      // arrival time at the current node
  private double del;
  private BitSet visited; // nodes actually in the path
  //it is infeasible to extend a label to these nodes
  private BitSet infeasible; 
  // length of path sigma, with regards to non-critical nodes, as in Righini and Salani
  private int pathLength;         
  // other info
  private boolean isFw;
  private boolean isExt; // false iff it has never been considered for extension
  private HeuristicLabel mother;            
  private List<Short> partialPath = new ArrayList<>();
  
  /**
   * Creates a forward or backward label associated to a path from the depot to the node.
   * 
   * @param node the node to which extension is to perform
   * @param inst the problem instance
   * @param isForward whether the extension is forward or not
   */
  public HeuristicLabel(short node, Instance inst, PricingState state, boolean isForward) {
    this.node = node;
    double distance = inst.getDistance(0, node);
    double svt = isForward ? inst.getServiceTime(0) : inst.getServiceTime(node);
    double duration = svt + distance;
    this.cost = inst.getPar().getDistUnitCost() * distance 
        + inst.getPar().getTimeUnitCost() * duration;
    this.reducedCost = this.cost - state.getPrice(node);
    this.totalTravel = duration;
    this.totalDistance = distance;
    double[] tws = isForward ? inst.getTws() : inst.getTwsBw();
    this.startTime = tws[node] - duration;
    this.arrivalTime = tws[node];
    this.del = inst.getDem()[node];
    this.visited = new BitSet(inst.getSize() + 2);
    short depot = (short) (isForward ? 0 : inst.getSize() + 1);
    this.addVisited(depot);
    this.addVisited(node);
    this.infeasible = new BitSet(inst.getSize() + 2);
    Set<Short> theta = state.getOpt().usingLocalTheta() 
        ? state.getLocalTheta((short)0) : state.getTheta();
    this.pathLength = theta.contains(node) ? 0 : 1;
    this.isFw = isForward;
    this.isExt = false;
    this.mother = null;
    this.partialPath = new ArrayList<>();
    this.partialPath.add(depot);
    this.partialPath.add(node);
  }

  private HeuristicLabel() {}

  public double getStartTime() {
    return startTime;
  }
  
  public double getArrivalTime() {
    return arrivalTime;
  }
  
  public double getCost() {
    return cost;
  }
  
  @Override
  public double getRedCost() {
    return reducedCost;
  }
  
  @Override
  public boolean isExt() {
    return isExt;
  }
  
  public double getTotalDistance() {
    return totalDistance;
  }

  @Override
  public double getDuration() {
    return arrivalTime - startTime;
  }

  @Override
  public void setExt(boolean val) {
    isExt = val;
  }
  
  public void addInfeasible(short node) {
    //this.infeasible.add(node);
    this.infeasible.set(node);
  }

  @Override
  public Label extend(Instance inst, PricingState state, DirectedEdge edge) {
    HeuristicLabel newL = new HeuristicLabel();
    final short newNode = (short) (isFw ? edge.to() : edge.from());
    // experimental
    if (isInfeasible(newNode)) {
      return null;
    }
    // we check if the node is reachable and mark it
    Set<Short> theta = state.getOpt().usingLocalTheta() 
        ? state.getLocalTheta(newNode) : state.getTheta();
    if (theta.contains(newNode)) {
      //if (visited.contains(newNode)) { 
      if (!isReachable(newNode)) {
        return null;
      }
      newL.addVisited(newNode);
    } else {
      newL.pathLength++;
    }
    // "untravellable" arcs - lead to unreachable nodes
    if (edge.weight() == Double.POSITIVE_INFINITY) {
      return markInfeasible(state, newNode);
    }
    
    // Capacity check
    newL.del = del + inst.getDemand(newNode);
    if (newL.del > inst.getPar().getMaxCapacity()) {
      return markInfeasible(state, newNode);
    }
    // Time-related computations
    final double[] tws = isFw ? inst.getTws() : inst.getTwsBw();
    final double[] twe = isFw ? inst.getTwe() : inst.getTweBw();
    final double barT = edge.weight() + inst.getServiceTime(edge.from());
    
    newL.totalTravel = totalTravel + barT;
    newL.startTime = this.startTime;
    double minArrivalTime = this.arrivalTime + barT;
    if (minArrivalTime > twe[newNode] + Lib.TOLERANCE) { 
      return markInfeasible(state, newNode);
    }
    newL.arrivalTime = Math.max(tws[newNode], minArrivalTime);
    
    // Infeasible w/r to duration
    if (newL.getDuration() > inst.getPar().getMaxDuration()) {
      return markInfeasible(state, newNode);
    }
    // Bounding
    if (newL.getDuration() > (inst.getPar().getMaxDuration() * .5 + Lib.TOLERANCE)) {
      return null;
    }
    // Cost
    newL.totalDistance = this.totalDistance + edge.weight();
    double increase = inst.getPar().getDistUnitCost() * edge.weight()
        + inst.getPar().getTimeUnitCost() * (newL.getDuration() - this.getDuration());
    newL.cost = this.cost + increase;
    newL.reducedCost = this.reducedCost - state.getPrice(newNode) + increase;

    Lib.markDeletedAsInfeasible(inst, state, newL, newNode);    
    Lib.updateVisited(state, this, newL, newNode);
    // finishing
    newL.partialPath = new ArrayList<>();
    newL.getPartialPath().addAll(partialPath);
    newL.getPartialPath().add(newNode);
    newL.mother = this;
    return newL;
  }
  
  private HeuristicLabel markInfeasible(PricingState state, short newNode) {
    addInfeasible(newNode);
    return null;
  }
  
  public double getDel() {
    return del;
  }

  @Override
  public boolean isVisited(short node) {
    return visited.get(node);
  }

  @Override
  public boolean isFw() {
    return isFw;
  }

  @Override
  public boolean dominates(Instance inst, PricingState state,
      Label opponent, Espprc.DomType dt) {
    if (opponent.getClass() != this.getClass()) {
      throw new IllegalArgumentException();
    }
    HeuristicLabel that = (HeuristicLabel) opponent;
    boolean exper = (state.getOpt().getDomRule() == Options.DomRule.EXPER);
    boolean[] flag = {true, false};
    if (dt == Espprc.DomType.HEUR_1) {
      return (this.getRedCost() < that.getRedCost());
    } else {
      compareDel(that, flag);
      compareRedCost(that, flag);
      if (!exper) {
        compareDur(that, flag);
        compareElem(that, flag, state);
      }
      return flag[0] && flag[1];
    }
  }

  private void compareElem(HeuristicLabel that, boolean[] flag, PricingState state) {
    boolean[] val = Lib.compareUnreachable(this, that, state);
    flag[0] &= val[0];
    flag[1] |= val[1];
    if (state.getOpt().getAlgo() == Options.Algo.DSSR_G 
        || state.getOpt().getAlgo() == Options.Algo.DSSR_L
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1_PLUS) {
      flag[0] &= this.pathLength <= that.pathLength + Lib.TOLERANCE;
      flag[1] |= this.pathLength < that.pathLength - Lib.TOLERANCE;
    }
  }

  private void compareDur(HeuristicLabel that, boolean[] flag) {
    flag[0] &= this.getDuration() <= that.getDuration() + Lib.TOLERANCE;
    flag[1] |= this.getDuration() < that.getDuration() - Lib.TOLERANCE;
  }

  private void compareRedCost(HeuristicLabel that, boolean[] flag) {
    flag[0] &= this.reducedCost <= that.reducedCost + Lib.TOLERANCE;
    flag[1] |= this.reducedCost < that.reducedCost - Lib.TOLERANCE;
  }

  private void compareDel(HeuristicLabel that, boolean[] flag) {
    flag[0] &= this.getDel() <= that.getDel() + Lib.TOLERANCE;
    flag[1] |= this.getDel() < that.getDel() - Lib.TOLERANCE;
  }

  @Override
  public short getNode() {
    return node;
  }

  @Override
  public boolean isReachable(short node) {
    return (!isVisited(node) && !isInfeasible(node));
  }

  @Override
  public BitSet getVisited() {
    return null;
  }
  
  @Override
  public List<Short> getSequence() {
    return partialPath;
  }

  @Override
  public boolean isInfeasible(short node) {
    return infeasible.get(node);
  }

  @Override
  public List<Short> getPartialPath() {
    return partialPath;
  }

  @Override
  public void addVisited(short node) {
    this.visited.set(node);
  }

  @Override
  public void addAllVisited(Collection<? extends Short> collection) {
    //this.visited.addAll(collection);
    for (short s : collection) {
      visited.set(s);
    }
  }

  @Override
  public BitSet getInfeasible() {
    return null;
  }

  @Override
  public void addAllInfeasible(Collection<? extends Short> collection) {
    //this.infeasible.addAll(collection);
    for (short s : collection) {
      infeasible.set(s);
    }
  }

  @Override
  public Label getMother() {
    return this.mother;
  }
  
  @Override
  public String toString() {
    return partialPath.toString();
  }
}
