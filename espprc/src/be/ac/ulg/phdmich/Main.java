package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.StdOut;

import java.util.List;

public class Main {

  private static final String USAGE = "phd [OPTIONS] FILENAME...";

  /**
   * Main program.
   * 
   * @param args program arguments
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    if (options.askHelp()) {
      options.printHelp(USAGE);
      return;
    }
    List<String> instances = options.getArgs();
    if (instances.isEmpty()) {
      StdOut.println("No instances provided.\n" + USAGE);
      return;
    }
    for (String i : instances) {
      //String pfile = "data/Param.txt";
      Instance instance = new Instance(i, options);
      String header = getHeader(args, instance);
      if (!options.isTuning()) {
        StdOut.println(header);
      }
      String out = "";
      String newline = System.getProperty("line.separator");
      Solution sol = null;
      switch (options.getProg()) {
        case CG:
          State state = new State(instance, options, true);
          ColumnGeneration cg = new ColumnGeneration(instance, state, options);
          sol = cg.getSolution();
          out += cg.toString();
          break;
        case BP:
          BranchPrice bp = new BranchPrice(instance, options);
          sol = bp.getBestSolution();
          out += bp.toString();
          break;
        default:
          break;
      }
      System.gc();
      if (options.checkSolution()) {
        SolutionChecker sc = new SolutionChecker(sol, instance);
        out += newline + sc.toString();
      }
      
      StdOut.println(out);
      if (options.hasOuput()) {
        Lib.writeToFile(options.getOutFilename(), header + out);
      }
      StdOut.println();
    }
    System.exit(0);
  }
  
  

  private static String getHeader(String[] args, Instance instance) {
    String header = instance.getName();
    String newline = System.getProperty("line.separator");
    header += newline;
    for (int k = 0; k < args.length; k++) {
      header += (args[k] + " ");
    }
    header += newline;
    return header;
  }
}
