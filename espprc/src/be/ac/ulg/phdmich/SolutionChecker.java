package be.ac.ulg.phdmich;

import java.util.List;
import java.util.Set;

public class SolutionChecker {
  
  private static final double COST_TOL = 1.E-4;
  private final boolean isFeasible;
  private final boolean sense;
  private boolean[] visited;
  private double oldCost = 0;
  private double newCost = 0;
  private boolean allPathsFeasible = true;
  private boolean costCorrect;

  /**
   * Simple class to check the feasibility of a solution relatively to a certain instance.
   * 
   * @param solution the solution to check
   * @param instance the relative instance
   */
  public SolutionChecker(Solution solution, Instance instance) {
    if (solution == null || instance == null
        || solution.getPaths() == null || solution.getPaths().isEmpty()) {
      isFeasible = false;
      sense = false;
      return;
    }
    sense = true;
    int size = instance.getSize();
    visited = new boolean[size + 2];
    visited[0] = true;
    oldCost = solution.getCost();
    Set<Path> paths = solution.getPaths();
    for (Path p : paths) {
      List<Short> sequence = p.getSequence();
      for (short n : sequence) {
        visited[n] = true;
      }
      Path newp = new Path(sequence, instance);
      allPathsFeasible &= newp.isFeasible();
      double val = solution.getValue(p);
      newCost += newp.getCost() * val;
    }
    isFeasible = allPathsFeasible && allVisited();
    costCorrect = (Math.abs(newCost - oldCost) < COST_TOL);
  }
  
  private boolean allVisited() {
    int len = visited.length;
    boolean ans = true;
    for (int i = 0; i < len; i++) {
      ans &= visited[i];
    }
    return ans;
  }
  
  @Override
  public String toString() {
    if (!sense) {
      return "Nothing to check.";
    }
    StringBuilder str = new StringBuilder();
    String newline = System.getProperty("line.separator");
    str.append("Feasible: " + isFeasible + ", cost correct: " + costCorrect);
    str.append(newline);
    return str.toString();
  }
  
  public static void main(String[] args) {
    
  }
}
