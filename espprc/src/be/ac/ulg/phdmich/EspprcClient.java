package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class EspprcClient {

  /**
   * Client for batch runs of the ESPPRC data type.
   * 
   * @param args the program arguments: the options for the ESPPRC parameters
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    
    final boolean test = false;
    //    Out out = new Out("data/output.txt");
    StdOut.println("*************************************************************************");
    StdOut.println();
    StdOut.println("                       ****    ESPPRC    ****                            ");
    StdOut.println();
    StdOut.println("*************************************************************************");
    StdOut.println(options);
    //    StdOut.println("Decremental state space relaxation: " + (dssr ? "ON." : "OFF."));
    //    StdOut.println("NG-route relaxation: " + (ng ? "ON." : "OFF."));
    In paraFile = new In("data/Param.txt");
    Instance.Parameters par = new Instance.Parameters(paraFile);
    paraFile.close();
    StdOut.println("Capacity: " + par.getMaxCapacity());
    StdOut.println("Shift duration: " + par.getMaxDuration());
    StdOut.println("*************************************************************************");
    StdOut.println();
    String[] paths = {
        "data50/Clustered/",
        "data50/Random-Clustered/",
        "data50/Random/",
        "data100/Clustered/",
        "data100/Random-Clustered/",
        "data100/Random/" };
    int[] nums = { 9, 8, 12, 9, 8, 12 };
    if (!test) {
      for (int k = 0; k < paths.length; k++) {
        StdOut.println("*************************************************************************");
        StdOut.println("Set " + paths[k] + ", " + nums[k] + " instances: ");
        StdOut.println("*************************************************************************");
        //out.println("Set " + paths[k] + ": ");
        for (int h = 1; h <= nums[k]; h++) {
          runInstance(paths[k], h, options);
        }
      }
    } else {
      runInstance(paths[2], 2, options);
    }
    //out.close();
  }
  
  private static void runInstance(String path, int inum, Options options) {
    StdOut.println("Set " + path + ", " + "instance " + inum + ": ");
    //out.println("Set " + path + ", " + "instance " + inum + ": ");
    // read instance data
    In instFile = new In("data/" + path + inum + ".txt");
    In dualFile = new In("data/" + path + "random_prizes_" + inum + ".txt");
    In paraFile = new In("data/Param.txt");
    Instance instance = new Instance(paraFile, instFile);
    double[] dual = Lib.readDualPricesFromFile(dualFile, instance.getSize());
    State state = new State(instance, options, true);
    // work on the instance
    Espprc espprc = new Espprc(instance, state, options, dual, "no", "no");
    StdOut.println(espprc);
    //out.println(espprc);
    //out.println();
    //espprc.output("data/output" + inum + ".txt");
    StdOut.println("Done.");
    StdOut.println("*************************************************************************");
    StdOut.println();
    instFile.close();
    dualFile.close();
    paraFile.close();
  }
}
