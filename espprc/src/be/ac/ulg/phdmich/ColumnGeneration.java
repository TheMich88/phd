package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.StopwatchCPU;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

//import be.ac.ulg.phdmich.PhdOptions.HeurUse;


public class ColumnGeneration {

  private Options options;
  private Solution solution;
  private Solution integralSolution = null;
  private LpModel model;
  private double elapsedTime;
  private Stopwatch stopwatch;
  private double ipTime = -1;
  private Stopwatch timeLimiter = null;
  private int totColumns = 0;
  private int iter = 0;
  private int secondHeurIter = 0;
  private int exactIter = 0;
  private boolean isFeasible = true;
  private boolean stopped = false;
  private boolean pathsAdded = false;
  private ExecutorService executor = Executors.newSingleThreadExecutor();
  
  public ColumnGeneration(Instance instance, State state, Set<Path> validPaths, Set<Path> pathPool,
      Options options, boolean ip) {
    this(instance, state, validPaths, pathPool, options, ip, null, null);
  }

  /**
   * Solves the linear relaxation of the current model, given the branching rules.
   * 
   * @param instance the problem instance
   * @param state the state of the algorithm
   * @param options the options for the algorithm
   */
  public ColumnGeneration(Instance instance, State state, Set<Path> validPaths, Set<Path> pathPool,
      Options options, boolean ip, Stopwatch timeLim, BranchPrice bp) {
    this.options = options;
    boolean log = !options.isQuiet();
    this.timeLimiter = timeLim;
    stopwatch = new Stopwatch();
    // StdOut.println(allPaths);
    model = new LpModel(instance, options, validPaths);
    // Espprc espprc;
    while (true) {
      iter++;
      int iterPathCount = 0;
      if (model.solve(false)) {
        pathsAdded = false;
        solution = model.getSolution();
        double[] prices = model.getDuals();
        //debug
        if (log) {
          StdOut.println("Value found: " + solution.getCost());
          StdOut.println("Dual pr.sum: " + Lib.arrSum(prices));
          StdOut.println("Dual prices:\n" + Arrays.toString(prices));
          StdOut.println("Solution:\n" + solution);
        }
        PricingState pstate = new PricingState(instance, state, prices, options);
        Espprc espprc = null;
        Set<Path> currentPaths = new HashSet<>();

        Options.HeurUse use = options.getHeurUse();
        if (use == Options.HeurUse.ONE || use == Options.HeurUse.BOTH) {
          if (log) {
            Lib.log("Trying first heuristic...", stopwatch);
          }
          espprc = launchEspprc(instance, pstate, options, "exact", "heuristic", timeLim);
          if (espprc == null) {
            this.stopped = true;
            break;
          }
          currentPaths = espprc.getPaths();
        }
        if (currentPaths.isEmpty()) {
          if (use == Options.HeurUse.TWO || use == Options.HeurUse.BOTH) {
            secondHeurIter++;
            if (log) {
              Lib.log("Trying second heuristic...", stopwatch);
            }
            espprc = launchEspprc(instance, pstate, options, "exact", "heuristic2", timeLim);
            if (espprc == null) {
              this.stopped = true;
              break;
            }
            currentPaths = espprc.getPaths();
          }
        } else {
          addPaths(currentPaths, validPaths, pathPool);
        }
        if (currentPaths.isEmpty()) {
          if (use == Options.HeurUse.START) {
            secondHeurIter++;
            if (log) {
              Lib.log("Trying H heuristic...", stopwatch);
            }
            espprc = launchEspprc(instance, pstate, options, "heuristic", "normal", timeLim);
            if (espprc == null) {
              this.stopped = true;
              break;
            }
            currentPaths = espprc.getPaths();
          }
        } else {
          addPaths(currentPaths, validPaths, pathPool);
        }
        if (currentPaths.isEmpty()) {
          exactIter++;
          if (log) {
            Lib.log("Trying exact DP...", stopwatch);
          }
          espprc = launchEspprc(instance, pstate, options, "exact", "normal", timeLim);
          if (espprc == null) {
            this.stopped = true;
            break;
          }
          currentPaths = espprc.getPaths();
        } else {
          addPaths(currentPaths, validPaths, pathPool);
        }
        if (currentPaths.isEmpty()) {
          if (log) {
            Lib.log("No new columns found. Terminating...", stopwatch);
          }
          break;
        } else {
          addPaths(currentPaths, validPaths, pathPool);
        }
        if (log) {
          //debug
          StdOut.println("Adding paths to the model. Size = " + currentPaths.size());
          List<Path> list = new ArrayList<>(currentPaths);
          Collections.sort(list, new Path.CostMinOrder());
          StdOut.println(list);
        }
        model.addColumns(currentPaths);
        // model.addElementaryColumns(paths);
        totColumns += currentPaths.size();
        iterPathCount = currentPaths.size();
      } else {
        if (log) {
          Lib.log("Model is not feasible", stopwatch);
        }
        isFeasible = false;
        break;
      }
      if (state.isRootNode() && options.monitorBounds()) {
        bp.getIterPool().put(iter, iterPathCount);
        bp.getIterTotalPool().put(iter, model.getPathMap().size());
        Solution is = null;
        Stopwatch swip = new Stopwatch();
        if (model.solve(true)) {
          bp.getIterIpTime().put(iter, swip.elapsedTime());
          is = model.getSolution();
          bp.getIterBound().put(iter, is.getCost());
          bp.getIterElTime().put(iter, stopwatch.elapsedTime());
          Lib.log("Iter " + iter + ": inserted " + iterPathCount
              + " columns, total " + model.getPathMap().size(), stopwatch);
          StdOut.println("        upper bound " 
              + Lib.roundToString(is.getCost()));
          StdOut.println("        obtained in " 
              + Lib.roundToString(bp.getIterIpTime().get(iter)) + " seconds");
          
          if (integralSolution == null || is.getCost() < integralSolution.getCost()) {
            integralSolution = is;
            if (bp.getSc() != null) {
              bp.getSc().addSol(is);
            }
          }
        }
      }
      if (timeLimiter != null && timeLimiter.elapsedTime() > options.getTimeLimit()) {
        this.stopped = true;
        break;
      }
      if (Thread.interrupted()) {
        return;
      }
    }
    System.gc();
    solution = isFeasible ? model.getSolution() : null;
    if (isFeasible && ip) {
      if (!solution.isIntegral()) {
        if (log) {
          Lib.log("Attempting to solve the IP...", stopwatch);
        }
        StopwatchCPU stopwatchIp = new StopwatchCPU();
        if (model.solve(true)) {
          Solution is = model.getSolution();
          if (integralSolution == null || is.getCost() < integralSolution.getCost()) {
            integralSolution = is;
          }
        }
        ipTime = stopwatchIp.elapsedTime();
      } else {
        integralSolution = solution;
      }
    }
    elapsedTime = stopwatch.elapsedTime();
    model.close();
    executor.shutdownNow();
  }

  private Espprc launchEspprc(Instance instance, PricingState pstate, Options options, 
      String isHeur, String domRule, Stopwatch timelim) {
    long timelimit = Math.round(options.getTimeLimit() - timelim.elapsedTime());
    if (timelimit < 1) {
      return null;
    }
    Future<Espprc> future = executor.submit(new Callable<Espprc>() {
        public Espprc call() throws Exception {
          return new Espprc(instance, pstate, 
            options, isHeur, domRule);
        }
    });
    try {
      return future.get(timelimit, TimeUnit.SECONDS); //timeout is in 2 seconds
    } catch (TimeoutException ex) {
      return null;
    } catch (InterruptedException ex) {
      // handle the interrupts
      return null;
    } catch (ExecutionException ex) {
      // handle other exceptions
      ex.printStackTrace();
      return null;
    } finally {
      future.cancel(true);
    }
  }
  
  public boolean isStopped() {
    return stopped;
  }
  
  public double getIpTime() {
    return ipTime;
  }

  /**
   * Adds the paths in the current set to the set of valid paths, and the whole pool of columns if
   * not null.
   * 
   * @param currentPaths the set of current paths
   * @param validPaths the set of valid paths
   * @param pathPool the pool of columns
   */
  private void addPaths(Set<Path> currentPaths, Set<Path> validPaths, Set<Path> pathPool) {
    if (pathsAdded) {
      return;
    } else {
      pathsAdded = true;
    }
    for (Path p : currentPaths) {
      validPaths.add(p);
      if (pathPool != null) {
        pathPool.add(p);
      }
    }
  }

  public ColumnGeneration(Instance instance, Options options) {
    this(instance, new State(instance, options, true), options);
  }
  
  public ColumnGeneration(Instance instance, State state, Options options) {
    this(instance, state, Lib.getInitialSetOfPaths(instance), null, options, false);
  }

  public boolean hasIntegral() {
    return (integralSolution != null);
  }

  public Solution integralSolution() {
    return integralSolution;
  }

  /**
   * Method used to build the LaTeX table of results.
   * 
   * @return a string with the results of the computations on the instance, aimed to the LaTeX
   *         format
   */
  public String getResult() {
    String et = " & ";
    return et + Lib.roundToString(getValue()) + et + Lib.roundToString(getTime()) + et + getIter()
        + et + getExactIter() + et + getTotCol();
  }

  /**
   * Returns the current solution, or null if the problem is infeasible.
   * 
   * @return the solution or null if infeasible
   */
  public Solution getSolution() {
    if (!isFeasible) {
      return null;
    }
    return solution;
  }

  public boolean isFeasible() {
    return isFeasible;
  }

  public double getValue() {
    return solution.getCost();
  }

  public double getTime() {
    return elapsedTime;
  }

  public int getIter() {
    return iter;
  }
  
  public LpModel getModel() {
    return model;
  }

  public int getSecondHeurIter() {
    return secondHeurIter;
  }

  public int getTotCol() {
    return totColumns;
  }

  public int getExactIter() {
    return exactIter;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    String newline = System.getProperty("line.separator");
    if (solution != null) {
      if (options.printSolution()) {
        str.append("Solution: " + newline + solution);
      } else {
        str.append("Total cost: " + Lib.roundToString(solution.getCost()));
      } 
    } else {
      str.append("No solution found.");
    }
    str.append(newline);
    str.append("Total time: " + Lib.roundToString(elapsedTime));
    str.append(newline);
    str.append("Total iterations: " + (iter));
    str.append(newline);
    str.append("2nd heuristic Iterations: " + (secondHeurIter));
    str.append(newline);
    str.append("Exact Iterations: " + exactIter);
    str.append(newline);
    str.append("Total columns added: " + totColumns);
    str.append(newline);
    return str.toString();
  }

  /**
   * Test client.
   * 
   * @param args the program arguments
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    String pfile = "data/Param.txt";
    String ifile = "data/data100/Random-Clustered/5.txt";
    Instance instance = new Instance(pfile, ifile);
    State state = new State(instance, options, true);
    ColumnGeneration cg = new ColumnGeneration(instance, state, options);
    StdOut.println(cg);
  }
}
