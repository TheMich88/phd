package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Class implementing the main Branch and Price algorithm.
 */
public final class BranchPrice {
  private double upperBound = Double.POSITIVE_INFINITY;
  private double lowerBound = Double.NEGATIVE_INFINITY;
  private double upperBoundRoot = Double.POSITIVE_INFINITY;
  private double lowerBoundRoot = Double.NEGATIVE_INFINITY;
  private Set<Path> allPaths = new HashSet<>();
  private Solution bestSolution = null;
  private final Instance instance;
  private final Options options;
  private final Node rootNode;
  private final Map<Short, Set<Short>> ngSetRoot = new HashMap<>();
  private final Map<Short, Set<Short>> ngSetNonRoot = new HashMap<>();
  private int nodeCounter = 0;
  private double elapsedTime;
  private Stopwatch stopwatch;
  //private Stopwatch timeLimiter;
  private Queue<Node> activeNodes;
  private int iterTotal = 0;
  private int secondHeurIterTotal = 0;
  private int exactIterTotal = 0;
  private int iterRoot = 0;
  private int secondHeurIterRoot = 0;
  private int exactIterRoot = 0;
  private double ipRootTime;
  private final boolean bfs;
  private static final int SOLVER_INTERVAL = 20;
  private static final int THRESHOLD_INTERVAL = 100;
  private static final double THRESHOLD_MULTIPLIER = 1.2;
  private static double threshold = 0.0;
  private boolean stopped = false;
  private ExecutorService executor = Executors.newSingleThreadExecutor();
  private SolutionContainer sc;
  private Map<Integer, Double> iterBound = new HashMap<>();
  private Map<Integer, Double> iterIpTime = new HashMap<>();
  private SortedMap<Integer, Integer> iterPool = new TreeMap<>();
  private Map<Integer, Integer> iterTotalPool = new HashMap<>();
  private Map<Integer, Double> iterElTime = new HashMap<>();
  private double salvageTime = 0;
  
  /**
   * Constructor for the branch and price class. TODO complete
   *
   * @param instance the problem instance
   * @param options the algorithm options
   */
  public BranchPrice(Instance instance, Options options) {
    this.sc = new SolutionContainer(instance);
    this.instance = instance;
    this.options = options;
    stopwatch = new Stopwatch();
    //timeLimiter = stopwatch;
    bfs = (options.getTreeNav() == Options.TreeNav.BEST_FIRST);
    activeNodes = bfs 
        ? (new PriorityQueue<>(11, new MinParent())) : (new ArrayDeque<>());
    // initialization
    log("Initializing...");
    
    // Better to initialize immediately the neighborhoods
    // if we are using them instead of rebuilding them for 
    // every node in the BB tree
    if (options.usingNgSets()) {
      for (short i = 0; i <= instance.getSize() + 1; i++) {
        ngSetRoot.put(i, Lib.buildNgSet(instance, options, i, true));
        ngSetNonRoot.put(i, Lib.buildNgSet(instance, options, i, false));
      }
    }
    
    rootNode = new Node(instance, options);
    activeNodes.add(rootNode);
    // main BP loop
    while (!activeNodes.isEmpty()) {
      if (stopped || stopwatch.elapsedTime() > options.getTimeLimit()) {
        this.stopped = true;
        break;
      }
      nodeCounter++;
      boolean cplexToggle = false; // TODO test
      if (cplexToggle && (nodeCounter % SOLVER_INTERVAL == 0)) {
        log("Attempting to solve the IP at the root node");
        solveRootNode();
      }
      if (nodeCounter % THRESHOLD_INTERVAL == 0) {
        threshold *= THRESHOLD_MULTIPLIER;
      }
      Node currentNode = activeNodes.remove(); // removeFirst
      
      // process node within remaining time
      Future<Void> future = executor.submit(new Callable<Void>() {
          public Void call() throws Exception {
            processNode(currentNode);
            return null;
          }
      });
      long timelimit = Math.round(options.getTimeLimit() - stopwatch.elapsedTime());
      if (timelimit < 1) {
        this.stopped = true;
        break;
      }
      try {
        future.get(timelimit, TimeUnit.SECONDS);
      } catch (TimeoutException ex) {
        // handle timeout
        this.stopped = true;
        break;
      } catch (InterruptedException ex) {
        // handle the interrupts
        this.stopped = true;
        break;
      } catch (ExecutionException ex) {
        // handle other exceptions
        ex.printStackTrace();
        break;
      } finally {
        future.cancel(true);
        if (options.monitorBounds()) {
          activeNodes.clear();
        }
      }
    }
    elapsedTime = stopwatch.elapsedTime();
    if (bestSolution == null && options.isSalvaging()) {
      Stopwatch sw = new Stopwatch();
      LpModel model = new LpModel(instance, options, allPaths);
      if (model.solve(true)) {
        sc.addSol(model.getSolution());
        salvageTime = sw.elapsedTime();
      }
    }
    executor.shutdownNow();
    Solution bs = sc.getSolution();
    if (bestSolution == null || bs.getCost() < upperBound) {
      bestSolution = bs;
      upperBound = bs.getCost();
    }
  }
  
  SolutionContainer getSc() {
    return this.sc;
  }

  Map<Integer, Double> getIterBound() {
    return iterBound;
  }
  
  Map<Integer, Double> getIterIpTime() {
    return iterIpTime;
  }
  
  SortedMap<Integer, Integer> getIterPool() {
    return iterPool;
  }
  
  Map<Integer, Integer> getIterTotalPool() {
    return iterTotalPool;
  }
  
  Map<Integer, Double> getIterElTime() {
    return iterElTime;
  }
  
  @SuppressWarnings("unused")
  private void dbg() {
    List<Path> list = new ArrayList<>();
    for (Path p : this.allPaths) {
      if (p.getSequence().size() == 15 
          && p.getSequence().get(1) == 43
          && p.getSequence().get(2) == 42
          && p.getSequence().get(3) == 41
          && p.getSequence().get(4) == 40
          && p.getSequence().get(5) == 44
          && p.getSequence().get(6) == 46
          && p.getSequence().get(7) == 45
          && p.getSequence().get(8) == 48
          && p.getSequence().get(9) == 51
          && p.getSequence().get(10) == 50
          && p.getSequence().get(11) == 52
          && p.getSequence().get(12) == 49
          && p.getSequence().get(13) == 47) {
        list.add(p);
      }
    }
    for (Path p : list) {
      // ClassicLabel left = (ClassicLabel)p.getLeftLabel();
      // Label right = p.getRightLabel();
      // boolean res = Lib.halfway(instance, left, right, p.getValues());
      // StdOut.println(left.getNode() + " " + right.getNode() + ": " + res);
      // DirectedEdge edge = instance.getG().edge(left.getNode(), right.getNode());
      // Label ext = left.extend(instance, left.ps, edge);
      // boolean eff = Lib.eff(instance, left.ps, ext, Espprc.DomType.HEUR_2);
      // StdOut.println(eff);
    }
  }

  private void processNode(Node currentNode) {
    currentNode.loadMatrix();
    log("Attempting to solve node " + nodeCounter + ", with bound "
        + (upperBound == Double.POSITIVE_INFINITY 
        ? upperBound : Lib.round(upperBound, 2))
        + ", size of deque is " + (activeNodes.size() + 1) + ": \n" 
        + currentNode);
    
    // here we can put the option to solve integrally
    currentNode.solve(currentNode.isRoot, this); 
    if (currentNode.stopped) {
      this.stopped = true;
      return;
    }
    if (!currentNode.isFeasible()) {
      log("Current node is infeasible.\n");
      return;
    } else {
      Solution currentSolution = currentNode.getSolution();
      double currentCost = currentSolution.getCost();
      if (currentCost > lowerBound && lowerBound < upperBound) {
        lowerBound = currentCost;
      }
      if (currentNode.isRoot) {
        lowerBoundRoot = currentCost;
        upperBoundRoot = currentNode.getIntegralSolution().getCost();
      }
      log("Current node is feasible, solution value is " 
          + Lib.round(currentCost, 2));
      if (currentSolution.isIntegral()) {
        log("Current solution is integral!");
        if (currentCost < upperBound) {
          log("Current solution is better than incumbent!\n");
          upperBound = currentCost;
          bestSolution = currentSolution;
        } else {
          log("Current solution is not better than incumbent.\n");
        }
      } else {
        if (currentCost >= upperBound) {
          log("Linear relaxation is not better than incumbent, "
              + "fathoming...\n");
        } else { //if (currentNode.hasIntegral() || gap > threshold) {
          if (currentNode.hasIntegral()) {
            Solution intSol = currentNode.getIntegralSolution();
            double intopt = intSol.getCost();
            log("Best integer solution found with value " 
                + Lib.round(intopt, 2));
            upperBound = Math.min(upperBound, intopt);
            if (bestSolution == null || intopt < bestSolution.getCost()) {
              bestSolution = intSol;
            }
          }
          Double gap = (upperBound - currentCost) / currentCost;
          log("Gap is " + Lib.round(gap * 100, 2) + " %");
          if (gap > threshold) {
            DirectedEdge branchingEdge = findBranchingEdge(currentSolution, currentNode);
            String str = "Branching on edge " 
                + branchingEdge.from() + "->" + branchingEdge.to()
                + ", gap is " + Lib.round(gap * 100, 2) + " %" + "\n";
            log(str);
            addChildNodes(currentNode, branchingEdge);
          } else {
            log("Gap is smaller than threshold=" + threshold 
                + ", no branching performed.\n");
          }
        }
      }
    }
    currentNode.clean();
    System.gc();
  }

  private void log(String string) {
    if (!options.isQuiet()) {
      Lib.log(string, stopwatch);
    }
  }
  
  // attempts to solve the IP at the root node 
  // with all the columns found until now
  private void solveRootNode() {
    rootNode.solve(true, this);
    if (rootNode.hasIntegral()) {
      Solution sol = rootNode.getIntegralSolution();
      double cost = sol.getCost();
      if (cost < upperBound) {
        log("Improved the bound!");
        bestSolution = sol;
        upperBound = cost;
      }
    }
  }

  // computes the flow on each arc 
  // and returns the edge with flow closest to 0.5
  private DirectedEdge findBranchingEdge(Solution solution, Node node) {
    // int size = instance.getSize();
    // double[][] flowVars = new double[size + 1][size + 1];
    Map<Pair<Short,Short>,Double> flowVars = new HashMap<>();
    for (Path p : solution.getPaths()) {
      double val = solution.getValue(p);
      if (0 < val && val < 1) {
        for (short i = 1; i < p.getSequence().size() - 2; i++) {
          short tail = p.getSequence().get(i);
          short head = p.getSequence().get(i + 1);
          // flowVars[tail][head] += val;
          Pair<Short, Short> arc = new Pair<>(tail, head);
          if (flowVars.containsKey(arc)) {
            flowVars.put(arc, flowVars.get(arc) + val);
          } else {
            flowVars.put(arc, val);
          }
        }
      }
    }
    double maxDiff = Double.MAX_VALUE;
    double fracFlow = 0;
    int branchTail = 0;
    int branchHead = 0;
    final double ref = 0.5;
    //for (int i = 1; i < size + 1; i++) {
    //for (int j = 1; j < size + 1; j++) {
    for (Pair<Short, Short> arc : flowVars.keySet()) {
      if (maxDiff == 0) {
        break;
      }
      double flow = flowVars.get(arc);//[i][j];
      double diff = Math.abs(ref - flow);
      if (diff < maxDiff) {
        maxDiff = diff;
        fracFlow = flow;
        branchTail = arc.getLeft();//i;
        branchHead = arc.getRight();//j;
      }
      // }
    }
    // TODO move check for edge validity here
    DirectedEdge edge = instance.getG().getEdge(branchTail, branchHead);
    checkEdge(node, edge, fracFlow);
    return edge;
  }
  
  private class Pair<L,R> {

    private final L left;
    private final R right;

    public Pair(L left, R right) {
      this.left = left;
      this.right = right;
    }

    public L getLeft() {
      return left;
    }
    
    public R getRight() {
      return right;
    }

    @Override
    public int hashCode() { 
      return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object ob) {
      if (!(ob instanceof Pair<?,?>)) {
        return false;
      }
      Pair<?,?> pairob = (Pair<?,?>) ob;
      return this.left.equals(pairob.getLeft()) 
          && this.right.equals(pairob.getRight());
    }
  }

  // derives the child notes from the current one and adds them if feasibility
  // estimation gives the ok
  private void addChildNodes(Node currentNode, DirectedEdge branchingEdge) {
    Node forcedNode = currentNode.getForcedChild(branchingEdge);
    Node forbiddenNode = currentNode.getForbiddenChild(branchingEdge);
    if (forbiddenNode != null) {
      if (bfs) {
        activeNodes.add(forbiddenNode);
      } else {
        // activeNodes.addFirst(forbiddenNode);
        switch (options.getTreeNav()) {
          case DEPTH_FIRST:
            ((Deque<Node>) activeNodes).addFirst(forbiddenNode);
            break;
          default:
            ((Deque<Node>) activeNodes).addLast(forbiddenNode);
        }
      }
    }
    if (forcedNode != null) {
      if (bfs) {
        activeNodes.add(forcedNode);
      } else {
        switch (options.getTreeNav()) {
          case BREADTH_FIRST:
            ((Deque<Node>)activeNodes).addLast(forcedNode);
            break;
          default:
            ((Deque<Node>)activeNodes).addFirst(forcedNode);            
        }
      }
    }
  }
  
  private void checkEdge(Node node, DirectedEdge edge, double flow) {
    boolean inb = node.branchForbidden.contains(edge);
    boolean inc = node.branchForced.contains(edge);
    if (inb || inc) {
      System.err.println("Branching on edge " + edge + ", flow " + flow + ", is illegal");
      if (inb) {
        System.err.println("Edge is in forbidden list: " + node.branchForbidden);
      } else {
        System.err.println("Edge is in forced list: " + node.branchForced);
      }
      System.err.println(node.getSolution());
      throw new IllegalStateException();
    }
  }

  public double getTime() {
    return elapsedTime;
  }

  public Solution getBestSolution() {
    return bestSolution;
  }

  public int solvedNodes() {
    return nodeCounter;
  }
  
  public int getIter() {
    return iterTotal;
  }
  
  public int getSecondIter() {
    return secondHeurIterTotal;
  }
  
  public int getExactIter() {
    return exactIterTotal;
  }
  
  public int getIterRoot() {
    return iterRoot;
  }
  
  public int getSecondIterRoot() {
    return secondHeurIterRoot;
  }
  
  public int getExactIterRoot() {
    return exactIterRoot;
  }
  
  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    String newline = System.getProperty("line.separator");
    double iraceResult = this.stopped 
        ? Math.min(options.getTimeLimit() + upperBound, Double.MAX_VALUE)
        : elapsedTime;
    str.append(iraceResult);
    str.append(newline);
    if (this.stopped) {
      str.append("Runtime exceeded time limit");
      str.append(newline);
    }
    if (bestSolution == null) {
      str.append("No solution found.");
    } else {
      str.append("Total cost: " + Lib.roundToString(bestSolution.getCost()));
    }
    str.append(newline);
    str.append("Total time: " + Lib.roundToString(elapsedTime) 
    //    + " (" + Lib.formatSeconds(elapsedTime) + ")"
    );
    str.append(newline);
    //str.append("Best lower bound: " + Lib.roundToString(lowerBound));
    //str.append(newline); // XXX currently broken - have to decide what to do
    str.append("Best upper bound: " + Lib.roundToString(upperBound));
    str.append(newline);
    str.append("Lower bound at root: " + Lib.roundToString(lowerBoundRoot));
    str.append(newline);
    str.append("Upper bound at root: " + Lib.roundToString(upperBoundRoot));
    str.append(newline);
    str.append("Nodes solved: " + nodeCounter);
    str.append(newline);
    //int h1 = (iterRoot - secondHeurIterRoot);
    //int h2 = (secondHeurIterRoot - exactIterRoot);
    str.append("Total iterations at root: " + iterRoot);
    str.append(newline);
    str.append("2nd heur iterations at root: " + secondHeurIterRoot);
    str.append(newline);
    str.append("Exact iterations at root: " + exactIterRoot);
    str.append(newline);
    str.append("Total iterations, non root: " + (iterTotal - iterRoot));
    str.append(newline);
    str.append("2nd heur iterations, non root: " 
        + (secondHeurIterTotal - secondHeurIterRoot));
    str.append(newline);
    str.append("Exact iterations, non root: " 
        + (exactIterTotal - exactIterRoot));
    str.append(newline);
    
    if (ipRootTime >= 0) {
      str.append("Time for solving root node IP: " + ipRootTime);
    } else {
      str.append("Root node IP was not attempted.");
    }
    str.append(newline);
    if (salvageTime > 0) {
      str.append("Salvaged solution found in " + Lib.round(salvageTime) 
          + " seconds." + newline);
      
    }
    if (options.monitorBounds()) {
      str.append("CG root statistics:" + newline);
      for (int k: iterPool.keySet()) {
        str.append("[" + iterElTime.get(k) + "] Iter " + k 
            + ": inserted " + iterPool.get(k)
            + " columns, total " + iterTotalPool.get(k) + newline);
        if (iterBound.containsKey(k)) {
          str.append("        upper bound " 
              + Lib.roundToString(iterBound.get(k)) + newline);
        }
        if (iterIpTime.containsKey(k)) {
          str.append("        obtained in " 
              + Lib.roundToString(iterIpTime.get(k)) + " seconds" + newline);
        }
      }
    }
    if (options.printSolution()) {
      str.append("Solution: " + newline + bestSolution);
    }
    return str.toString();
  }

  private class Node {
    // possible arcs
    private boolean[][] forbidden;
    private Solution solution = null;
    private Solution integralSolution = null;
    private boolean isFeasible = false;
    private Set<Path> trivial;
    private List<DirectedEdge> branchForced = new ArrayList<>();
    private List<DirectedEdge> branchForbidden = new ArrayList<>();
    private double parentCost = Double.POSITIVE_INFINITY;
    private boolean stopped = false;
    private boolean isRoot = false;
    private boolean cleaned = false;
    private double cost;

    // constructor initializes the root node
    public Node(Instance instance, Options options) {
      isRoot = true;
      this.forbidden = instance.getForbidden();
      this.trivial = Lib.getInitialSetOfPaths(instance);
      allPaths = new HashSet<>(trivial);
    }

    @Override
    public String toString() {
      StringBuilder str = new StringBuilder();
      String newline = System.getProperty("line.separator");
      str.append("Force : " + branchForced);
      str.append(newline);
      str.append("Forbid: " + branchForbidden);
      return str.toString();
    }
    
    public void clean() {
      this.cost = this.getCost();
      if (!this.isRoot) {
        this.branchForbidden = null;
        this.branchForced = null;
        this.forbidden = null;
        this.solution = null;
        this.integralSolution = null;
        this.trivial = null;
      }
      this.cleaned = true;
    }

    public double getCost() {
      if (this.cleaned) {
        return this.cost;
      } else {
        if (!this.isFeasible || this.getSolution() == null) {
          return Double.POSITIVE_INFINITY;
        } else {
          return this.getSolution().getCost();
        }
      }
    }
    
    // If we build the matrix only when necessary
    // we should occupy less memory
    /**
     * Loads the matrix of forbidden vertices for this node.
     */
    public void loadMatrix() {
      if (!this.isRoot) {
        this.forbidden = copyMatrix(instance.getForbidden());
        for (DirectedEdge e : this.branchForbidden) {
          this.forbidden[e.from()][e.to()] = true;
        }
        for (DirectedEdge e : this.branchForced) {
          for (int i = 0; i < instance.getSize() + 2; i++) {
            if (i != e.to()) {
              this.forbidden[e.from()][i] = true;
            }
            if (i != e.from()) {
              this.forbidden[i][e.to()] = true;
            }
          }
        } 
      }
    }

    // Returns the child node on which the edge is forbidden.
    // Does not return null - no feasibility test performed
    public Node getForbiddenChild(DirectedEdge edge) {
      Node child = new Node();
      child.parentCost = this.getCost();
      child.branchForced = new ArrayList<>(this.branchForced);
      child.branchForbidden = new ArrayList<>(this.branchForbidden);
      child.branchForbidden.add(edge);
      child.trivial = new HashSet<>(this.trivial);
      return child;
    }

    // Returns the child node on which the edge is forced.
    // This method returns null in case the
    // "preliminary analysis of feasibility" fails
    public Node getForcedChild(DirectedEdge edge) {
      Node child = new Node();
      child.parentCost = this.getCost();
      child.branchForced = new ArrayList<>(this.branchForced);
      child.branchForbidden = new ArrayList<>(this.branchForbidden);
      child.branchForced.add(edge);
      child.trivial = analyseTrivialSolution(this.trivial, 
          this.branchForced, edge);
      if (child.trivial == null) {
        return null;
      }
      return child;
    }

    // Rebuilds the trivial solution, returns null if 
    // it is revealed as infeasible
    private Set<Path> analyseTrivialSolution(Set<Path> paths, 
        List<DirectedEdge> forcedList, DirectedEdge edge) {
      Set<Path> tempSet = new HashSet<>();
      short tail = (short) edge.from(); // i
      short head = (short) edge.to(); // j
      boolean tailUsed = false;
      boolean headUsed = false;
      for (DirectedEdge e : forcedList) {
        // if i is already used as the head of another edge
        if (e.to() == tail) {
          tailUsed = true;
        }
        // if j is already used as the tail of another edge
        if (e.from() == head) {
          headUsed = true;
        }
      }
      List<Short> sequence = new LinkedList<>();
      short depot = (short) (instance.getSize() + 1);
      if (!tailUsed && !headUsed) {
        // just remove 0-i-0, 0-j-0 and add 0-i-j-0
        for (Path p : paths) {
          short node = p.getSequence().get(1);
          if (node != tail && node != head) {
            tempSet.add(p);
          }
        }
        sequence = Arrays.asList((short)0, tail, head, depot);
      } else if (!tailUsed && headUsed) {
        // remove 0-i-0, modify the path 0-j-... to 0-i-j-...
        for (Path p : paths) {
          short node = p.getSequence().get(1);
          if (node != tail && node != head) {
            tempSet.add(p);
          } else if (node == head) {
            sequence = new LinkedList<>(p.getSequence());
          }
        }
        try {
          sequence.add(1, tail);
        } catch (Exception ex) {
          System.err.println("Edge " + edge + "\nForced:");
          for (DirectedEdge e : forcedList) {
            System.err.println(e);
          }
          System.err.println("Paths:");
          for (Path p : paths) {
            System.err.println(p);
          }
          ex.printStackTrace();
          return null;
        }
      } else if (tailUsed && !headUsed) {
        // remove 0-j-0, modify ...-i-0 to ...-i-j-0
        for (Path p : paths) {
          short node = p.getSequence().get(p.getSequence().size() - 2);
          if (node != tail && node != head) {
            tempSet.add(p);
          } else if (node == tail) {
            sequence = new LinkedList<>(p.getSequence());
          }
        }
        try {
          sequence.add(sequence.size() - 1, head);
        } catch (Exception ex) {
          System.err.println("Edge " + edge + "\nForced:");
          for (DirectedEdge e : forcedList) {
            System.err.println(e);
          }
          System.err.println("Paths:");
          for (Path p : paths) {
            System.err.println(p);
          }
          ex.printStackTrace();
          return null;
        }
      } else {
        // merge ...-i-0 and 0-j-... to ...-i-j-...
        List<Short> sequenceLeft = new LinkedList<>();
        List<Short> sequenceRight = new LinkedList<>();
        for (Path p : paths) {
          short node1 = p.getSequence().get(1);
          short node2 = p.getSequence().get(p.getSequence().size() - 2);
          if (node1 != head && node2 != tail) {
            tempSet.add(p);
          } else if (node1 == head) {
            sequenceRight = new LinkedList<>(p.getSequence());
            sequenceRight.remove(0);
          } else if (node2 == tail) {
            sequenceLeft = new LinkedList<>(p.getSequence());
            sequenceLeft.remove(sequenceLeft.size() - 1);
          }
        }
        sequence.addAll(sequenceLeft);
        sequence.addAll(sequenceRight);
      }
      // if the "new" path is not feasible, return null
      Path newPath = new Path(sequence, instance);

      if (!newPath.isFeasible()) {
        return null;
      } else {
        tempSet.add(newPath);
      }
      return tempSet;
    }

    // Copy a boolean matrix (fast(?) implementation)
    private boolean[][] copyMatrix(boolean[][] matrix) {
      boolean[][] ans = new boolean[matrix.length][];
      for (int i = 0; i < matrix.length; i++) {
        boolean[] row = matrix[i];
        int length = row.length;
        ans[i] = new boolean[length];
        System.arraycopy(row, 0, ans[i], 0, length);
      }
      return ans;
    }

    private Node() {}

    public void solve(boolean withIp, BranchPrice bp) {
      Map<Short, Set<Short>> ngSet = this.isRoot ? ngSetRoot : ngSetNonRoot;
      State state = new State(instance, this.forbidden, options, this.isRoot, ngSet);
      Set<Path> validPaths = filterPaths(allPaths, branchForbidden, 
          branchForced);
      ColumnGeneration cg = launchColGen(instance, state, options,
          validPaths, withIp, stopwatch, bp);
      if (cg == null || cg.isStopped()) {
        this.stopped = true;
        return;
      }
      iterTotal += cg.getIter();
      secondHeurIterTotal += cg.getSecondHeurIter();
      exactIterTotal += cg.getExactIter();
      if (this.isRoot) {
        iterRoot += cg.getIter();
        secondHeurIterRoot += cg.getSecondHeurIter();
        exactIterRoot += cg.getExactIter();
        ipRootTime = cg.getIpTime();
        if (!options.isQuiet()) {
          // only to run the experiment on the heuristic
          // options.setHeurUse(HeurUse.NONE); 
          //debug
          StdOut.println(cg.getSolution());
          StdOut.println(cg.integralSolution());
          StdOut.println(cg.getModel().getPathMap().size() + "\n" 
                       + cg.getModel().getPathMap());
        }
      }
      this.isFeasible = cg.isFeasible();
      if (cg.isFeasible()) {
        this.solution = cg.getSolution();
      }
      if (cg.hasIntegral()) {
        integralSolution = cg.integralSolution();
      }
    }

    // Apply the branching decisions to the available pool of columns.
    // Return only the paths that don't contain forbidden edges
    // and include the forced ones
    private Set<Path> filterPaths(Set<Path> allPaths, 
        List<DirectedEdge> branchForbidden, List<DirectedEdge> branchForced) {
      if (this.isRoot) {
        return allPaths;
      }
      Set<Path> validPaths = new HashSet<>();
      for (Path p : allPaths) {
        boolean pass = true;
        for (DirectedEdge e : branchForbidden) {
          if (p.containsEdge(e)) {
            pass = false;
            break;
          }
        }
        if (!pass) {
          continue;
        }
        for (DirectedEdge e : branchForced) {
          if (p.containsIncompatibleEdge(e)) {
            pass = false;
            break;
          }
        }
        if (!pass) {
          continue;
        }
        validPaths.add(p);
      }
      return validPaths;
    }

    private ColumnGeneration launchColGen(Instance instance, State state, Options options, 
        Set<Path> validPaths, boolean withIp, Stopwatch timelim, BranchPrice bp) {
      long timelimit = Math.round(options.getTimeLimit() - timelim.elapsedTime());
      if (timelimit < 1) {
        return null;
      }
      ExecutorService cgexecutor = Executors.newSingleThreadExecutor();
      Future<ColumnGeneration> future = cgexecutor.submit(new Callable<ColumnGeneration>() {
          public ColumnGeneration call() throws Exception {
            return new ColumnGeneration(instance, state, 
              validPaths, allPaths, options, withIp, timelim, bp);
          }
      });
      try {
        return future.get(timelimit, TimeUnit.SECONDS); //timeout is in 2 seconds
      } catch (TimeoutException ex) {
        return null;
      } catch (InterruptedException ex) {
        // handle the interrupts
        return null;
      } catch (ExecutionException ex) {
        // handle other exceptions
        ex.printStackTrace();
        return null;
      } finally {
        future.cancel(true);
        cgexecutor.shutdownNow();
      }
    }

    
    public boolean hasIntegral() {
      return integralSolution != null;
    }

    public Solution getIntegralSolution() {
      return integralSolution;
    }
    
    public double getParentCost() {
      return parentCost;
    }

    public Solution getSolution() {
      if (solution == null) {
        throw new IllegalStateException("Node not solved yet or infeasible");
      } else {
        return solution;
      }
    }

    public boolean isFeasible() {
      return this.isFeasible;
    }
  }

  private static class MinParent implements Comparator<Node> {
    @Override
    public int compare(Node n1, Node n2) {
      return Double.compare(n1.getParentCost(), n2.getParentCost());
    }
  }

  /**
   * Test client for this class.
   *
   * @param args program arguments: the option specifications
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    String ifile = "data/data100/Random-Clustered/4.txt";
    Instance instance = new Instance(ifile, options);
    BranchPrice bp = new BranchPrice(instance, options);
    StdOut.println(bp);
  }
}
