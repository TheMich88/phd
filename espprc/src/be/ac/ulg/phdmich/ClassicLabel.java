package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class ClassicLabel implements Label {
  private short node = 0;
  private float dual = 0; // sum of dual prices
  private float del = 0; // total demand
  private float totalDistance = 0;
  private float totalDuration = 0;
  
  private BitSet visited ; // nodes actually in the path
  // it is infeasible to extend a label to these nodes
  private BitSet infeasible ;
  // length of path sigma, with regards to non-critical nodes, as in Righini and Salani
  private short pathLength = 0;         
  // other info
  private boolean isFw = true;
  private boolean isExt = false; // false iff it has never been considered for extension
  private List<Short> bounded = new ArrayList<>();
  private ClassicLabel mother = null;            
  private List<Short> partialPath = new ArrayList<>();
  // PricingState ps;
  
  /**
   * Basic constructor.
   * 
   * @param inst the problem instance
   */
  public ClassicLabel(Instance inst) {
    this.visited = new BitSet(inst.getSize() + 2);
    this.infeasible = new BitSet(inst.getSize() + 2);
  }
  
  /**
   * Creates a label starting at the source.
   * 
   * @param inst the problem instance
   * @return the label at the source
   */
  public static ClassicLabel sourceLabel(Instance inst) {
    ClassicLabel label = new ClassicLabel(inst);
    label.partialPath.add((short)0);
    label.addVisited((short)0);
    return label;
  }
  
  /**
   * Creates a label starting at the sink.
   * 
   * @param inst the problem instance
   * @return the label at the sink
   */
  public static ClassicLabel sinkLabel(Instance inst) {
    ClassicLabel label = new ClassicLabel(inst);
    label.isFw = false;
    int num = inst.getSize();
    label.node = (short)(num + 1);
    label.partialPath.add((short)(num + 1));
    label.addVisited((short)(num + 1));
    return label;
  }
  
  @Override
  public boolean isExt() {
    return isExt;
  }

  @Override
  public double getDuration() {
    return totalDuration;
  }

  @Override
  public void setExt(boolean val) {
    this.isExt = val;
  }

  
  /**
   * Extends the current label along the edge e. If the extension is not feasible, it returns null
   * and marks the candidate node as infeasible for this label.
   * 
   * @param inst the problem instance
   * @param state the state of the algorithm
   * @param edge the edge along which to perform the extension
   * @return the label associated with the partial path ending at the new node, or null if it is 
   *     not possible.
   */
  @Override
  public Label extend(Instance inst, PricingState state, DirectedEdge edge) {
    // ps = state;
    ClassicLabel newL = new ClassicLabel(inst);
    final short newNode = (short) (isFw ? edge.to() : edge.from());
    newL.node = newNode;
    newL.isFw = isFw;
    newL.pathLength = pathLength;
    // experimental
    if (isInfeasible(newNode)) {
      return null;
    }
    // we check if the node is reachable and mark it
    Set<Short> theta = state.getOpt().usingLocalTheta() 
        ? state.getLocalTheta(newNode) : state.getTheta();
    if (theta.contains(newNode)) {
      //if (visited.contains(newNode)) { 
      if (!isReachable(newNode)) {
        return null;
      }
      newL.addVisited(newNode);
    } else {
      newL.pathLength++;
    }
    // "untravellable" arcs - lead to unreachable nodes
    if (edge.weight() == Double.POSITIVE_INFINITY) {
      return markInfeasible(state, newNode);
    }
    newL.totalDistance = (float) (this.totalDistance + edge.weight());
    newL.dual = (float) (this.dual + state.getPricesRs(node, newNode));
    // Capacity check
    newL.del = (float) (del + inst.getDemand(newNode));
    if (newL.getDel() > inst.getPar().getMaxCapacity()) {
      return markInfeasible(state, newNode);
    }
    final double tws = isFw ? inst.getTwsOf(newNode) : inst.getTwsBwRs(newNode);
    final double twe = isFw ? inst.getTweOf(newNode) : inst.getTweBwRs(newNode);
    final double svt = inst.getServiceTime(this.node);
    final double lim = isFw ? tws : (inst.getMax() - twe);
    final double limFeas = isFw ? twe : (inst.getMax() - tws);
    newL.totalDuration = (float) Math.max(this.totalDuration + svt + edge.weight(), lim);
    if (newL.totalDuration > limFeas) {
      return markInfeasible(state, newNode);
    }
    // Bounding
    if (newL.getDuration() > (inst.getMax() * .5)) {
      bounded.add(newNode);
      return null;
    }
    Lib.markDeletedAsInfeasible(inst, state, newL, newNode);
    Lib.updateVisited(state, this, newL, newNode);
    // finishing
    newL.getPartialPath().addAll(partialPath);
    newL.getPartialPath().add(newNode);
    newL.mother = this;
    return newL;
  }
  
  double newDuration(Instance inst, DirectedEdge edge) {
    final short newNode = (short) (isFw ? edge.to() : edge.from());
    final double tws = isFw ? inst.getTwsOf(newNode) : inst.getTwsBwRs(newNode);
    final double twe = isFw ? inst.getTweOf(newNode) : inst.getTweBwRs(newNode);
    final double svt = inst.getServiceTime(this.node);
    final double lim = isFw ? tws : (inst.getMax() - twe);
    return Math.max(this.totalDuration + svt + edge.weight(), lim);
  }
  
  private ClassicLabel markInfeasible(PricingState state, short newNode) {
    this.addInfeasible(newNode);
    return null;
  }

  @Override
  public boolean isVisited(short node) {
    return visited.get(node);
  }

  @Override
  public void addVisited(short node) {
    this.visited.set(node);
  }

  @Override
  public void addAllVisited(Collection<? extends Short> collection) {
    for (short s : collection) {
      this.visited.set(s);
    }
  }

  @Override
  public boolean isFw() {
    return isFw;
  }

  @Override
  public boolean dominates(Instance inst, PricingState state, Label opponent, Espprc.DomType dt) {
    if (opponent.getClass() != this.getClass()) {
      throw new IllegalArgumentException();
    }
    ClassicLabel that = (ClassicLabel) opponent;
    boolean[] flag = {true, false};
    compareRedCost(that, flag);
    if (dt == Espprc.DomType.EXACT) {
      compareDel(that, flag);
      compareDur(that, flag);
      compareElem(that, flag, state);
    }
    return flag[0] && flag[1];
  }
  
  private void compareRedCost(ClassicLabel that, boolean[] flag) {
    // double trc = this.getRedCost();
    // double thrc = that.getRedCost();
    flag[0] &= this.getRedCost() <= that.getRedCost() + Lib.TOLERANCE;
    flag[1] |= this.getRedCost() <  that.getRedCost() - Lib.TOLERANCE;
  }
  
  //compares the amount of demand satisfied
  private void compareDel(ClassicLabel that, boolean[] flag) {
    flag[0] &= this.getDel() <= that.getDel() + Lib.TOLERANCE; // Del
    flag[1] |= this.getDel() < that.getDel() - Lib.TOLERANCE;
  }
  
  private void compareDur(ClassicLabel that, boolean[] flag) {
    flag[0] &= this.getDuration() <= that.getDuration() + Lib.TOLERANCE;
    flag[1] |= this.getDuration() <  that.getDuration() - Lib.TOLERANCE;
  }
  
  //compares the elementarity resources, depending on the algorithm in use
  private void compareElem(ClassicLabel that, boolean[] flag, PricingState state) {
    boolean[] val = Lib.compareUnreachable(this, that, state);
    flag[0] &= val[0];
    flag[1] |= val[1];
    if (state.getOpt().getAlgo() == Options.Algo.DSSR_G 
        || state.getOpt().getAlgo() == Options.Algo.DSSR_L
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1_PLUS) {
      flag[0] &= this.pathLength <= that.pathLength + Lib.TOLERANCE;
      flag[1] |= this.pathLength < that.pathLength - Lib.TOLERANCE;
    }
  }
  
  @Override
  public short getNode() {
    return node;
  }

  @Override
  public double getRedCost() {
    return totalDistance - dual;
  }

  @Override
  public double getTotalDistance() {
    return totalDistance;
  }

  @Override
  public boolean isReachable(short node) {
    return (!isVisited(node) && !isInfeasible(node));
  }

  @Override
  public BitSet getVisited() {
    return visited;
  }

  @Override
  public List<Short> getSequence() {
    return partialPath;
  }

  @Override
  public BitSet getInfeasible() {
    return infeasible;
  }

  @Override
  public boolean isInfeasible(short node) {
    return infeasible.get(node);
  }

  @Override
  public void addInfeasible(short node) {
    infeasible.set(node);
  }

  @Override
  public void addAllInfeasible(Collection<? extends Short> collection) {
    for (short s : collection) {
      this.infeasible.set(s);
    }
  }

  @Override
  public double getDel() {
    return del;
  }

  @Override
  public List<Short> getPartialPath() {
    return partialPath;
  }

  @Override
  public Label getMother() {
    return mother;
  }
  
  @Override
  public String toString() {
    return partialPath.toString();
  }
  
  /**
   * Test client.
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    List<String> instances = options.getArgs();
    if (instances.isEmpty()) {
      return;
    }
    for (String i : instances) {
      Instance instance = new Instance(i, options);
      StdOut.println("Instance:\n" + instance);
      double[] dual = Lib.readDualPricesFromFile(
          new In("data/data100/Random/random_prizes_12.txt"), instance.getSize());
      PricingState ps = new PricingState(instance, 
          new State(instance, options, true), dual, options);
      ClassicLabel source = sourceLabel(instance);
      List<Short> seq = new ArrayList<>();
      seq.add((short) 1);
      seq.add((short) 2);
      seq.add((short) 3);
      seq.add((short) 4);
      seq.add((short) 6);
      Label lab = source;
      short prev = 0;
      for (short j : seq) {
        if (lab == null) {
          break;
        }
        DirectedEdge edge = instance.getG().edge(prev, j);
        lab = lab.extend(instance, ps, edge);
        prev = j;
      }
      if (lab == null) {
        StdOut.println("No label");
      } else {
        StdOut.println(lab);
      }
    }
  }
}
