package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.Queue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Class representing the data on which the pricing algorithm is working at any given moment,
 * or its "state". TODO complete doc
 * 
 * @author Michelini
 */
class PricingState {
  private short size;
  private PriorityQueue<Path> paths;
  private final Options options;
  private final State state;
  private final double[] prices;
  // DP-used fields
  private Map<Short, List<Label>> gammaFw;
  private Map<Short, List<Label>> gammaBw;
  private Queue<Short> nodesToExamine;
  private Set<Short> nodesToExamineAux;
  // DSSR-used fields
  private Set<Short> theta;
  private Map<Short, Set<Short>> localTheta;
  private Set<Short> psi; // set of nodes visited multiple times
  private double dssrLb;        // DSSR lower bound 
  private boolean plusSecondPhase = false;
  
  /** 
   * Constructor.
   * 
   * @param instance the problem instance
   * @param options the options for the algorithm
   */
  public PricingState(Instance instance, State state, double[] prices, Options options) {
    this.size = instance.getSize();
    this.options = options;
    this.state = state;
    this.prices = Lib.readDualPrices(prices, instance.getSize());
    theta   = new HashSet<>();
    localTheta = new HashMap<>();
    psi     = new HashSet<>();
    paths   = new PriorityQueue<>(11, new Path.ReducedCostMinOrder());
    gammaFw = new HashMap<>();
    gammaBw = new HashMap<>();
    dssrLb  = Double.NEGATIVE_INFINITY;
    
    if (options.usingGlobalTheta()) {
      initializeTheta(instance, options, true);
    } else if (options.usingLocalTheta()) {
      initializeTheta(instance, options, false);
      fillTheta();
    } else {
      fillTheta();
    }
  }
  
  private void fillTheta() {
    for (short k = 1; k <= size; k++) {
      theta.add(k);
    }
  }
  
  // TODO change when passing to new options
  private int getDssrInitAmount() {
    //return options.getDssrInitAmount();
    double amount = options.getDssrInitAmount(this.isRootNode());
    return Lib.ratio(amount, size);
  }
  
  public int getDssrInsPathAmount() {
    //return options.getInsPathAmount();
    double amount = options.getDssrInsPathAmount(this.isRootNode());
    switch (options.getDssrInsPathStr(isRootNode())) {
      case ONE_PATH:
        return 1;
      case ALL_PATHS:
        return this.getNumCol();
      default:
        break;
    }
    return Lib.ratio(amount, size);
  }
  
  private void initializeTheta(Instance instance, Options options, boolean isGlobal) {
    SortedMap<Double, Short> hca = new TreeMap<>();
    SortedMap<Double, Short> tca = new TreeMap<>();
    SortedMap<Double, Short> whca = new TreeMap<>();
    SortedMap<Double, Short> wtca = new TreeMap<>();
    HashSet<Short> finalSet = new HashSet<>();
    switch (options.getDssrInitStr(this.isRootNode())) {
      case HCA:
        for (short i = 1; i < instance.getSize(); i++) {
          hca.put(Lib.getHca(instance, prices, i, false), i);
        }
        for (int i = 0; i < getDssrInitAmount(); i++) {
          if (!hca.isEmpty()) {
            finalSet.add(hca.remove(hca.lastKey())); // uses natural ordering, highest value is last
          }
        }
        break;
      case TCA:
        for (short i = 1; i < instance.getSize(); i++) {
          tca.put(Lib.getTca(instance, prices, i, false), i);
        }
        for (int i = 0; i < getDssrInitAmount(); i++) {
          if (!tca.isEmpty()) {
            finalSet.add(tca.remove(tca.lastKey()));
          }
        }
        break;
      case WHCA:
        for (short i = 1; i < instance.getSize(); i++) {
          whca.put(Lib.getHca(instance, prices, i, true), i);
        }
        for (int i = 0; i < getDssrInitAmount(); i++) {
          if (!whca.isEmpty()) {
            finalSet.add(whca.remove(whca.lastKey())); 
          }
        }
        break;
      case WTCA:
        for (short i = 1; i < instance.getSize(); i++) {
          wtca.put(Lib.getTca(instance, prices, i, true), i);
        }
        for (int i = 0; i < getDssrInitAmount(); i++) {
          if (!wtca.isEmpty()) {
            finalSet.add(wtca.remove(wtca.lastKey())); 
          }
        }
        break;
      case MIXED:
        for (short i = 1; i <= instance.getSize(); i++) {
          hca.put(Lib.getHca(instance, prices, i, false), i);
          tca.put(Lib.getTca(instance, prices, i, false), i);
          whca.put(Lib.getHca(instance, prices, i, true), i);
          wtca.put(Lib.getTca(instance, prices, i, true), i);
        }
        HashSet<Short> temp1 = new HashSet<>();
        HashSet<Short> temp2 = new HashSet<>();
        HashSet<Short> temp3 = new HashSet<>();
        for (int i = 0; i < getDssrInitAmount(); i++) {
          if (!hca.isEmpty()) {
            temp1.add(hca.remove(hca.lastKey()));
          }
          if (!tca.isEmpty()) {
            temp2.add(tca.remove(tca.lastKey())); 
          }
          if (!whca.isEmpty()) {
            temp3.add(whca.remove(whca.lastKey())); 
          }
          if (!wtca.isEmpty()) {
            finalSet.add(wtca.remove(wtca.lastKey())); 
          }
        }
        finalSet.retainAll(temp1);
        finalSet.retainAll(temp2);
        finalSet.retainAll(temp3);
        break;
      default: 
    }
    if (isGlobal) {
      theta.addAll(finalSet);
    } else {
      initLocalTheta();
      addToLocalTheta(finalSet);
    }
  }
  
  private void initLocalTheta() {
    for (short i = 0; i <= size + 1; i++) {
      localTheta.put(i, new HashSet<>());
    }
  }
  
  // adds the contents of the set to all the local thetas
  private void addToLocalTheta(Set<Short> set) {
    for (short node : set) {
      for (short i = 0; i <= size + 1; i++) {
        Set<Short> lt = localTheta.get(i);
        lt.add(node);
      }
    }
  }
  
  public boolean plusSecondPhase() {
    return plusSecondPhase;
  }
  
  public void switchPlus() {
    plusSecondPhase = true;
  }
  
  public Set<Short> getLocalTheta(short node) {
    return localTheta.get(node);
  }

  public Map<Short, Set<Short>> getLocalThetas() {
    return localTheta;
  }
  
  public Set<Short> getNgSetAt(short node) {
    return state.getNgSetAt(node);
  }
  
  public Set<Short> getTempNgSetAt(short node) {
    return state.getTempNgSetAt(node);
  }

  public Map<Short, Set<Short>> getTempNgSets() {
    return state.getTempNgSet();
  }
  
  public boolean isForbidden(short from, short to) {
    return state.isForbidden(from, to);
  }
  
  public boolean isRootNode() {
    return state.isRootNode();
  }

  public short size() {
    return size;
  }
  
  public List<Label> getGammaFwAt(short node) {
    return gammaFw.get(node);
  }
  
  public List<Label> getGammaBwAt(short node) {
    return gammaBw.get(node);
  }
  
  public double[] getPrices() {
    return prices;
  }
  
  public double getPrice(short node) {
    return prices[node];
  }
  
  public double getPricesRs(short n1, short n2) {
    return (prices[n1] + prices[n2]) / 2;
  } 
  
  /**
   * Returns a node to be examined in the label extension phase of the algorithm.
   * 
   * @return the node to examine
   */
  public short pollNodeToExamine() {
    short node = nodesToExamine.dequeue();
    nodesToExamineAux.remove(node);
    return node;
  }
  
  /**
   * Adds a node to the set of the ones to be examined 
   * in the label extension phase of the algorithm.
   * 
   * @param node the node to add to the set
   */
  public void addNodeToExamine(short node) {
    if (!nodesToExamineAux.contains(node)) {
      nodesToExamine.enqueue(node);
      nodesToExamineAux.add(node);
    }
  }
  
  public boolean areThereNodesToExamine() {
    return !nodesToExamine.isEmpty();
  }
  
  /**
   * Empties the set of nodes to be examined and resets it to start a new iteration.
   * 
   * @param numOfNodes the cardinality of the problem
   */
  public void resetNodesToExamine(short numOfNodes, boolean isHeuristic) {
    nodesToExamine = new Queue<>();
    nodesToExamineAux = new HashSet<>();
    if (isHeuristic) {
      for (short i = 1; i <= numOfNodes; i++) {
        nodesToExamine.enqueue(i);
        nodesToExamineAux.add(i);
      }
    } else {
      nodesToExamine.enqueue((short)0);
      nodesToExamineAux.add((short)0);
      nodesToExamine.enqueue((short)(numOfNodes + 1));
      nodesToExamineAux.add((short)(numOfNodes + 1));
    }
  }
  
  public boolean doesThetaContain(short node) {
    return theta.contains(node);
  }
  
  public boolean areTherePaths() {
    return !paths.isEmpty();
  }
  
  public int howManyPaths() {
    return paths.size();
  }
  
  public Path getMinPath() {
    return paths.peek();
  }
  
  public boolean isPsiEmpty() {
    return psi.isEmpty();
  }
  
  public Set<Short> getTheta() {
    return theta;
  }
  
  public void setTheta(HashSet<Short> val) {
    this.theta = val;
  }
  
  public Set<Short> getPsi() {
    return psi;
  }

  public void setPsi(HashSet<Short> psi) {
    this.psi = psi;
  }
  
  public State getState() {
    return state;
  }

  public Map<Short, List<Label>> getGammaFw() {
    return gammaFw;
  }
  
  public void setGammaFw(HashMap<Short, List<Label>> val) {
    this.gammaFw = val;
  }
  
  public Map<Short, List<Label>> getGammaBw() {
    return gammaBw;
  }
  
  public void setGammaBw(HashMap<Short, List<Label>> val) {
    this.gammaBw = val;
  }
  
  public Queue<Short> getNodesToExamine() {
    return nodesToExamine;
  }
  
  public void setNodesToExamine(Queue<Short> val) {
    this.nodesToExamine = val;
  }
  
  public Set<Short> getNodesToExamineAux() {
    return nodesToExamineAux;
  }
  
  public void setNodesToExamineAux(Set<Short> val) {
    this.nodesToExamineAux = val;
  }
  
  public PriorityQueue<Path> getPaths() {
    return paths;
  }

  public void setPaths(PriorityQueue<Path> val) {
    this.paths = val;
  }

  public double getDssrLowerBound() {
    return dssrLb;
  }

  public void setDssrLowerBound(double val) {
    this.dssrLb = val;
  }

  public Options getOpt() {
    return options;
  }
  
  public Options.DssrInsNodeStr getDssrInsNodeStr() {
    return this.options.getDssrInsNodeStr(this.isRootNode());
  }
  
  public double getDssrInsNodeAmount() {
    return this.options.getDssrInsNodeAmount(this.isRootNode());
  }
  
  public int getNumCol() {
    double amount = options.getNumCol(this.isRootNode());
    return Lib.ratio(amount, size);
  }
  
  public int getConcatStop() {
    double amount = options.getConcatStop(this.isRootNode());
    return Lib.ratio(amount, size);
  }

  public void updateNgSets(List<Short> list, short node, short index) {
    state.updateNgSets(list, node, index);
  }
}
