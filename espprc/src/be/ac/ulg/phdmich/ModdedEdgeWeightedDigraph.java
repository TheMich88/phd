package be.ac.ulg.phdmich;
/*************************************************************************
 *  Compilation:  javac ModEdgeWeightedDigraph.java
 *  Execution:    java ModEdgeWeightedDigraph V E
 *  Dependencies: StdOut.java
 *
 *  <p>An edge-weighted digraph, implemented using an adjacency matrix.
 *  Parallel edges are disallowed; self-loops are allowed.
 *  
 *  <p>MODIFIED VERSION 
 *************************************************************************/

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *  This class is a modification of the original data type for a edge-weighted
 *  digraph based on an adjacency-matrix. Ulterior supported operations are 
 *  edge removal and edge retrieval, given tail and head nodes.
 * 
 *  <p>Description of the original class: 
 * 
 *  <p>The <tt>AdjMatrixEdgeWeightedDigraph</tt> class represents a edge-weighted
 *  digraph of vertices named 0 through <em>V</em> - 1, where each
 *  directed edge is of type {@link DirectedEdge} and has a real-valued weight.
 *  It supports the following two primary operations: add a directed edge
 *  to the digraph and iterate over all of edges incident from a given vertex.
 *  It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges are disallowed; self-loops are permitted.
 *  
 *  <p>This implementation uses an adjacency-matrix representation.
 *  All operations take constant time (in the worst case) except
 *  iterating over the edges incident from a given vertex, which takes
 *  time proportional to <em>V</em>.
 *  
 *  <p>For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/44sp">Section 4.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class ModdedEdgeWeightedDigraph {
  private int vertices;
  private int edges;
  private DirectedEdge[][] adj;

  /**
   * Initializes an empty edge-weighted digraph with <tt>V</tt> vertices and 0 edges.
   * param V the number of vertices
   * @throws java.lang.IllegalArgumentException if <tt>V</tt> < 0
   */
  public ModdedEdgeWeightedDigraph(int num) {
    if (num < 0) {
      throw new RuntimeException("Number of vertices must be nonnegative");
    }
    this.vertices = num;
    this.edges = 0;
    this.adj = new DirectedEdge[num][num];
  }

  /**
   * Initializes a random edge-weighted digraph with <tt>V</tt> vertices and <em>E</em> edges.
   * param V the number of vertices
   * param E the number of edges
   * @throws java.lang.IllegalArgumentException if <tt>V</tt> < 0
   * @throws java.lang.IllegalArgumentException if <tt>E</tt> < 0
   */
  public ModdedEdgeWeightedDigraph(int vertexNum, int edgeNum) {
    this(vertexNum);
    if (edgeNum < 0) {
      throw new RuntimeException("Number of edges must be nonnegative");
    }
    if (edgeNum > vertexNum * vertexNum) {
      throw new RuntimeException("Too many edges");
    }

    // can be inefficient
    while (this.edges != edgeNum) {
      int v1 = (int) (vertexNum * Math.random());
      int v2 = (int) (vertexNum * Math.random());
      double weight = Math.round(100 * Math.random()) / 100.0;
      addEdge(new DirectedEdge(v1, v2, weight));
    }
  }

  /**
   * Returns the number of vertices in the edge-weighted digraph.
   * @return the number of vertices in the edge-weighted digraph
   */
  public int getVertices() {
    return vertices;
  }

  /**
   * Returns the number of edges in the edge-weighted digraph.
   * @return the number of edges in the edge-weighted digraph
   */
  public int getEdges() {
    return edges;
  }
  
  public DirectedEdge getEdge(int tail, int head) {
    return adj[tail][head];
  }

  /**
   * Adds the directed edge <tt>e</tt> to the edge-weighted digraph (if there
   * is not already an edge with the same endpoints).
   * @param edge the edge
   */
  public void addEdge(DirectedEdge edge) {
    int v1 = edge.from();
    int v2 = edge.to();
    if (adj[v1][v2] == null) {
      edges++;
      adj[v1][v2] = edge;
    }
  }
  
  /**
   * Removes the directed edge incident from vertex <tt>v</tt> to vertex <tt>w</tt>.
   * @param v1 the tail vertex 
   * @param v2 the head vertex
   * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V and 0 <= w < V
   */
  public void removeEdge(int v1, int v2) {
    adj[v1][v2] = null;
  }

  /**
   * Returns the directed edge incident from vertex <tt>v</tt> to vertex <tt>w</tt>,
   * or null if it does not exist.
   * 
   * @param v1 the tail vertex 
   * @param v2 the head vertex
   * @return the directed edge incident from vertex <tt>v</tt> to vertex <tt>w</tt>, 
   *     or null if it does not exist.
   * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V and 0 <= w < V
   */
  public DirectedEdge edge(int v1, int v2) {
    return adj[v1][v2];
  }

  /**
   * Returns the directed edges incident from vertex <tt>v</tt>.
   * @param v1 the vertex
   * @return the directed edges incident from vertex <tt>v</tt> as an Iterable
   * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
   */
  public Iterable<DirectedEdge> adj(int v1) {
    return new AdjIterator(v1);
  }

  /**
   * Returns the directed edges incident to vertex <tt>v</tt>.
   * @param v1 the vertex
   * @return the directed edges incident to vertex <tt>v</tt> as an Iterable
   * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
   */
  public Iterable<DirectedEdge> adjTo(int v1) {
    return new AdjIteratorBw(v1);
  }

  // support iteration over graph vertices
  private class AdjIterator implements Iterator<DirectedEdge>, Iterable<DirectedEdge> {
    private int v1 = 0;
    private int v2 = 0;
    
    AdjIterator(int val) {
      this.v1 = val;
    }

    public Iterator<DirectedEdge> iterator() {
      return this;
    }

    public boolean hasNext() {
      while (v2 < vertices) {
        if (adj[v1][v2] != null) {
          return true;
        }
        v2++;
      }
      return false;
    }

    public DirectedEdge next() {
      if (hasNext()) {
        return adj[v1][v2++];
      } else {
        throw new NoSuchElementException();
      }
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  // support iteration over graph vertices - to navigate arcs backwards
  private class AdjIteratorBw implements Iterator<DirectedEdge>, Iterable<DirectedEdge> {
    private int v1 = 0;
    private int v2 = 0;
    
    AdjIteratorBw(int val) {
      this.v1 = val;
    }

    public Iterator<DirectedEdge> iterator() {
      return this;
    }

    public boolean hasNext() {
      while (v2 < vertices) {
        if (adj[v2][v1] != null) {
          return true;
        }
        v2++;
      }
      return false;
    }

    public DirectedEdge next() {
      if (hasNext()) {
        return adj[v2++][v1];
      } else {
        throw new NoSuchElementException();
      }
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  /**
   * Returns a string representation of the edge-weighted digraph. This method takes
   * time proportional to <em>V</em><sup>2</sup>.
   * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
   followed by the <em>V</em> adjacency lists of edges
   */
  public String toString() {
    final String newLine = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    str.append(vertices + " " + edges + newLine);
    for (int v = 0; v < vertices; v++) {
      str.append(v + ": ");
      for (DirectedEdge e : adj(v)) {
        str.append(e + "  ");
      }
      str.append(newLine);
    }
    return str.toString();
  }

  /**
   * Unit tests the <tt>AdjMatrixEdgeWeightedDigraph</tt> data type.
   */
  public static void main(String[] args) {
    int vert = Integer.parseInt(args[0]);
    int edg = Integer.parseInt(args[1]);
    ModdedEdgeWeightedDigraph graph = new ModdedEdgeWeightedDigraph(vert, edg);
    StdOut.println(graph);
  }

}