package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Out;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class CgClient {
  static final String data = "data/";
  static final String para = data + "Param.txt";
  static final String[] out_dest = { 
      data + "out/out_50_c.txt",
      data + "out/out_50_rc.txt",
      data + "out/out_50_r.txt",
      data + "out/out_100_c.txt",
      data + "out/out_100_rc.txt",
      data + "out/out_100_r.txt",};
  static final String[] paths = {
      "data50/Clustered/",
      "data50/Random-Clustered/",
      "data50/Random/",
      "data100/Clustered/",
      "data100/Random-Clustered/",
      "data100/Random/" };
  static final String[] names = {
      data + "out/sol_50_C_",
      data + "out/sol_50_RC_",
      data + "out/sol_50_R_",
      data + "out/sol_100_C_",
      data + "out/sol_100_RC_",
      data + "out/sol_100_R_" };
  static final int[] nums = { 9, 8, 12, 9, 8, 12 };
  static final String[] s_50_none = { "25", "none", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_50_dssr = { "25", "dssr", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_50_dssr_l = { "25", "dssr_l", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_50_ng = { "25", "ng", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_50_h1 = { "25", "hybrid1", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_50_h2 = { "25", "hybrid2", "mixed", "5", "IN_BETWEEN", 
      "15", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_none = { "50", "none", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_dssr = { "50", "dssr", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_dssr_l = { "50", "dssr_l", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_ng = { "50", "ng", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_h1 = { "50", "hybrid1", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  static final String[] s_100_h2 = { "50", "hybrid2", "mixed", "10", "in_between", 
      "25", "all_nodes", "0.8", "max_duration", "15", "0.9"};
  
  static final Options op_50_none  = new Options(s_50_none);
  static final Options op_50_dssr  = new Options(s_50_dssr);
  static final Options op_50_dssr_l  = new Options(s_50_dssr_l);
  static final Options op_50_ng    = new Options(s_50_ng);
  static final Options op_50_h1    = new Options(s_50_h1);
  static final Options op_50_h2    = new Options(s_50_h2);
  static final Options op_100_none = new Options(s_100_none);
  static final Options op_100_dssr = new Options(s_100_dssr);
  static final Options op_100_dssr_l = new Options(s_100_dssr_l);
  static final Options op_100_ng   = new Options(s_100_ng);
  static final Options op_100_h1   = new Options(s_100_h1);
  static final Options op_100_h2   = new Options(s_100_h2);
  static final Options[] op_50 = { op_50_none, op_50_dssr, op_50_dssr_l, 
      op_50_ng, op_50_h1, op_50_h2 };
  static final Options[] op_100 = { op_100_none, op_100_dssr, op_100_dssr_l, 
      op_100_ng, op_100_h1, op_100_h2 };
  
  
  private static void runInstance(int instClass, int instNum, Out out) {
    String file = data + paths[instClass] + instNum + ".txt";
    In paraFile = new In(para);
    In instFile = new In(file);
    Instance instance = new Instance(paraFile, instFile);
    Options[] op = instClass < 3 ? op_50 : op_100;
    out.print(instNum);
    for (int i = 0; i < op.length; i++) {
      System.gc();
      StdDraw.clear();
      //instance.draw();
      Options options = op[i];
      State state = new State(instance, options, true);
      ColumnGeneration cg = new ColumnGeneration(instance, state, options);
      StdOut.println(cg);
      String solFileName = names[instClass] + instNum + "_opt_" + (i + 1);
      Out solFile = new Out(solFileName + ".txt");
      Solution sol = cg.getSolution();
      solFile.println(sol);
      sol.draw();
      StdDraw.save(solFileName + ".png");
      solFile.close();
      out.print(cg.getResult());
    }
    out.println("\\\\");
  }
  
  /**
   * Program for batch runs of the column generation algorithm.
   * 
   * @param args the program arguments - none needed
   */
  public static void main(String[] args) {
    for (int k = 0; k < paths.length; k++) {
      StdOut.println("*************************************************************************");
      StdOut.println("Set " + paths[k] + ", " + nums[k] + " instances: ");
      StdOut.println("*************************************************************************");
      //out.println("Set " + paths[k] + ": ");
      Out out = new Out(out_dest[k]);
      out.println("\\hline");
      out.println("n & val & t & it & ex it & col "
          + "& val & t & it & ex it & col "
          + "& val & t & it & ex it & col "
          + "& val & t & it & ex it & col "
          + "& val & t & it & ex it & col\\\\");
      for (int h = 1; h <= nums[k]; h++) {
        out.println("\\hline");
        runInstance(k, h, out);
      }
      out.println("\\hline");
      out.close();
    }
  }
}
