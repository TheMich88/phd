package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Out;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StopwatchCPU;

import java.util.HashSet;
import java.util.Set;

public class Espprc {
  private Path minPath;
  private PricingState pstate;
  private double elapsedTime;
  private StopwatchCPU stopwatch;
  private String type;
  private String domRule;
  private boolean stopped = false;
  
  public enum DomType { EXACT, HEUR_1, HEUR_2 }
  
  public Espprc(Instance instance, PricingState pstate, 
      Options options, String isHeur, String dominRule) {
    this(instance, pstate, options, isHeur, dominRule, false);
  }
  
  /**
   * Constructor. TODO complete doc
   *
   * @param instance the problem instance
   * @param options the options for the algorithm
   */
  public Espprc(Instance instance, PricingState pstate,
      Options options, String isHeur, String dominRule, boolean log) {
    // TODO need to wrap this in a time limited thread or something?
    stopwatch = new StopwatchCPU();
    if (log) {
      Lib.log("Starting...", stopwatch);
    }
    // First initialization
    this.pstate = pstate;
    boolean isHeuristic = (isHeur.equalsIgnoreCase("heuristic"));
    DomType dt;
    switch (dominRule.toUpperCase()) {
      case "HEURISTIC":
        dt = DomType.HEUR_1;
        this.domRule = "heuristic";
        break;
      case "HEURISTIC2":
        dt = DomType.HEUR_2;
        this.domRule = "heuristic2";
        break;
      default:
        dt = DomType.EXACT;
        this.domRule = "exact";
    }
    this.type = isHeuristic ? "heuristic" : "exact";
    double minCost = 0;
    boolean pathsAreInvalid = false;
    // DSSR main loop
    do {
      Lib.initialize(instance, pstate, isHeuristic);
      if (log) {
        Lib.log("Initialized, starting search...", stopwatch);
      }
      
      // Search
      do {
        short currentNode = pstate.pollNodeToExamine();
        // forward extension
        Lib.labelExtension(instance, pstate, options, currentNode, true, dt);
        if (Thread.interrupted()) {
          return;
        }
        // backward extension
        Lib.labelExtension(instance, pstate, options, currentNode, false, dt);
        // this takes a lot of computation time
        // if (timeLimiter != null && timeLimiter.elapsedTime() > options.getTimeLimit()) {
        //   this.stopped = true;
        //   return;
        // }
        if (Thread.interrupted()) {
          return;
        }
      } while (pstate.areThereNodesToExamine());

      if (log) {
        Lib.log("Terminated extensions.\nConcatenating labels...", stopwatch);
      }
      boolean ok = Lib.concatenateAllLabels(instance, pstate, isHeuristic);
      if (!ok) {
        this.stopped = true;
        return;
      }
      //      StdOut.println("Fw labels in sink: " + pstate.getGammaFwAt(pstate.size() + 1).size()
      //          + "\nBw labels in source: " + pstate.getGammaBwAt(0).size()
      //          + "\nConcatenated paths: " + pstate.getPaths().size());
      if (!pstate.areTherePaths()) {
        if (log) {
          Lib.log("No feasible paths found.", stopwatch);
        }
        break;
      }
      Path currentMinPath = pstate.getMinPath();
      if (log) {
        Lib.log("Best path found: " + currentMinPath, stopwatch);
        Lib.log("Paths generated: " + pstate.howManyPaths(), stopwatch);
      }

      if (pstate.getOpt().getAlgo() != Options.Algo.NONE
          && pstate.getOpt().getAlgo() != Options.Algo.NG) {
        if (log) {
          Lib.log("Checking best " + pstate.getDssrInsPathAmount() 
              + " paths...", stopwatch);
        }
        pathsAreInvalid = checkViolations();
      }
      minCost = currentMinPath.getReducedCost();
      pstate.setDssrLowerBound(Math.max(pstate.getDssrLowerBound(), minCost));

      if (log) {
        // checking the best solution found
        if (pathsAreInvalid) {
          Lib.log("Invalid multiple visits detected.", stopwatch);
          if (minCost >= -Lib.TOLERANCE) {
            StdOut.println("Reduced cost non negative. Terminating...");
          } else {
            StdOut.println("Iterating again...");
          }
        } else {
          Lib.log("No invalid multiple visits detected. Terminating...", stopwatch);
        } 
      }
    } while (minCost <= -Lib.TOLERANCE && pathsAreInvalid);

    elapsedTime = stopwatch.elapsedTime();
    minPath = pstate.getPaths().peek();
    boolean test = false;
    if (test) {
      Path path = pstate.getPaths().poll();
      int countNeg = 1;
      if (path != null && path.getReducedCost() < 0) {
        while (path.getReducedCost() < 0) {
          StdOut.println((countNeg++) + ") TEST NEG: " + path);
          path = pstate.getPaths().poll();
        }
      }
    }
  }
  
  public boolean isStopped() {
    return stopped;
  }

  public Espprc(Instance instance, State state, Options options, 
      double[] prices, String isHeur, String dominRule) {
    this(instance, new PricingState(instance, state, prices, options), 
        options, isHeur, dominRule, true);
  }

  
  public String getType() {
    return type;
  }
  
  public String getDominationRule() {
    return domRule;
  }
  
  // Checks whether the first path is valid or not: 
  // if yes it returns false, otherwise it checks for violations 
  // in the next paths as specified by the options
  private boolean checkViolations() {
    boolean bestPathIsInvalid = false;
    Set<Path> support = new HashSet<>();
    if (pstate.getPaths().isEmpty()) {
      return false;
    }
    Path bestPath = pstate.getPaths().remove();
    support.add(bestPath);
    bestPathIsInvalid |= Lib.multipleVisits(bestPath, pstate, true);
    if (bestPathIsInvalid) {
      for (int i = 0; i < pstate.getDssrInsPathAmount() - 1; i++) {
        if (pstate.getPaths().isEmpty()) {
          break;
        }
        Path path = pstate.getPaths().remove();
        support.add(path);
        if (path.getReducedCost() > - Lib.TOLERANCE) {
          break;
        }
        Lib.multipleVisits(path, pstate, false);
      }
    }
    pstate.getPaths().addAll(support);
    return bestPathIsInvalid;
  }

  /**
   * Writes a text file with the results of the algorithm.
   *
   * @param filename the name of the output file.
   */
  public void output(String filename) {
    Out out = new Out(filename);
    if (minPath == null) {
      out.println("No feasible paths found.");
    } else {
      out.println("Best path: " + minPath.toString());
      out.println("Total paths: " + pstate.getPaths().size());
      out.println("Elapsed seconds: " + elapsedTime);
      int count = 1;
      while (!pstate.getPaths().isEmpty()) {
        out.printf(count + ")\t" + pstate.getPaths().remove().toString());
        count++;
      }
    }
    out.println();
    out.close();
  }
  
  /**
   * Once the ESPPRC is solved, gets the best k paths that are valid among all the 
   * ones with negative reduced cost.
   * 
   * @return the best (at most) k valid paths
   */
  public Set<Path> getPaths() {
    Set<Path> set = new HashSet<>();
    Set<Path> support = new HashSet<>();
    int validPathsExtracted = 0;
    int numCol = pstate.getNumCol();
    while (!pstate.getPaths().isEmpty() && validPathsExtracted < numCol) {
      Path path = pstate.getPaths().remove();
      support.add(path);
      if (path.getReducedCost() > - Lib.TOLERANCE) {
        break;
      } else {
        //StdOut.println(path.getReducedCost() + ", " + (0 - Lib.TOLERANCE));
      }
      if (//true
          //path.isElementary()
          Lib.isValid(path, pstate.getState(), pstate.getOpt())
          ) {
        validPathsExtracted++;
        set.add(path);
      }
    }
    pstate.getPaths().addAll(support);
    return set;
  }

  @Override
  public String toString() {
    String newline = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    if (minPath == null) {
      str.append("No feasible paths found.");
      str.append(newline);
    } else {
      str.append("Best path: " + minPath.toString());
      str.append(newline);
      str.append("Total paths: " + pstate.getPaths().size());
      str.append(newline);
      str.append("Elapsed seconds: " + elapsedTime);
      str.append(newline);
    }
    return str.toString();
  }

  public Path minPath() {
    return minPath;
  }

  /**
   * Test client for the class.
   *
   * @param args the program arguments
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    String pfile = "data/Param.txt";
    String ifile = "data/data100/Random/12.txt";
    Instance instance = new Instance(pfile, ifile);
    State state = new State(instance, options, true);
    In dualFile = new In("data/data100/Random/random_prizes_12.txt");
    double[] dual = Lib.readDualPricesFromFile(dualFile, instance.getSize());
    Espprc espprc = new Espprc(instance, state, options, dual, "heuristi", "heuristic2");
    if (!espprc.getPaths().isEmpty()) {
      Path path = new Path(espprc.minPath().getSequence(), instance);
      StdOut.println(espprc);
      StdOut.println(path + "\nPath feasible: " + path.isFeasible());
    }
  }
}
