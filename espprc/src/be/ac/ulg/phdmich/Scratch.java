package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.StdOut;

import java.util.List;

class Scratch {

  /**
   * Test client for the package.
   * 
   * @param args program arguments
   */
  public static void main(String[] args) {
    // Options options = new Options(args);
    // String pfile = "data/Param.txt";
    // String ifile = "data/data100/Random-Clustered/2.txt";
    // Instance instance = new Instance(pfile, ifile);
    
    List<Integer> list = Lib.shuffledList(10);
    for (int i : list) {
      StdOut.println(i);
    }
  }
}
