package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Class supporting. TODO complete doc
 * 
 * @author Mich
 */
class ExactLabel implements Label {
  private short node = 0;
  private Instance inst;
  // data fields as in the paper
  private float totalDistance = 0;
  private float staticCost = 0;       // delta
  private float dual = 0; // sum of dual prices
  private float totalTravel = 0;      // theta
  private float latestStartTime = Float.POSITIVE_INFINITY;  // l
  private float earliestServTime = Float.NEGATIVE_INFINITY; // a_tilde
  private float dynamicCost = 0;      // A
  private float del = 0;
  private BitSet visited ; // nodes actually in the path
  // it is infeasible to extend a label to these nodes
  private BitSet infeasible ;
  // length of path sigma, with regards to non-critical nodes, as in Righini and Salani
  private short pathLength = 0;         
  // other info
  private boolean isFw = true;
  private boolean isExt = false; // false iff it has never been considered for extension
  private ExactLabel mother = null;            
  private List<Short> partialPath = new ArrayList<>();
  
  public ExactLabel(Instance inst) {
    this.inst = inst;
    this.visited = new BitSet(inst.getSize() + 2);
    this.infeasible = new BitSet(inst.getSize() + 2);
  }
  
  /**
   * Returns travel time plus the waiting on the partial path.
   * 
   * @return the total duration of the partial path represented by the label
   */
  public double getDuration() {
    if (getLst() < Double.POSITIVE_INFINITY) {
      return Math.max(getTotalTravel(), getEst() - getLst());
    } else {
      return 0;
    }
  }
  
  /**
   * Creates a label starting at the source.
   * 
   * @param inst the problem instance
   * @return the label at the source
   */
  public static ExactLabel sourceLabel(Instance inst) {
    ExactLabel label = new ExactLabel(inst);
    label.partialPath.add((short)0);
    label.addVisited((short)0);
    return label;
  }
  
  /**
   * Creates a label starting at the sink.
   * 
   * @param inst the problem instance
   * @return the label at the sink
   */
  public static ExactLabel sinkLabel(Instance inst) {
    ExactLabel label = new ExactLabel(inst);
    label.isFw = false;
    int num = inst.getSize();
    label.node = (short)(num + 1);
    label.partialPath.add((short)(num + 1));
    label.addVisited((short)(num + 1));
    return label;
  }

  /**
   * Extends the current label along the edge e. If the extension is not feasible, it returns null
   * and marks the candidate node as infeasible for this label.
   * 
   * @param inst the problem instance
   * @param state the state of the algorithm
   * @param edge the edge along which to perform the extension
   * @return the label associated with the partial path ending at the new node, or null if it is 
   *     not possible.
   */
  public ExactLabel extend(Instance inst, PricingState state, DirectedEdge edge) {
    ExactLabel newL = new ExactLabel(inst);
    final short newNode = (short) (isFw ? edge.to() : edge.from());
    newL.node = newNode;
    newL.isFw = isFw;
    newL.pathLength = pathLength;
    // experimental
    if (isInfeasible(newNode)) {
      return null;
    }
    // we check if the node is reachable and mark it
    Set<Short> theta = state.getOpt().usingLocalTheta() 
        ? state.getLocalTheta(newNode) : state.getTheta();
    if (theta.contains(newNode)) {
      //if (visited.contains(newNode)) { 
      if (!isReachable(newNode)) {
        return null;
      }
      newL.addVisited(newNode);
    } else {
      newL.pathLength++;
    }
    // "untravellable" arcs - lead to unreachable nodes
    if (edge.weight() == Double.POSITIVE_INFINITY) {
      return markInfeasible(state, newNode);
    }
    newL.totalDistance = (float) (this.totalDistance + edge.weight());
    // static cost - includes dual prizes
    newL.staticCost = (float) (this.staticCost 
        + inst.getPar().getDistUnitCost() * edge.weight());
    newL.dual = (float) (this.dual + state.getPrice(newNode));
    // Capacity check
    newL.del = (float) (del + inst.getDemand(newNode));
    if (newL.getDel() > inst.getPar().getMaxCapacity()) {
      return markInfeasible(state, newNode);
    }
    // Time-related computations
    final double[] tws = isFw ? inst.getTws() : inst.getTwsBw();
    final double[] twe = isFw ? inst.getTwe() : inst.getTweBw();
    final double barT = edge.weight() + inst.getServiceTime(edge.from());
    
    newL.totalTravel = (float) (totalTravel + barT);
    newL.latestStartTime = (float) Math.min(latestStartTime, twe[newNode] - newL.getTotalTravel());
    newL.earliestServTime = (float) Math.max(tws[newNode], earliestServTime + barT);
    
    // Infeasible w/r to waiting
    if (newL.getEst() > twe[newNode]) {
      return markInfeasible(state, newNode);
    }
    // Infeasible w/r to duration
    if (newL.getDuration() > inst.getPar().getMaxDuration()) {
      return markInfeasible(state, newNode);
    }
    // Bounding
    if (newL.getDuration() > (inst.getPar().getMaxDuration() * .5)) {
      return null;
    }
    newL.dynamicCost = (float) Math.max(
        dynamicCost + inst.getPar().getTimeUnitCost() * barT,
        inst.getPar().getTimeUnitCost() * (newL.getEst() - newL.getLst()));
    // This apparently was causing a bug where on certain occasions
    // certain reduced cost paths were *not* generated when they should have
    Lib.markDeletedAsInfeasible(inst, state, newL, newNode);
    Lib.updateVisited(state, this, newL, newNode);
    // finishing
    newL.getPartialPath().addAll(partialPath);
    newL.getPartialPath().add(newNode);
    newL.mother = this;
    return newL;
  }

  public BitSet getInfeasible() {
    return infeasible;
  }

  public void addInfeasible(short node) {
    //infeasible.add(node);
    infeasible.set(node);
  }
  
  public void addAllInfeasible(Collection<? extends Short> collection) {
    // this.infeasible.addAll(collection);
    for (short s : collection) {
      this.infeasible.set(s);
    }
  }
  
  public void addVisited(short node) {
    //this.visited.add(node);
    this.visited.set(node);
  }
  
  public void addAllVisited(Collection<? extends Short> collection) {
    //this.visited.addAll(collection);
    for (short s : collection) {
      this.visited.set(s);
    }
  }

  private ExactLabel markInfeasible(PricingState state, short newNode) {
    this.addInfeasible(newNode);
    return null;
  }

  /**
   * Compares two label by applying domination rules.
   * 
   * @param inst the problem instance
   * @param state the state of the algorithm
   * @param opponent the label against which dominance is tested
   * @return true iff l1 dominates l2
   */
  public boolean dominates(Instance inst, PricingState state, 
      Label opponent, Espprc.DomType dt) {
    if (opponent.getClass() != this.getClass()) {
      throw new IllegalArgumentException();
    }
    ExactLabel that = (ExactLabel) opponent;
    boolean[] flag = {true, false};
    
    boolean test = (state.getOpt().getDomRule() == Options.DomRule.EXPER); // to test Hande's idea
    
    if (dt == Espprc.DomType.HEUR_1) {
      //BF heuristic
      return (this.getRedCost() < that.getRedCost()); 
    } else if (dt == Espprc.DomType.HEUR_2) {
      // experimental heuristic
      if (test) {
        compareDualLatest(that, flag);
      } else {
        compareSCost(that, flag);
      }
      compareDCost(that, flag);
      return flag[0] && flag[1];
    } else {
      if (test) {
        compareDualLatest(that, flag);        
      } else {
        compareSCost(that, flag);
        compareLst(that, flag);        
      }
      compareDCost(that, flag);
      compareEst(that, flag);
      compareDel(that, flag);
      // compare elementarity
      compareElem(that, flag, state);
      return flag[0] && flag[1];
    }
  }

  // compare the reduced cost
  @SuppressWarnings("unused")
  private void compareRedCost(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getRedCost() <= that.getRedCost();
    flag[1] |= this.getRedCost() > that.getRedCost();
  }

  // compare the static cost (includes dual prices)
  private void compareSCost(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getSCost() <= that.getSCost() + Lib.TOLERANCE; // delta
    flag[1] |= this.getSCost() < that.getSCost() - Lib.TOLERANCE;
  }

  // compares the dynamic cost (duration)
  private void compareDCost(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getDCost() <= that.getDCost() + Lib.TOLERANCE; // A
    flag[1] |= this.getDCost() < that.getDCost() - Lib.TOLERANCE;
  }

  // compares the latest feasible departure time
  private void compareLst(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getLst() >= that.getLst() - Lib.TOLERANCE; // l
    flag[1] |= this.getLst() > that.getLst() + Lib.TOLERANCE;
  }

  // compares the earliest feasible service time
  private void compareEst(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getEst() <= that.getEst() + Lib.TOLERANCE; // a_tilde (E)
    flag[1] |= this.getEst() < that.getEst() - Lib.TOLERANCE;
  }

  // compares the amount of demand satisfied
  private void compareDel(ExactLabel that, boolean[] flag) {
    flag[0] &= this.getDel() <= that.getDel() + Lib.TOLERANCE; // Del
    flag[1] |= this.getDel() < that.getDel() - Lib.TOLERANCE;
  }
  
  private void compareDualLatest(ExactLabel that, boolean[] flag) {
    double cmp = that.dual - this.inst.getPar().getTimeUnitCost() 
        * Math.min(0, this.latestStartTime - that.latestStartTime);
    flag[0] &= this.dual >= cmp - Lib.TOLERANCE;
    flag[1] |= this.dual >  cmp + Lib.TOLERANCE;
  }

  // compares the elementarity resources, depending on the algorithm in use
  private void compareElem(ExactLabel that, boolean[] flag, PricingState state) {
    boolean[] val = Lib.compareUnreachable(this, that, state);
    flag[0] &= val[0];
    flag[1] |= val[1];
    if (state.getOpt().getAlgo() == Options.Algo.DSSR_G 
        || state.getOpt().getAlgo() == Options.Algo.DSSR_L
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1
        || state.getOpt().getAlgo() == Options.Algo.HYBRID1_PLUS) {
      flag[0] &= this.pathLength <= that.pathLength + Lib.TOLERANCE;
      flag[1] |= this.pathLength < that.pathLength - Lib.TOLERANCE;
    }
  }
  
  public boolean isVisited(short node) {
    //return visited.contains(node);
    return visited.get(node);
  }
  
  public boolean isInfeasible(short node) {
    //return infeasible.contains(node);
    return infeasible.get(node);
  }
  
  public boolean isReachable(short node) {
    return (!isVisited(node) && !isInfeasible(node));
  }

  public short getNode() {
    return node;
  }

  public boolean isExt() {
    return isExt;
  }

  public void setExt(boolean val) {
    this.isExt = val;
  }

  public boolean isFw() {
    return isFw;
  }

  public double getDel() {
    return del;
  }

  public double getTotalTravel() {
    return totalTravel;
  }

  public double getEst() {
    return earliestServTime;
  }

  public double getLst() {
    return latestStartTime;
  }

  public BitSet getVisited() {
    return visited;
  }
  
  public List<Short> getSequence() {
    return partialPath;
  }
  
  public double getTotalDistance() {
    return totalDistance;
  }

  public double getSCost() {
    return staticCost - dual;
  }
  
  public double getDual() {
    return dual;
  }

  public double getDCost() {
    return dynamicCost;
  }
  
  @Override
  public double getRedCost() {
    return staticCost + dynamicCost;
  }

  public Label getMother() {
    return mother;
  }
  
  public List<Short> getPartialPath() {
    return partialPath;
  }
  
  @Override
  public String toString() {
    return partialPath.toString();
  }
}
