package be.ac.ulg.phdmich;

import java.util.HashMap;

public class SolutionContainer {
  private Solution sol;
  
  public SolutionContainer(Instance inst) {
    sol = new Solution(Double.POSITIVE_INFINITY, new HashMap<>(), inst, true);
  }
  
  public Solution getSolution() {
    return sol;
  }
  
  /**
   * Adds a solution to the container.
   * 
   * @param newSol the candidate solution
   */
  public void addSol(Solution newSol) {
    if (newSol.isIntegral() && newSol.getCost() < sol.getCost()) {
      sol = newSol;
    }
  }
}
