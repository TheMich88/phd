package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;

interface Label {

  boolean isExt();

  double getDuration();

  void setExt(boolean isext);

  Label extend(Instance inst, PricingState state, DirectedEdge edge);

  boolean isVisited(short to);
  
  void addVisited(short node);
  
  void addAllVisited(Collection<? extends Short> collection);

  boolean isFw();

  boolean dominates(Instance inst, PricingState state, 
      Label newLabel, Espprc.DomType dt);

  short getNode();
  
  double getRedCost();
  
  double getTotalDistance();

  boolean isReachable(short from);

  BitSet getVisited();
  
  List<Short> getSequence();
  
  BitSet getInfeasible();

  boolean isInfeasible(short node);
  
  void addInfeasible(short node);
  
  void addAllInfeasible(Collection<? extends Short> collection);
  
  double getDel();

  List<Short> getPartialPath();
  
  Label getMother();
}
