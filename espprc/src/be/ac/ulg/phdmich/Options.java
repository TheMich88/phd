package be.ac.ulg.phdmich;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;


public final class Options {
  private CommandLine cmd;
  private CommandLineParser parser = new DefaultParser();
  private org.apache.commons.cli.Options options = new org.apache.commons.cli.Options();

  public enum Prog { CG, BP }

  public enum Algo {
    NONE, DSSR_G, DSSR_L,
    NG, HYBRID1, HYBRID2,
    HYBRID1_PLUS, HYBRID2_PLUS
  }

  public enum DssrInitStr { HCA, TCA, WHCA, WTCA, MIXED, NONE }

  public enum DssrInsPathStr { ONE_PATH, ALL_PATHS, IN_BETWEEN }

  public enum DssrInsNodeStr { ONE_NODE, ALL_NODES, IN_BETWEEN }

  public enum NgSetType { TRAVEL_TIME, CHEAP_CYCLE_RISK, MIXED }

  public enum TreeNav { BEST_FIRST, DEPTH_FIRST, BREADTH_FIRST, WEIRD }

  public enum HeurUse { NONE, ONE, TWO, BOTH, START }
  
  public enum DomRule { NORMAL, EXPER }
  
  public enum IpModel { SCOVER, SPART }
  
  /* *************DEFAULTS**************** */
  
  public static final           Prog PROG_DEFAULT                  = Prog.BP;
  public static final            int SIZE_DEFAULT                  = Integer.MAX_VALUE;
  public static final         double CAPACITY_DEFAULT              = 200;
  public static final         double SHIFT_DEFAULT                 = -1;
  public static final         double DIST_UNIT_COST_DEFAULT        = 0;
  public static final         double TIME_UNIT_COST_DEFAULT        = 1;
  public static final            int TIME_LIMIT_DEFAULT            = 21600;
  public static final         double NUM_COL_DEFAULT               = 1;
  public static final           Algo ALGO_DEFAULT                  = Algo.DSSR_G;
  public static final    DssrInitStr DSSR_INIT_STR_DEFAULT         = DssrInitStr.MIXED;
  public static final         double DSSR_INIT_AMOUNT_DEFAULT      = .1;
  public static final DssrInsPathStr DSSR_INS_STR_PATH_DEFAULT     = DssrInsPathStr.ALL_PATHS;
  public static final         double DSSR_INS_PATH_AMOUNT_DEFAULT  = 0.25;
  public static final DssrInsNodeStr DSSR_INS_STR_NODE_DEFAULT     = DssrInsNodeStr.ALL_NODES;
  public static final         double DSSR_INS_NODE_PERCENT_DEFAULT = .75;
  public static final      NgSetType NG_SET_TYPE_DEFAULT           = NgSetType.MIXED;
  public static final         double NG_SIZE_DEFAULT               = .15;
  public static final         double NG_MIX_ALPHA_DEFAULT          = .5;
  public static final            int CONCAT_STOP_DEFAULT           = 100;
  public static final        TreeNav TREE_NAV_DEFAULT              = TreeNav.BEST_FIRST;
  public static final        HeurUse HEUR_USE_DEFAULT              = HeurUse.TWO;
  public static final        DomRule DOM_RULE_DEFAULT              = DomRule.EXPER;  
  public static final        IpModel MIP_MODEL_NG_DEFAULT          = IpModel.SPART;  
  public static final        IpModel MIP_MODEL_EXACT_DEFAULT       = IpModel.SCOVER;
  public static final         double SOLVER_TIME_DEFAULT           = 10800;
  //public static final         String OUT_DEFAULT                   = "out.txt";
  
  /* **********************Attributes******************************** */
  
  private Prog    program;
  private int     size;
  private double  capacity;
  private double  shift;
  private double  distCost;
  private double  timeCost;
  private int     timeLimit;
  private Algo    algo;
  private TreeNav treeNav;
  
  private double         numCol;
  private double         concatStop;
  private DssrInitStr    dssrInitStr;
  private double         dssrInitAmount;
  private DssrInsPathStr dssrInsPathStr;
  private double         dssrInsPathAmount;
  private DssrInsNodeStr dssrInsNodeStr;
  private double         dssrInsNodeAmount;
  private NgSetType      ngSetType;
  private double         ngSetSize;
  private double         ngMixAlpha;
  
  private double         numColRoot;
  private double         concatStopRoot;
  private DssrInitStr    dssrInitStrRoot;
  private double         dssrInitAmountRoot;
  private DssrInsPathStr dssrInsPathStrRoot;
  private double         dssrInsPathAmountRoot;
  private DssrInsNodeStr dssrInsNodeStrRoot;
  private double         dssrInsNodeAmountRoot;
  private NgSetType      ngSetTypeRoot;
  private double         ngSetSizeRoot;
  private double         ngMixAlphaRoot;
  
  private HeurUse heurUse;
  private DomRule domRule;
  private IpModel ipModel;
  private double  solverTime;
  private String  outFileName = "";
  
  /**
   * Constructor.
   * 
   * @param args program arguments
   */
  public Options(String[] args) {
    options.addOption("h", "help", false, "Print this message");
    options.addOption(Option.builder("p")
        .longOpt("program")
        .desc("Specify the program to run: column-generation (cg) "
            + "or branch-and-price (bp)")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder("o")
        .longOpt("output")
        .desc("Specify the output file")
        .hasArg()
        .argName("FILENAME")
        .build());
    options.addOption(Option.builder()
        .longOpt("ip-model")
        .desc("Set if the MIP model at root is set cover or set partition")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder()
        .longOpt("solver-time")
        .desc("Set the time limit for the MIP solver")
        .hasArg()
        .argName("SECONDS")
        .build());
    options.addOption(Option.builder()
        .longOpt("domination-rule")
        .desc("Specify the domination rule") //TODO describe defaults
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder("s")
        .longOpt("print-solution")
        .desc("Print solution values to the program log")
        .build());
    options.addOption(Option.builder()
        .longOpt("check-solution")
        .desc("Checks feasibility of returned solution for the given instance")
        .build());
    options.addOption(Option.builder()
        .longOpt("verbose")
        .desc("Run in verbose mode")
        .build());
    options.addOption(Option.builder("n")
        .longOpt("number-customers")
        .desc("Override the maximum number of customers")
        .hasArg()
        .argName("N")
        .build());
    options.addOption(Option.builder("Q")
        .longOpt("capacity")
        .desc("Override the maximum capacity for the vehicles")
        .hasArg()
        .argName("CAPACITY")
        .build());
    options.addOption(Option.builder()
        .longOpt("half-horizon")
        .desc("Set the maximum shift duration to half horizon")
        .build());
    options.addOption(Option.builder()
        .longOpt("full-horizon")
        .desc("Set the maximum shift duration to full horizon")
        .build());
    options.addOption(Option.builder("S")
        .longOpt("shift")
        .desc("Override the maximum shift duration for the vehicles")
        .hasArg()
        .argName("SHIFT")
        .build());
    options.addOption(Option.builder("d")
        .longOpt("dist-unit-cost")
        .desc("Multiplier for the distance cost per unit")
        .hasArg()
        .argName("COEFFICIENT")
        .build());
    options.addOption(Option.builder("t")
        .longOpt("time-unit-cost")
        .desc("Multiplier for the time cost per unit")
        .hasArg()
        .argName("COEFFICIENT")
        .build());
    options.addOption(Option.builder("T")
        .longOpt("time-limit")
        .desc("Set the time limit for the program")
        .hasArg()
        .argName("T")
        .build());
    options.addOption(Option.builder("c")
        .longOpt("classic")
        .desc("Solve the classic VRPTW")
        .build());
    options.addOption(Option.builder("a") // 1
        .longOpt("algorithm")
        .desc("Pricing problem algorithm to use")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 2
        .longOpt("num-col")
        .desc("Maximum number of columns to insert at each CG iteration, "
            + "as a percentage of the problem size")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 3
        .longOpt("dssr-init-strategy")
        .desc("Ininitialization strategy for DSSR")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 4
        .longOpt("dssr-init-amount")
        .desc("Amount needed for DSSR initialization, in percent of the problem size")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 5
        .longOpt("dssr-insert-path-strategy")
        .desc("Insertion strategy for DSSR, with regards to the paths to examine")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 6
        .longOpt("dssr-insert-path-amount")
        .desc("Maximum amount of paths to check at the end of a DSSR iteration,"
            + " in percent of the problem size")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 7
        .longOpt("dssr-insert-node-strategy")
        .desc("Insertion strategy for DSSR, with regards to the nodes to include")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 8
        .longOpt("dssr-insert-node-amount")
        .desc("Percentage (per path) of invalid nodes to mark as critical "
            + "at the end of a DSSR iteration")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 9
        .longOpt("ng-set-type")
        .desc("Type of metric used to build the ng sets")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 10
        .longOpt("ng-set-size")
        .desc("Size of the ng sets, as a percentage of the problem size")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 11
        .longOpt("ng-mix-alpha")
        .desc("Affine coefficient for the ng-set mixed metric")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 12
        .longOpt("concat-stop")
        .desc("Multiplier for the threshold for early concatenation stop, "
            + "with respect to the problem size")
        .hasArg()
        .argName("NUMBER")
        .build());
    options.addOption(Option.builder() // 13
        .longOpt("tree-navigation")
        .desc("Strategy for navigation of the branch-and-bound tree")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 12
        .longOpt("heuristic-use")
        .desc("Defines the heuristic use in the CG algorithm: none, 1, 2, or both")
        .hasArg()
        .argName("TYPE")
        .build()); 
    // options at root node
    options.addOption(Option.builder() // 2
        .longOpt("num-col-root")
        .desc("Maximum number of columns to insert at each CG iteration, "
            + "as a percentage of the problem size, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 3
        .longOpt("dssr-init-strategy-root")
        .desc("Ininitialization strategy for DSSR, only at the root node")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 4
        .longOpt("dssr-init-amount-root")
        .desc("Amount needed for DSSR initialization,"
            + " in percent of the problem size, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 5
        .longOpt("dssr-insert-path-strategy-root")
        .desc("Insertion strategy for DSSR,"
            + " with regards to the paths to examine, only at the root node")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 6
        .longOpt("dssr-insert-path-amount-root")
        .desc("Maximum amount of paths to check at the end of a DSSR iteration,"
            + " in percent of the problem size, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 7
        .longOpt("dssr-insert-node-strategy-root")
        .desc("Insertion strategy for DSSR, "
            + "with regards to the nodes to include, only at the root node")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 8
        .longOpt("dssr-insert-node-amount-root")
        .desc("Percentage (per path) of invalid nodes to mark as critical "
            + "at the end of a DSSR iteration, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 9
        .longOpt("ng-set-type-root")
        .desc("Type of metric used to build the ng sets, only at the root node")
        .hasArg()
        .argName("NAME")
        .build());
    options.addOption(Option.builder() // 10
        .longOpt("ng-set-size-root")
        .desc("Size of the ng sets, as a percentage of the problem size, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 11
        .longOpt("ng-mix-alpha-root")
        .desc("Affine coefficient for the ng-set mixed metric, only at the root node")
        .hasArg()
        .argName("PERCENTAGE")
        .build());
    options.addOption(Option.builder() // 12
        .longOpt("concat-stop-root")
        .desc("Multiplier for the threshold for early concatenation stop, "
            + "with respect to the problem size, only at the root node")
        .hasArg()
        .argName("NUMBER")
        .build());
    options.addOption(Option.builder()
        .longOpt("eager-ip")
        .desc("Solve the integer program at every iteration of column generation at the root node")
        .build());
    options.addOption(Option.builder()
        .longOpt("monitor-bounds")
        .desc("Report the best bounds obtained at every iteration of column generation. "
            + "Inactive if --eager-ip-solve is not activated")
        .build());
    options.addOption(Option.builder()
        .longOpt("no-salvage")
        .desc("By default, the program attempts to use available columns to generate or improve "
            + "the feasible solution if it times out. This switch turns this procedure off.")
        .build());
    options.addOption(Option.builder()
        .longOpt("tuning")
        .desc("Use this flag to remove first two lines of standard output log"
            + " and allow output to be readable by IRACE.")
        .build());
    try {
      cmd = parser.parse(options, args);
      // type of program
      if (cmd.hasOption("program")) {
        switch (cmd.getOptionValue("program").toUpperCase()) {
          case "BP": case "BRANCH-AND-PRICE":
            program = Prog.BP;
            break;
          case "CG": case "COLUMN-GENERATION":
            program = Prog.CG;
            break;
          default:
            program = PROG_DEFAULT;
        }
      } else {
        program = PROG_DEFAULT;
      }
      // problem size
      if (cmd.hasOption("number-customers")) {
        size = Integer.parseInt(cmd.getOptionValue("number-customers"));
      } else {
        size = SIZE_DEFAULT;
      }
      // output file
      if (cmd.hasOption("output")) {
        outFileName = cmd.getOptionValue("output");
      } else {
        outFileName = "";
      }
      // capacity
      if (cmd.hasOption("capacity")) {
        capacity = Double.parseDouble(cmd.getOptionValue("capacity"));
      } else {
        capacity = CAPACITY_DEFAULT;
      }
      // shift
      if (cmd.hasOption("shift")) {
        shift = Double.parseDouble(cmd.getOptionValue("shift"));
      } else if (cmd.hasOption("full-horizon")) {
        shift = -1;
      } else if (cmd.hasOption("half-horizon")) {
        shift = 0;
      } else {
        shift = SHIFT_DEFAULT;
      }
      // distance unit cost
      if (cmd.hasOption("dist-unit-cost")) {
        distCost = Double.parseDouble(cmd.getOptionValue("dist-unit-cost"));
      } else {
        distCost = DIST_UNIT_COST_DEFAULT;
      }
      // time unit cost
      if (cmd.hasOption("time-unit-cost")) {
        timeCost = Double.parseDouble(cmd.getOptionValue("time-unit-cost"));
      } else {
        timeCost = TIME_UNIT_COST_DEFAULT;
      }
      // time limit
      if (cmd.hasOption("time-limit")) {
        timeLimit = Integer.parseInt(cmd.getOptionValue("time-limit"));
      } else {
        timeLimit = TIME_LIMIT_DEFAULT;
      }
      // solver time limit
      if (cmd.hasOption("solver-time")) {
        solverTime = Double.parseDouble(cmd.getOptionValue("solver-time"));
      } else {
        solverTime = SOLVER_TIME_DEFAULT;
      }
      // tree navigation strategy
      if (cmd.hasOption("tree-navigation")) {
        switch (cmd.getOptionValue("tree-navigation").toUpperCase()) {
          case "BEST-FIRST":
            treeNav = TreeNav.BEST_FIRST;
            break;
          case "DEPTH-FIRST":
            treeNav = TreeNav.DEPTH_FIRST;
            break;
          case "BREADTH-FIRST":
            treeNav = TreeNav.BREADTH_FIRST;
            break;
          default:
            treeNav = TREE_NAV_DEFAULT;
        }
      } else {
        treeNav = TREE_NAV_DEFAULT;
      }
      // pricing algorithm
      if (cmd.hasOption("algorithm")) {
        switch (cmd.getOptionValue("algorithm").toUpperCase()) {
          case "NONE":
            algo = Algo.NONE;
            break;
          case "DSSR-G": case "DSSR":
            algo = Algo.DSSR_G;
            break;
          case "DSSR-L":
            algo = Algo.DSSR_L;
            break;
          case "NG":
            algo = Algo.NG;
            break;
          case "HYBRID1": case "NG-DSSR-G":
            algo = Algo.HYBRID1;
            break;
          case "HYBRID1-PLUS": case "NG-DSSR-G-CORR":
            algo = Algo.HYBRID1_PLUS;
            break;
          case "HYBRID2": case "NG-DSSR-L":
            algo = Algo.HYBRID2;
            break;
          case "HYBRID2-PLUS": case "NG-DSSR-L-CORR":
            algo = Algo.HYBRID2_PLUS;
            break;
          default:
            algo = ALGO_DEFAULT;
        }
      } else {
        algo = ALGO_DEFAULT;
      }
      // mip model
      if (cmd.hasOption("ip-model")) {
        switch (cmd.getOptionValue("ip-model").toUpperCase()) {
          case "SP":
            ipModel = IpModel.SPART;
            break;
          case "SC":
            ipModel = IpModel.SCOVER;
            break;
          default:
            if (givesNgRoutes()) {
              ipModel = MIP_MODEL_NG_DEFAULT;
            } else {
              ipModel = MIP_MODEL_EXACT_DEFAULT;
            }
        }
      } else {
        if (givesNgRoutes()) {
          ipModel = MIP_MODEL_NG_DEFAULT;
        } else {
          ipModel = MIP_MODEL_EXACT_DEFAULT;
        }
      }
      // num columns (non root)
      if (cmd.hasOption("num-col")) {
        numCol = Double.parseDouble(cmd.getOptionValue("num-col"));
      } else {
        numCol = NUM_COL_DEFAULT;
      }
      // concatenation stop multiplier (non root)
      if (cmd.hasOption("concat-stop")) {
        concatStop = Integer.parseInt(cmd.getOptionValue("concat-stop"));
      } else {
        concatStop = CONCAT_STOP_DEFAULT;
      }
      // dssr init strategy (non root)
      if (cmd.hasOption("dssr-init-strategy")) {
        switch (cmd.getOptionValue("dssr-init-strategy").toUpperCase()) {
          case "NONE":
            dssrInitStr = DssrInitStr.NONE;
            break;
          case "HCA":
            dssrInitStr = DssrInitStr.HCA;
            break;
          case "TCA":
            dssrInitStr = DssrInitStr.TCA;
            break;
          case "WHCA":
            dssrInitStr = DssrInitStr.WHCA;
            break;
          case "WTCA":
            dssrInitStr = DssrInitStr.WTCA;
            break;
          case "MIXED":
            dssrInitStr = DssrInitStr.MIXED;
            break;
          default:
            dssrInitStr = DSSR_INIT_STR_DEFAULT;
        }
      } else {
        dssrInitStr = DSSR_INIT_STR_DEFAULT;
      }
      // dssr init amount (non root)
      if (cmd.hasOption("dssr-init-amount")) {
        dssrInitAmount = Double.parseDouble(cmd.getOptionValue("dssr-init-amount"));
      } else {
        dssrInitAmount = DSSR_INIT_AMOUNT_DEFAULT;
      }
      // dssr insert path strategy (non root)
      if (cmd.hasOption("dssr-insert-path-strategy")) {
        switch (cmd.getOptionValue("dssr-insert-path-strategy").toUpperCase()) {
          case "ONE-PATH":
            dssrInsPathStr = DssrInsPathStr.ONE_PATH;
            break;
          case "IN-BETWEEN":
            dssrInsPathStr = DssrInsPathStr.IN_BETWEEN;
            break;
          case "ALL-PATHS":
            dssrInsPathStr = DssrInsPathStr.ALL_PATHS;
            break;
          default:
            dssrInsPathStr = DSSR_INS_STR_PATH_DEFAULT;
        }
      } else {
        dssrInsPathStr = DSSR_INS_STR_PATH_DEFAULT;
      }
      // dssr insert path amount (non root)
      if (cmd.hasOption("dssr-insert-path-amount")) {
        dssrInsPathAmount = Double.parseDouble(cmd.getOptionValue("dssr-insert-path-amount"));
      } else {
        dssrInsPathAmount = DSSR_INS_PATH_AMOUNT_DEFAULT;
      }
      // dssr insert node strategy (non root)
      if (cmd.hasOption("dssr-insert-node-strategy")) {
        switch (cmd.getOptionValue("dssr-insert-node-strategy").toUpperCase()) {
          case "ONE-PATH":
            dssrInsNodeStr = DssrInsNodeStr.ONE_NODE;
            break;
          case "IN-BETWEEN":
            dssrInsNodeStr = DssrInsNodeStr.IN_BETWEEN;
            break;
          case "ALL-PATHS":
            dssrInsNodeStr = DssrInsNodeStr.ALL_NODES;
            break;
          default:
            dssrInsNodeStr = DSSR_INS_STR_NODE_DEFAULT;
        }
      } else {
        dssrInsNodeStr = DSSR_INS_STR_NODE_DEFAULT;
      }
      // dssr insert node amount (non root)
      if (cmd.hasOption("dssr-insert-node-amount")) {
        dssrInsNodeAmount = Double.parseDouble(cmd.getOptionValue("dssr-insert-node-amount"));
      } else {
        dssrInsNodeAmount = DSSR_INS_NODE_PERCENT_DEFAULT;
      }
      // ng set type (non root)
      if (cmd.hasOption("ng-set-type")) {
        switch (cmd.getOptionValue("ng-set-type").toUpperCase()) {
          case "TRAVEL-TIME":
            ngSetType = NgSetType.TRAVEL_TIME;
            break;
          case "CYCLE-RISK":
            ngSetType = NgSetType.CHEAP_CYCLE_RISK;
            break;
          case "MIXED":
            ngSetType = NgSetType.MIXED;
            break;
          default:
            ngSetType = NG_SET_TYPE_DEFAULT;
        }
      } else {
        ngSetType = NG_SET_TYPE_DEFAULT;
      }
      // ng set size (non root)
      if (cmd.hasOption("ng-set-size")) {
        ngSetSize = Double.parseDouble(cmd.getOptionValue("ng-set-size"));
      } else {
        ngSetSize = NG_SIZE_DEFAULT;
      }
      // ng mixing alpha (non root)
      if (cmd.hasOption("ng-mix-alpha")) {
        ngMixAlpha = Double.parseDouble(cmd.getOptionValue("ng-mix-alpha"));
      } else {
        ngMixAlpha = NG_MIX_ALPHA_DEFAULT;
      }
      // num columns (root)
      if (cmd.hasOption("num-col-root")) {
        numColRoot = Double.parseDouble(cmd.getOptionValue("num-col-root"));
      } else {
        numColRoot = NUM_COL_DEFAULT;
      }
      // concatenation stop multiplier (root)
      if (cmd.hasOption("concat-stop-root")) {
        concatStopRoot = Integer.parseInt(cmd.getOptionValue("concat-stop-root"));
      } else {
        concatStopRoot = CONCAT_STOP_DEFAULT;
      }
      // dssr init strategy (root)
      if (cmd.hasOption("dssr-init-strategy-root")) {
        switch (cmd.getOptionValue("dssr-init-strategy-root").toUpperCase()) {
          case "NONE":
            dssrInitStrRoot = DssrInitStr.NONE;
            break;
          case "HCA":
            dssrInitStrRoot = DssrInitStr.HCA;
            break;
          case "TCA":
            dssrInitStrRoot = DssrInitStr.TCA;
            break;
          case "WHCA":
            dssrInitStrRoot = DssrInitStr.WHCA;
            break;
          case "WTCA":
            dssrInitStrRoot = DssrInitStr.WTCA;
            break;
          case "MIXED":
            dssrInitStrRoot = DssrInitStr.MIXED;
            break;
          default:
            dssrInitStrRoot = DSSR_INIT_STR_DEFAULT;
        }
      } else {
        dssrInitStrRoot = DSSR_INIT_STR_DEFAULT;
      }
      // dssr init amount (root)
      if (cmd.hasOption("dssr-init-amount-root")) {
        dssrInitAmountRoot = Double.parseDouble(cmd.getOptionValue("dssr-init-amount-root"));
      } else {
        dssrInitAmountRoot = DSSR_INIT_AMOUNT_DEFAULT;
      }
      // dssr insert path strategy (root)
      if (cmd.hasOption("dssr-insert-path-strategy-root")) {
        switch (cmd.getOptionValue("dssr-insert-path-strategy-root").toUpperCase()) {
          case "ONE-PATH":
            dssrInsPathStrRoot = DssrInsPathStr.ONE_PATH;
            break;
          case "IN-BETWEEN":
            dssrInsPathStrRoot = DssrInsPathStr.IN_BETWEEN;
            break;
          case "ALL-PATHS":
            dssrInsPathStrRoot = DssrInsPathStr.ALL_PATHS;
            break;
          default:
            dssrInsPathStrRoot = DSSR_INS_STR_PATH_DEFAULT;
        }
      } else {
        dssrInsPathStrRoot = DSSR_INS_STR_PATH_DEFAULT;
      }
      // dssr insert path amount (root)
      if (cmd.hasOption("dssr-insert-path-amount-root")) {
        dssrInsPathAmountRoot = 
            Double.parseDouble(cmd.getOptionValue("dssr-insert-path-amount-root"));
      } else {
        dssrInsPathAmountRoot = DSSR_INS_PATH_AMOUNT_DEFAULT;
      }
      // dssr insert node strategy (root)
      if (cmd.hasOption("dssr-insert-node-strategy-root")) {
        switch (cmd.getOptionValue("dssr-insert-node-strategy-root").toUpperCase()) {
          case "ONE-PATH":
            dssrInsNodeStrRoot = DssrInsNodeStr.ONE_NODE;
            break;
          case "IN-BETWEEN":
            dssrInsNodeStrRoot = DssrInsNodeStr.IN_BETWEEN;
            break;
          case "ALL-PATHS":
            dssrInsNodeStrRoot = DssrInsNodeStr.ALL_NODES;
            break;
          default:
            dssrInsNodeStrRoot = DSSR_INS_STR_NODE_DEFAULT;
        }
      } else {
        dssrInsNodeStrRoot = DSSR_INS_STR_NODE_DEFAULT;
      }
      // dssr insert node amount (root)
      if (cmd.hasOption("dssr-insert-node-amount-root")) {
        dssrInsNodeAmountRoot = 
            Double.parseDouble(cmd.getOptionValue("dssr-insert-node-amount-root"));
      } else {
        dssrInsNodeAmountRoot = DSSR_INS_NODE_PERCENT_DEFAULT;
      }
      // ng set type (root)
      if (cmd.hasOption("ng-set-type-root")) {
        switch (cmd.getOptionValue("ng-set-type-root").toUpperCase()) {
          case "TRAVEL-TIME":
            ngSetTypeRoot = NgSetType.TRAVEL_TIME;
            break;
          case "CYCLE-RISK":
            ngSetTypeRoot = NgSetType.CHEAP_CYCLE_RISK;
            break;
          case "MIXED":
            ngSetTypeRoot = NgSetType.MIXED;
            break;
          default:
            ngSetTypeRoot = NG_SET_TYPE_DEFAULT;
        }
      } else {
        ngSetTypeRoot = NG_SET_TYPE_DEFAULT;
      }
      // ng set size (root)
      if (cmd.hasOption("ng-set-size-root")) {
        ngSetSizeRoot = Double.parseDouble(cmd.getOptionValue("ng-set-size-root"));
      } else {
        ngSetSizeRoot = NG_SIZE_DEFAULT;
      }
      // ng mixing alpha (root)
      if (cmd.hasOption("ng-mix-alpha-root")) {
        ngMixAlphaRoot = Double.parseDouble(cmd.getOptionValue("ng-mix-alpha-root"));
      } else {
        ngMixAlphaRoot = NG_MIX_ALPHA_DEFAULT;
      }
      // heuristic type
      if (cmd.hasOption("heuristic-use")) {
        switch (cmd.getOptionValue("heuristic-use").toUpperCase()) {
          case "NONE":
            heurUse = HeurUse.NONE;
            break;
          case "1": case "ONE":
            heurUse = HeurUse.ONE;
            break;
          case "2": case "TWO":
            heurUse = HeurUse.TWO;
            break;
          case "BOTH":
            heurUse = HeurUse.BOTH;
            break;
          case "START":
            heurUse = HeurUse.START;
            break;
          default:
            heurUse = HEUR_USE_DEFAULT;
        }
      } else {
        heurUse = HEUR_USE_DEFAULT;
      }
      // domination rules type
      if (cmd.hasOption("domination-rule")) {
        switch (cmd.getOptionValue("domination-rule").toUpperCase()) {
          case "NORMAL":
            domRule = DomRule.NORMAL;
            break;
          case "EXP": case "EXPERIMENTAL":
            domRule = DomRule.EXPER;
            break;
          default:
            domRule = DOM_RULE_DEFAULT;
        }
      } else {
        domRule = DOM_RULE_DEFAULT;
      }  
    } catch (ParseException ex) {
      System.out.println("Unexpected exception:" + ex.getMessage());
    }
  }

  public boolean askHelp() {
    return cmd.hasOption("h");
  }
  
  public String getOutFilename() {
    return outFileName;
  }
  
  public boolean hasOuput() {
    return cmd.hasOption("output");
  }
  
  public boolean hasCapacity() {
    return cmd.hasOption("capacity");
  }
  
  public void setOutFilename(String out) {
    this.outFileName = out;
  }
  
  public boolean printSolution() {
    return cmd.hasOption("print-solution");
  }
  
  public boolean checkSolution() {
    return cmd.hasOption("check-solution");
  }

  /**
   * Prints a help message.
   * 
   * @param name message to print with the help
   */
  public void printHelp(String name) {
    String header = "Do something useful with an input file\n\n";
    String footer = "\nPlease report issues at http://example.com/issues";
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(name, header, options, footer);
  }
  
  boolean givesNgRoutes() {
    return (getAlgo() == Algo.NG
        || getAlgo() == Algo.HYBRID1
        || getAlgo() == Algo.HYBRID2);
  }

  boolean usingGlobalTheta() {
    return (getAlgo() == Algo.DSSR_G
        || getAlgo() == Algo.HYBRID1
        || getAlgo() == Algo.HYBRID1_PLUS);
  }

  boolean usingNgSets() {
    return !(getAlgo() == Algo.NONE
        || getAlgo() == Algo.DSSR_G
        || getAlgo() == Algo.DSSR_L);
  }

  boolean usingTempNgSets() {
    return (getAlgo() == Algo.HYBRID2
        || getAlgo() == Algo.HYBRID2_PLUS);
  }

  boolean usingLocalTheta() {
    return (getAlgo() == Algo.DSSR_L);
  }

  boolean usingPlus() {
    return (getAlgo() == Algo.HYBRID1_PLUS
        || getAlgo() == Algo.HYBRID2_PLUS);
  }

  public Algo getAlgo() {
    return algo;
  }
  
  /**
   * Returns the amount of columns to insert in the model.
   * 
   * @param onRoot whether the algorithm is on the root node
   * @return the amount of columns to insert
   */
  public double getNumCol(boolean onRoot) {
    //return onRoot ? numColRoot : numCol;
    return Math.min(getConcatStop(onRoot), onRoot ? numColRoot : numCol);
  }

  public DssrInitStr getDssrInitStr(boolean onRoot) {
    return onRoot ? dssrInitStrRoot : dssrInitStr;
  }

  public double getDssrInitAmount(boolean onRoot) {
    return onRoot ? dssrInitAmountRoot : dssrInitAmount;
  }

  public DssrInsPathStr getDssrInsPathStr(boolean onRoot) {
    return onRoot ? dssrInsPathStrRoot : dssrInsPathStr;
  }

  public double getDssrInsPathAmount(boolean onRoot) {
    return onRoot ? dssrInsPathAmountRoot : dssrInsPathAmount;
  }

  public DssrInsNodeStr getDssrInsNodeStr(boolean onRoot) {
    return onRoot ? dssrInsNodeStrRoot : dssrInsNodeStr;
  }

  public double getDssrInsNodeAmount(boolean onRoot) {
    return onRoot ? dssrInsNodeAmountRoot : dssrInsNodeAmount;
  }

  public NgSetType getNgSetType(boolean onRoot) {
    return onRoot ? ngSetTypeRoot : ngSetType;
  }

  public double getNgSetSize(boolean onRoot) {
    return onRoot ? ngSetSizeRoot : ngSetSize;
  }

  public double getNgMixAlpha(boolean onRoot) {
    return onRoot ? ngMixAlphaRoot : ngMixAlpha;
  }

  public double getConcatStop(boolean onRoot) {
    return onRoot ? concatStopRoot : concatStop;
  }

  public double getNumColNonRoot() {
    return numCol;
  }

  public DssrInitStr getDssrInitStrNonRoot() {
    return dssrInitStr;
  }

  public double getDssrInitAmountNonRoot() {
    return dssrInitAmount;
  }

  public DssrInsPathStr getDssrInsPathStrNonRoot() {
    return dssrInsPathStr;
  }

  public double getDssrInsPathAmountNonRoot() {
    return dssrInsPathAmount;
  }

  public DssrInsNodeStr getDssrInsNodeStrNonRoot() {
    return dssrInsNodeStr;
  }

  public double getDssrInsNodeAmountNonRoot() {
    return dssrInsNodeAmount;
  }

  public NgSetType getNgSetTypeNonRoot() {
    return ngSetType;
  }

  public double getNgSetSizeNonRoot() {
    return ngSetSize;
  }

  public double getNgMixAlphaNonRoot() {
    return ngMixAlpha;
  }

  public double getConcatStopNonRoot() {
    return concatStop;
  }

  public double getNumColRoot() {
    return numColRoot;
  }

  public DssrInitStr getDssrInitStrRoot() {
    return dssrInitStrRoot;
  }

  public double getDssrInitAmountRoot() {
    return dssrInitAmountRoot;
  }

  public DssrInsPathStr getDssrInsPathStrRoot() {
    return dssrInsPathStrRoot;
  }

  public double getDssrInsPathAmountRoot() {
    return dssrInsPathAmountRoot;
  }

  public DssrInsNodeStr getDssrInsNodeStrRoot() {
    return dssrInsNodeStrRoot;
  }

  public double getDssrInsNodeAmountRoot() {
    return dssrInsNodeAmountRoot;
  }

  public NgSetType getNgSetTypeRoot() {
    return ngSetTypeRoot;
  }

  public double getNgSetSizeRoot() {
    return ngSetSizeRoot;
  }

  public double getNgMixAlphaRoot() {
    return ngMixAlphaRoot;
  }

  public double getConcatStopRoot() {
    return concatStopRoot;
  }

  public TreeNav getTreeNav() {
    return treeNav;
  }
  
  public IpModel getIpModel() {
    return ipModel;
  }

  public HeurUse getHeurUse() {
    return heurUse;
  }
  
  public void setHeurUse(HeurUse hu) {
    heurUse = hu;
  }
  
  public DomRule getDomRule() {
    return domRule;
  }
  
  public void setDomRule(DomRule dr) {
    domRule = dr;
  }

  public boolean isQuiet() {
    return !cmd.hasOption("verbose");
  }
  
  public boolean isEagerIp() {
    return cmd.hasOption("eager-ip");
  }
  
  public boolean monitorBounds() {
    return isEagerIp() && cmd.hasOption("monitor-bounds");
  }
  
  public boolean isSalvaging() {
    return !cmd.hasOption("no-salvage");
  }
  
  public boolean isTuning() {
    return cmd.hasOption("tuning");
  }
  
  public boolean isClassic() {
    return cmd.hasOption("classic");
  }

  public int getSize() {
    return size;
  }
  
  public void setSize(int size) {
    this.size = size;
  }

  public double getCapacity() {
    return capacity;
  }

  public double getShift() {
    return shift;
  }
  
  public double getDistUnitCost() {
    return distCost;
  }
  
  public double getTimeUnitCost() {
    return timeCost;
  }
  
  public int getTimeLimit() {
    return timeLimit;
  }
  
  public double getSolverTime() {
    return solverTime;
  }

  public Prog getProg() {
    return program;
  }
  
  public List<String> getArgs() {
    return cmd.getArgList();
  }

  @Override
  public String toString() {
    String newline = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    str.append("Acceleration Strategy:.........................");
    switch (getAlgo()) {
      case NONE:
        str.append("NONE");
        break;
      case DSSR_G:
        str.append("GLOBAL DSSR");
        break;
      case DSSR_L:
        str.append("LOCAL DSSR");
        break;
      case NG:
        str.append("NG");
        break;
      case HYBRID1:
        str.append("HYBRID1");
        break;
      case HYBRID2:
        str.append("HYBRID2");
        break;
      case HYBRID1_PLUS:
        str.append("HYBRID1_PLUS");
        break;
      case HYBRID2_PLUS:
        str.append("HYBRID2_PLUS");
        break;
      default:
        break;
    }
    str.append(newline);
    str.append("Number of columns:............................." + getNumColNonRoot());
    str.append(newline);
    str.append("Number of columns, on root node:..............." + getNumColRoot());
    str.append(newline);
    str.append("Initialization Strategy:.......................");
    if ((getAlgo() != Algo.DSSR_G && getAlgo() != Algo.DSSR_L
        && getAlgo() != Algo.HYBRID1 && getAlgo() != Algo.HYBRID1_PLUS)) {
      str.append("n/a");
    } else {
      switch (getDssrInitStrNonRoot()) {
        case HCA:
          str.append("HCA");
          str.append(newline);
          str.append("Size for Theta initialization:................."
              + getDssrInitAmountNonRoot());
          break;
        case TCA:
          str.append("TCA");
          str.append(newline);
          str.append("Size for Theta initialization:................."
              + getDssrInitAmountNonRoot());
          break;
        case WHCA:
          str.append("WHCA");
          str.append(newline);
          str.append("Size for Theta initialization:................."
              + getDssrInitAmountNonRoot());
          break;
        case WTCA:
          str.append("WTCA");
          str.append(newline);
          str.append("Size for Theta initialization:................."
              + getDssrInitAmountNonRoot());
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          str.append("Size for Theta initialization:................."
              + getDssrInitAmountNonRoot());
          break;
        case NONE:
          str.append("NONE");
          break;
        default:
          break;
      }
      str.append(newline);
      str.append("Initialization Strategy, on root node:.........");
      switch (getDssrInitStrRoot()) {
        case HCA:
          str.append("HCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + getDssrInitAmountRoot());
          break;
        case TCA:
          str.append("TCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + getDssrInitAmountRoot());
          break;
        case WHCA:
          str.append("WHCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + getDssrInitAmountRoot());
          break;
        case WTCA:
          str.append("WTCA");
          str.append(newline);
          str.append("Size for Theta initialization:................." + getDssrInitAmountRoot());
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          str.append("Size for Theta initialization:................." + getDssrInitAmountRoot());
          break;
        case NONE:
          str.append("NONE");
          break;
        default:
          break;
      }
      
    }
    str.append(newline);
    
    str.append("Path insertion strategy:.......................");
    if ((getAlgo() == Algo.NONE || getAlgo() == Algo.NG)) {
      str.append("n/a");
    } else {
      switch (getDssrInsPathStrNonRoot()) {
        case ONE_PATH:
          str.append("ONE_PATH");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + 1);
          break;
        case ALL_PATHS:
          str.append("ALL_PATHS");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + getNumColNonRoot());
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." 
              + getDssrInsPathAmountNonRoot());
          break;
        default:
          break;
      }
      str.append(newline);
      str.append("Path insertion strategy, on root node:.........");
      switch (getDssrInsPathStrRoot()) {
        case ONE_PATH:
          str.append("ONE_PATH");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + 1);
          break;
        case ALL_PATHS:
          str.append("ALL_PATHS");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." + getNumColRoot());
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          str.append("Amount of paths to verify and insert:.........." 
              + getDssrInsPathAmountRoot());
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("Node insertion strategy:.......................");
    if ((getAlgo() != Algo.DSSR_G
        && getAlgo() != Algo.HYBRID1
        && getAlgo() != Algo.HYBRID1_PLUS)) {
      str.append("n/a");
    } else {
      switch (getDssrInsNodeStrNonRoot()) {
        case ONE_NODE:
          str.append("ONE_NODE");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:..." + 1);
          break;
        case ALL_NODES:
          str.append("ALL_NODES");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:...ALL");
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          double perc = 100 * getDssrInsNodeAmountNonRoot();
          str.append("Amount of multiple nodes to insert per path:..." + perc + "%");
          break;
        default:
          break;
      }
      str.append(newline);
      str.append("Node insertion strategy, on root node:.........");
      switch (getDssrInsNodeStrRoot()) {
        case ONE_NODE:
          str.append("ONE_NODE");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:..." + 1);
          break;
        case ALL_NODES:
          str.append("ALL_NODES");
          str.append(newline);
          str.append("Amount of multiple nodes to insert per path:...ALL");
          break;
        case IN_BETWEEN:
          str.append("IN_BETWEEN");
          str.append(newline);
          double perc = 100 * getDssrInsNodeAmountRoot();
          str.append("Amount of multiple nodes to insert per path:..." + perc + "%");
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("NG set metric:.................................");
    if ((getAlgo() == Algo.DSSR_G
        || getAlgo() == Algo.NONE
        || getAlgo() == Algo.DSSR_L)) {
      str.append("n/a");
    } else {
      switch (getNgSetTypeNonRoot()) {
        case TRAVEL_TIME:
          str.append("TRAVEL_TIME");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case CHEAP_CYCLE_RISK:
          str.append("MAX_DURATION");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          double perc = 100 * getNgMixAlphaNonRoot();
          str.append("NG metric mixing percentage:..................." + perc + "%");
          break;
        default:
          break;
      }
      str.append(newline);
      str.append("NG set metric, on root node:...................");
      switch (getNgSetTypeRoot()) {
        case TRAVEL_TIME:
          str.append("TRAVEL_TIME");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case CHEAP_CYCLE_RISK:
          str.append("MAX_DURATION");
          str.append(newline);
          str.append("NG metric mixing percentage:...................n/a");
          break;
        case MIXED:
          str.append("MIXED");
          str.append(newline);
          double perc = 100 * getNgMixAlphaRoot();
          str.append("NG metric mixing percentage:..................." + perc + "%");
          break;
        default:
          break;
      }
    }
    str.append(newline);
    str.append("NG set size:...................................");
    if (getAlgo() == Algo.DSSR_G
        || getAlgo() == Algo.NONE
        || getAlgo() == Algo.DSSR_L) {
      str.append("n/a");
    } else {
      str.append(getNgSetSizeNonRoot());
      str.append(newline);
      str.append("NG set size, on root node:.....................");
      str.append(getNgSetSizeRoot());
    }
    return str.toString();
  }

  /**
   * Test client.
   * 
   * @param args program arguments
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    if (options.askHelp()) {
      options.printHelp("ciao");
    } else {
      System.out.println(options);
    }
  }
}
