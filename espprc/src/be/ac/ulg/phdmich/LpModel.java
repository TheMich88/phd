package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import ilog.concert.IloColumn;
import ilog.concert.IloConversion;
import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.concert.IloObjective;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class LpModel {
  private Instance instance;
  private Options options;
  private IloCplex cplex;
  private IloObjective totalCost;
  private IloRange[] visitConstraint;
  private VarArrayList var;
  private int size;
  private boolean isFeasible = true;
  private Map<IloNumVar, Path> pathMap;
  private static final double TOL = Lib.TOLERANCE;
  IloConversion lastConversion = null;
  
  /*
  private static class VarArray {
    int num = 0;
    IloNumVar[] array = new IloNumVar[3];

    public void add(IloNumVar ivar) {
      if (num >= this.array.length) {
        IloNumVar[] array = new IloNumVar[2 * this.array.length];
        System.arraycopy(this.array, 0, array, 0, num);
        this.array = array;
      }
      this.array[num++] = ivar;
    }
    
    public IloNumVar getElement(int index) {
      return this.array[index];
    }
    
    public int getSize() {
      return this.num;
    }
    
    public IloNumVar[] getArray() {
      return array;
    }
  }
  */
  
  private static class VarArrayList {
    ArrayList<IloNumVar> list = new ArrayList<>();
    
    public void add(IloNumVar ivar) {
      list.add(ivar);
    }
    
    public IloNumVar getElement(int index) {
      return list.get(index);
    }
    
    public int getSize() {
      return list.size();
    }
    
    public IloNumVar[] getArray() {
      return list.toArray(new IloNumVar[list.size()]);
    }
    
  }
  
  /**
   * Constructs a new LP model for the RLMP and initializes it with a trivial solution.
   * 
   * @param instance the problem instance
   * @param paths the set of paths representing the initial solution
   */
  public LpModel(Instance instance, Options options, Set<Path> paths) {
    // boolean log = !options.isQuiet();
    try {
      cplex = new IloCplex();
      cplex.setParam(IloCplex.BooleanParam.NumericalEmphasis, true);
      cplex.setParam(IloCplex.IntParam.Threads, 1);
      cplex.setParam(IloCplex.Param.TimeLimit, options.getSolverTime());
      if (true) {
        cplex.setOut(null);
      }
      var = new VarArrayList();
      totalCost = cplex.addMinimize();
      this.instance = instance;
      this.options = options;
      size = instance.getSize();
      pathMap = new HashMap<>();
      visitConstraint = new IloRange[size];
      for (int i = 0; i < size; i++) {
        String name = "visit " + (i + 1);
        visitConstraint[i] = cplex.addRange(1, Double.MAX_VALUE, name);
      }
      for (Path p : paths) {
        addColumn(p);
      }
    } catch (IloException ex) {
      System.err.println("Concert exception caught while constructing: " + ex);
    }
  }
  
  public boolean isDualFeasible() throws IloException {
    return cplex.isDualFeasible();
  }
  
  // adds a column to the LP model.
  private void addColumn(Path path) {
    int index = var.getSize();
    try {
      IloColumn col = cplex.column(totalCost, path.getCost());
      for (int node = 1; node <= size; node++) {
        int rangeIndex = node - 1;
        int coefficient = path.visits(node);
        col = col.and(cplex.column(visitConstraint[rangeIndex], coefficient));
      }
      String name = "r_" + (var.getSize() + 1);
      IloNumVar currentVar = cplex.numVar(col, 0, 1, name);
      cplex.add(currentVar);
      var.add(currentVar);
      pathMap.put(currentVar, path);
    } catch (IloException ex) {
      System.err.println("Concert exception caught while adding column " + index + ": " + ex);
    }
  }
  
  /**
   * Closes the model.
   * 
   */
  public void close() {
    try {
      cplex.clearModel();
    } catch (IloException ex) {
      System.err.println("Concert exception caught while closing model: " + ex);
    }
    cplex.end();
  }
  
  /**
   * Builds the current best solution of the RLMP.
   * 
   * @return the current optimal solution of the RLMP.
   */
  public Solution getSolution() {
    try {
      double cost = cplex.getObjValue();
      boolean allElem = true;
      Map<Path, Double> pathsValues = new HashMap<>();
      for (int i = 0; i < var.getSize(); i++) {
        IloNumVar ivar = var.getElement(i);
        double value = cplex.getValue(ivar);
        if (value > 0 + TOL) {
          Path path = pathMap.get(ivar);
          allElem &= path.isElementary();
          pathsValues.put(path, value);
        }
      }
      return new Solution(cost, pathsValues, instance, allElem); 
    } catch (IloException ex) {
      System.err.println("Concert exception caught while building solution: " + ex);
    }
    return new Solution(0, new TreeMap<>(), instance, true);
  }
  
  /**
   * Get the dual values associated to the current solution of the RLMP. 
   * The nodes associated do not include the depot.
   * 
   * @return an n-sized array with the dual values
   */
  public double[] getDuals() {
    double[] duals;
    try {
      duals = cplex.getDuals(visitConstraint);
      if (duals.length != size) {
        throw new IllegalStateException("Dual information with incorrect lenght");
      }
      for (int i = 0; i < duals.length; i++) {
        duals[i] = Lib.round(duals[i]);
      }
      return duals;
    } catch (UnknownObjectException ex) {
      System.err.println("Exception caught while returning duals: " + ex);
    } catch (IloException ex) {
      System.err.println("Concert exception caught while returning duals: " + ex);
    }
    return new double[size];
  }
  
  /**
   * Adds a set of columns to the LP model.
   * 
   * @param paths the set of paths associated with the columns to be added.
   */
  public void addColumns(Collection<Path> paths) {
    for (Path p : paths) {
      addColumn(p);
    }
  }

  /**
   * Adds a set of columns to the LP model, only if they are elementary.
   * 
   * @param paths the set of paths associated with the columns to be added.
   */
  public void addElementaryColumns(Collection<Path> paths) {
    for (Path p : paths) {
      if (p.isElementary()) {
        addColumn(p);
      }
    }
  }

  /**
   * Attempts to solve the LP model.
   * 
   * @param integral whether to attempt to solve the model as an IP.
   */
  public boolean solve(boolean integral) {
    try {
      IloConversion conversion = null;
      if (integral) {
        conversion = cplex.conversion(var.getArray(), IloNumVarType.Bool);
        cplex.add(conversion);
        if (options.getIpModel() == Options.IpModel.SPART) {
          for (int i = 0; i < visitConstraint.length; i++) {
            visitConstraint[i].setUB(1);
          }
        }
        lastConversion = conversion;
      }
      if (lastConversion != null && !integral) {
        cplex.remove(lastConversion);
        lastConversion = null;
        if (options.getIpModel() == Options.IpModel.SPART) {
          for (int i = 0; i < visitConstraint.length; i++) {
            visitConstraint[i].setUB(Double.POSITIVE_INFINITY);
          }
        }
      }
      //cplex.exportModel("model.sav");
      //cplex.writeParam("params.prm");
      boolean ans = cplex.solve();

      return ans;
    } catch (IloException ex) {
      System.err.println("Concert exception caught while solving: " + ex);
      isFeasible = false;
      return false;
    }
  }
  
  public boolean isFeasible() {
    return isFeasible;
  }

  public Map<IloNumVar, Path> getPathMap() {
    return pathMap;
  }
  
  private static List<Short> list(short[] arr) {
    List<Short> list = new ArrayList<>();
    for (short i = 0; i < arr.length; i++) {
      list.add(arr[i]);
    }
    return list;
  }
  
  /**
   * Test client.
   * 
   * @param args the program arguments
   */
  public static void main(String[] args) {
    short[] p1 = {0, 1, 2, 3, 6};
    short[] p2 = {0, 4, 5, 1, 6};
    short[] p3 = {0, 1, 2, 3, 4, 5, 6};
    short[] p4 = {0, 3, 5, 4, 1, 6};
    final Path path1 = new Path(list(p1), 21);
    final Path path2 = new Path(list(p2), 15);
    final Path path3 = new Path(list(p3), 28);
    final Path path4 = new Path(list(p4), 23);
    Set<Path> set = new HashSet<>();
    set.add(path1);
    set.add(path2);
    set.add(path3);
    In pfile = new In("data/Param.txt");
    In ifile = new In("data/data100/Random-Clustered/4.txt");
    Instance inst = new Instance(pfile, ifile);
    LpModel model = new LpModel(inst, new Options(new String[]{}), set);
    model.solve(false);
    double[] duals = model.getDuals();
    for (int i = 0; i < duals.length; i++) {
      StdOut.println(duals[i]);
    }
    set = new HashSet<>();
    set.add(path4);
    model.addColumns(set);
    model.solve(true);
    Solution solution = model.getSolution();
    StdOut.println(solution);
  }
}
