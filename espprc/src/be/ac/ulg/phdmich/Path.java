package be.ac.ulg.phdmich;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Path {
  private static final int FALLBACK_SIZE = 102;
  
  private List<Short> sequence;
  private boolean elementary;
  //private Label left = null;
  //private Label right = null;
  private Values values = null;
  //private Instance instance;
  private double demand = 0;
  private boolean feasible = true;
  
  /**
   * Build the path from its sequence and the associated problem instance.
   * 
   * @param sequence the list of nodes representing the path to build
   * @param instance the problem instance
   */
  public Path(List<Short> sequence, Instance instance) {
    this.sequence = new ArrayList<>(sequence);
    //this.instance = instance;
    this.feasible = checkIfValidSequence(this.sequence, instance);
    if (feasible) {
      double[] vals = computeValues(this.sequence, instance);
      if (vals != null) {
        double dur = vals[0];
        double dep = vals[1];
        double dist = vals[2];
        this.demand = vals[3];
        double cost = instance.getOptions().isClassic() ? dist
            : instance.getPar().getDistUnitCost() * dist
              + instance.getPar().getTimeUnitCost() * dur;
        this.values = new Values(0, cost, dur, dist, dep, dep);
        this.feasible = checkFeas(instance);
        checkElem(instance);
      } else {
        this.feasible = false;
      }
    }
  }
  
  Path(List<Short> seq, Values val, Instance inst, boolean co, Label le, Label ri) {
    this(seq, val, inst, co);
    demand = le.getDel() + ri.getDel();
    // next two lines for debugging
    // left = le;
    // right = ri;
  }

  Path(List<Short> seq, Values val, Instance instance, boolean compress) {
    this.sequence = seq;
    //this.instance = instance;
    checkElem(instance);
    this.values = val;
    if (compress) {
      compress(instance);
    }
  }
  
  Path(List<Short> seq, double cost) {
    this(seq, new Values(0, cost, cost, cost, 0, 0), null, false);
  }

  // checks if the path is elementary or not.
  private void checkElem(Instance instance) {
    this.elementary = true;
    int size = instance != null ? instance.getSize() + 2 : FALLBACK_SIZE;
    int[] visited = new int[size]; // TODO make a local variable
    for (int node : sequence) {
      if (visited[node] > 0) {
        this.elementary = false;
      }
      visited[node]++;
    }
  }
  
  Path.Values getValues() {
    return this.values;
  }
  
  private static boolean checkIfValidSequence(List<Short> sequence, Instance instance) {
    if (sequence.isEmpty()) {
      return false;
    }
    for (short i = 0; i < sequence.size() - 1; i++) {
      short n1 = sequence.get(i);
      short n2 = sequence.get(i + 1);
      if (instance.isForbidden(n1, n2)) {
        return false;
      }
    }
    return true;
  }
  
  private boolean checkFeas(Instance instance) {
    double maxShift = instance.getPar().getMaxDuration();
    double maxDem = instance.getPar().getMaxCapacity();
    if (instance.getOptions().isClassic()) {
      if (demand > maxDem) {
        return false;
      }
    } else {
      if (values.travelTime > maxShift || demand > maxDem) {
        return false;
      } 
    }
    return true;
  }
  
  private void compress(Instance inst) {
    double depStart = values.departureStart;
    double[] durAndDep = getDurAndDepTime(sequence, inst, depStart);
    
    double saving = inst.getPar().getTimeUnitCost() * (values.travelTime - durAndDep[0]);
    if (Math.abs(saving) < Lib.TOLERANCE) {
      values.departureEnd = durAndDep[1];
    } else {
      values.cost -= saving;
      values.cost = Lib.round(values.cost);
      values.reducedCost -= saving;
      values.reducedCost = Lib.round(values.reducedCost, 3);
      values.travelTime = durAndDep[0];
      values.departureStart = durAndDep[1]; // TODO correct this
      values.departureEnd = durAndDep[1];
    }
  }

  private static double[] computeValues(List<Short> sequence, Instance instance) {
    short n0 = sequence.get(0);
    short n1 = sequence.get(1);
    if (instance.getOptions().isClassic()) {
      return getClassicValues(sequence, instance);
    } else {
      double depTime = instance.getTwsOf(n1) - instance.getDistance(n0, n1);
      return getDurAndDepTime(sequence, instance, depTime);
    }
  }
  
  private static double[] getClassicValues(List<Short> sequence, Instance instance) {
    double totalDem = 0;
    double distance = 0;
    double duration = 0;
    for (short i = 1; i < sequence.size(); i++) {
      short prev = sequence.get(i - 1);
      short node = sequence.get(i);
      final double tws = instance.getTwsOf(node);
      final double twe = instance.getTweOf(node);
      final double dem = instance.getDemand(node);
      final double arc = instance.getDistance(prev, node);
      final double svt = instance.getServiceTime(prev);
      totalDem += dem;
      distance += arc;
      duration = Math.max(tws, duration + svt + arc);
      if (duration > twe) {
        return null;
      }
    }
    double[] values = { duration, 0, distance, totalDem };
    return values;
  }
  
  // Computes the actual duration, departure time, distance 
  // and cumulated demand of the sequence.
  // Returns null if a time window violation is detected
  private static double[] getDurAndDepTime(List<Short> sequence, 
      Instance inst, double depStart) {
    short firstNode = sequence.get(1);
    double distance = inst.getDistance(0, firstNode);
    double totalDem = inst.getDemand(firstNode);
    double maxDelay = inst.getTweOf(firstNode) - inst.getTwsOf(firstNode);
    double dlaydArr = inst.getTweOf(firstNode);
    for (short i = 2; i < sequence.size(); i++) {
      short prev = sequence.get(i - 1);
      short node = sequence.get(i);
      final double tws = inst.getTwsOf(node);
      final double twe = inst.getTweOf(node);
      distance += inst.getDistance(prev, node);
      totalDem += inst.getDemand(node);
      double time = inst.getDistance(prev, node) + inst.getServiceTime(prev);
      double slack = dlaydArr + time - twe;
      if (slack > 0) {
        maxDelay -= slack;
        if (maxDelay < 0) {
          return null;
        }
        dlaydArr = twe;
      } else {
        dlaydArr = Math.max(tws, dlaydArr + time); 
      }
    }
    
    double newDep = depStart + maxDelay;
    double duration = dlaydArr - newDep;
    double[] durAndDep = {duration, newDep, distance, totalDem};
    return durAndDep;
  }

  
  public double getCost() {
    return values.cost;
  }
  
  public boolean isFeasible() {
    return feasible;
  }
  
  public double getReducedCost() {
    return values.reducedCost;
  }
  
  public double getDistance() {
    return values.travelDistance;
  }
  
  public boolean isElementary() {
    return elementary;
  }
  
  /**
   * Method to test whether the path contains the specified edge.
   * 
   * @param edge the specified edge
   * @return true if and only if the path containes the specified edge
   */
  public boolean containsEdge(DirectedEdge edge) {
    return containsEdge(edge.from(), edge.to());
  }
  
  /**
   * Method to test whether the path contains the specified edge.
   * 
   * @param tail the tail node of the edge
   * @param head the head node of the edge
   * @return true if and only if the path containes the specified edge
   */
  public boolean containsEdge(int tail, int head) {
    for (int i = 0; i < sequence.size() - 1; i++) {
      if (sequence.get(i) == tail && sequence.get(i + 1) == head) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Checks whether the path contains an edge which is incompatible with
   * the specified one, i.e. if it has only one of the endpoints (i-k and k-j
   * are incompatible with i-j, for example).
   * 
   * @param edge the specified edge
   * @return whether the path contains an incompatible edge
   */
  public boolean containsIncompatibleEdge(DirectedEdge edge) {
    return containsIncompatibleEdge(edge.from(), edge.to());
  }
  
  /**
   * Checks whether the path contains an edge which is incompatible with
   * the specified one, i.e. if it has only one of the endpoints (i-k and k-j
   * are incompatible with i-j, for example).
   * 
   * @param tail the tail of the specified edge
   * @param head the head of the specified edge
   * @return whether the path contains an incompatible edge
   */
  public boolean containsIncompatibleEdge(int tail, int head) {
    for (int i = 0; i < sequence.size() - 1; i++) {
      int thisTail = sequence.get(i);
      int thisHead = sequence.get(i + 1);
      if ((thisTail == tail && thisHead != head) 
          || (thisHead == head && thisTail != tail)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Returns the amount of times the path visits a node.
   * 
   * @param node the selected node
   * @return the amount of times that node is visited
   */
  public int visits(int node) {
    int count = 0;
    for (int i : this.sequence) {
      if (i == node) {
        count++;
      }
    }
    return count;
  }
  
  //Label getLeftLabel() {
  //  return left;
  //}
  
  //Label getRightLabel() {
  //  return right;
  //}

  @Override
  public String toString() {
    final String newline = System.getProperty("line.separator");
    StringBuilder str = new StringBuilder();
    if (feasible) {
      str.append("cost: " + Lib.roundToString(this.values.cost) + "; ");
      str.append("r.cost: " + Lib.roundToString(this.values.reducedCost) + "; ");
      str.append("start time: [" + Lib.roundToString(this.values.departureStart) + ", "
          + Lib.roundToString(values.departureEnd) + "];\t");
      String elem = this.elementary ? "yes" : "no";
      str.append("elemetary: " + elem);
      str.append(newline);
    } else {
      str.append("Path is infeasible.");
      str.append(newline);
    }
    //str.append(" " 
    //    + "nodes: " + left.getNode() + " " + right.getNode() + "; " 
    //    + "dup? " + getValues().isFlagDe() + ";\t" 
    //    + "sequence: ");
    for (int node : getSequence()) {
      str.append(node + " -> ");
      //s.append(node + "\t");
    }
    str.delete(str.length() - 3, str.length());
    str.append(newline);
    return str.toString();
  }

  public List<Short> getSequence() {
    return sequence;
  }
  
  public double getDepartureEnd() {
    return values.getDepartureEnd();
  }
  
  public double getDepartureStart() {
    return values.getDepartureStart();
  }

  public static class ReducedCostMinOrder implements Comparator<Path> {
    @Override
    public int compare(Path p1, Path p2) {
      return Double.compare(p1.getReducedCost(), p2.getReducedCost());
    }
  }
  
  public static class CostMinOrder implements Comparator<Path> {
    @Override
    public int compare(Path p1, Path p2) {
      return Double.compare(p1.getCost(), p2.getCost());
    }
  }
  
  /**
   * A simple method to draw the path. Useful only after calling the 
   * same method on the instance first.
   * 
   */
  public void draw(Instance instance) {
    StdDraw.setPenColor(StdDraw.BLUE);
    StdDraw.setPenRadius(0.005);
    for (int i = 0; i < sequence.size() - 1; i++) {
      int from = sequence.get(i);
      int to   = sequence.get(i + 1);
      StdDraw.line(instance.getX(from), instance.getY(from), 
                   instance.getX(to),   instance.getY(to));
    }
  }

  public static class Values {
    private double reducedCost;
    private double cost;
    private double travelTime;
    private double travelDistance;
    private double departureStart;
    private double departureEnd;
    private boolean flagDe = false;

    /**
     * Constructor.
     * 
     * @param tval the travel time
     * @param cval the total cost
     * @param sval the earliest departure time
     * @param eval the latest departure time
     */
    public Values(double rc, double cval, double tval, double dval, double sval, double eval) {
      this.reducedCost = (rc > -Lib.TOLERANCE) ? 0 : Lib.round(rc, 3);
      this.cost = cval;
      this.travelTime = Lib.round(tval);
      this.travelDistance = Lib.round(dval);
      this.departureStart = Lib.round(sval);
      this.departureEnd = Lib.round(eval);
    }
    
    public boolean isFlagDe() {
      return flagDe;
    }

    public void setFlagDe(boolean val) {
      this.flagDe = val;
    }

    public double getCost() {
      return cost;
    }

    public double getTravelTime() {
      return travelTime;
    }

    public double getDepartureStart() {
      return departureStart;
    }

    public double getDepartureEnd() {
      return departureEnd;
    }
  }  
  
  /**
   * Test client.
   * 
   * @param args program arguments
   */
  public static void main(String[] args) {
    Options options = new Options(args);
    List<String> instances = options.getArgs();
    if (instances.isEmpty()) {
      return;
    }
    for (String i : instances) {
      Instance instance = new Instance(i, options);
      StdOut.println("Instance:\n" + instance);
      List<Short> sequence = Arrays.asList(
          (short)0, (short)67, (short)65, (short)63, (short)62, (short)74, (short)72, 
          (short)61, (short)64, (short)68, (short)66, (short)69, (short)101);
      Path path = new Path(sequence, instance);
      StdOut.println("Path:\n" + path);
      StdOut.println(path.isFeasible());
    }
  }
}
